package consumer2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: ZEN
 * @create: 2023-11-01 21:32
 **/
@SpringBootApplication
public class Consumer2Application {

    public static void main(String[] args) {
        SpringApplication.run(Consumer2Application.class);
    }
}
