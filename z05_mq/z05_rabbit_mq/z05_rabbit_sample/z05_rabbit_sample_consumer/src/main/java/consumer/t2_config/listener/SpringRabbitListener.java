package consumer.t2_config.listener;

import consumer.t2_config.config.DirectConfig;
import consumer.t2_config.config.FanoutConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: ZEN
 * @create: 2023-11-01 21:34
 **/
@Slf4j
@Component
public class SpringRabbitListener {

    /**
     * case6：声明交换机和队列
     * <pre>
     *     1 编程式声明
     *      @see DirectConfig
     *      @see  FanoutConfig
     *    2 注解式声明
     * </pre>
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "direct.queue1"),
            exchange = @Exchange(name = "hmall.direct", type = ExchangeTypes.DIRECT),
            key = {"red", "blue"}
    ))
    public void listenDirectQueue1Annotate(String msg) {
        System.out.println("消费者1接收到direct.queue1的消息：【" + msg + "】");
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "direct.queue2"),
            exchange = @Exchange(name = "hmall.direct", type = ExchangeTypes.DIRECT),
            key = {"red", "yellow"}
    ))
    public void listenDirectQueue2Annotate(String msg) {
        System.out.println("消费者2接收到direct.queue2的消息：【" + msg + "】");
    }


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "topic.queue1"),
            exchange = @Exchange(name = "hmall.topic", type = ExchangeTypes.TOPIC),
            key = "china.#"
    ))
    public void listenTopicQueue1Annotate(String msg) {
        System.out.println("消费者1接收到topic.queue1的消息：【" + msg + "】");
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "topic.queue2"),
            exchange = @Exchange(name = "hmall.topic", type = ExchangeTypes.TOPIC),
            key = "#.news"
    ))
    public void listenTopicQueue2Annotate(String msg) {
        System.out.println("消费者2接收到topic.queue2的消息：【" + msg + "】");
    }

    /**
     * 消息转换器测试
     *
     * @param msg
     * @throws InterruptedException
     */
    @RabbitListener(queues = "object.queue")
    public void listenSimpleQueueMessage(Map<String, Object> msg) throws InterruptedException {
        System.out.println("消费者接收到object.queue消息：【" + msg + "】");
    }

    /**
     * 报错：MismatchedInputException: Cannot deserialize value of
     * type `java.lang.String` from Object value (token `JsonToken.START_OBJECT`)
     * 因此实际开发中还是建议传String, json字符串
     *
     * @param msg
     * @throws InterruptedException
     */
//    @RabbitListener(queues = "object.queue")
    public void listenSimpleQueueMessage(String msg) throws InterruptedException {
        System.out.println("消费者接收到object.queue消息：【" + msg + "】");
    }


    // 消息确认机制
    @RabbitListener(queues = "sample.queue")
    public void listenSimpleAckessage(String msg) throws InterruptedException {
        log.info("spring 消费者接收到消息：【" + msg + "】");
        if (true) {
//            throw new RuntimeException("故意的");
        }
        log.info("消息处理完成");
    }


}
