package consumer.t3_reliability.listener;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentSkipListSet;

@Component
public class MessageConsumer {

    private ConcurrentSkipListSet<String> processedMessages = new ConcurrentSkipListSet<>();


    /**
     * Case1：唯一消息ID接收
     *
     * @param msg
     * @param headers
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "unique.queue"),
            exchange = @Exchange(name = "hmall.direct", type = ExchangeTypes.DIRECT),
            key = {"test-uid"}
    ))
    public void receiveUniqueMessage(@Payload String msg, @Headers MessageHeaders headers) {
        String messageId = headers.get("businessId", String.class);
        // 检查是否已经处理过该消息
        if (!processedMessages.add(messageId)) {
            // 如果返回false，说明该消息ID已经存在，即消息已经被处理过
            System.out.println("Message with ID " + messageId + " has already been processed.");
            return;
        }

        // 处理消息逻辑
        System.out.println("Processing message: " + msg);

        // 消息处理完成后，可以在这里确认消息
        // channel.basicAck(deliveryTag, false);
    }

    /**
     * Case2：唯一消息ID接收，基于CorrelationData
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "unique.queue1"),
            exchange = @Exchange(name = "hmall.direct", type = ExchangeTypes.DIRECT),
            key = {"test-uid1"}
    ))
    public void receiveMessage1(@Payload String msg, Message message) {
        String businessId = message.getMessageProperties().getCorrelationId();
        if (businessId == null) {
            System.out.println("businessId is null");
            return;
        }
        if (!processedMessages.add(businessId)) {
            // 如果返回false，说明该消息ID已经存在，即消息已经被处理过
            System.out.println("Message with ID " + businessId + " has already been processed.");
            return;
        }
        System.out.println("Received message with business ID: " + businessId);
        System.out.println("Payload: " + msg);
    }

}