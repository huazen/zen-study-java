package consumer.t3_reliability.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;

/**
 * @author: ZEN
 * @create: 2024-09-09 20:25
 **/
@Slf4j
public class LazyQueueConfig {

    /**
     * 方式一：基于bean
     *
     * @return
     */
    @Bean
    public Queue lazyQueue() {
        return QueueBuilder
                .durable("lazy.queue")
                .lazy() // 开启Lazy模式
                .build();
    }

    /**
     * 方式二：基于注解
     *
     * @param msg
     */
    @RabbitListener(queuesToDeclare = @org.springframework.amqp.rabbit.annotation.Queue(
            name = "lazy.queue",
            durable = "true",
            arguments = @Argument(name = "x-queue-mode", value = "lazy")
    ))
    public void listenLazyQueue(String msg) {
        log.info("接收到 lazy.queue的消息：{}", msg);
    }
}
