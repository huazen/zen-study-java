package consumer.t2_config.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: ZEN
 * @create: 2023-11-05 18:01
 **/
@Configuration
public class MessageConfig {

    @Bean
    public Queue objectQueue() {
        return new Queue("object.queue");
    }

    /**
     * 消息转换器
     * <pre>
     * 默认情况下Spring采用的序列化方式是JDK序列化。众所周知，JDK序列化存在下列问题：
     * ● 数据体积过大
     * ● 有安全漏洞
     * ● 可读性差
     * </pre>
     *
     * @return
     */
    @Bean
    public MessageConverter messageConverter() {
        // 1.定义消息转换器
        Jackson2JsonMessageConverter jackson2JsonMessageConverter = new Jackson2JsonMessageConverter();
        return jackson2JsonMessageConverter;
    }
}
