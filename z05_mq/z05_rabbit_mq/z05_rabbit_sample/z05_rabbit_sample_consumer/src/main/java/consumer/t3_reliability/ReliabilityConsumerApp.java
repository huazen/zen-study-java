package consumer.t3_reliability;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: ZEN
 * @create: 2024-09-09 22:19
 **/
@SpringBootApplication
public class ReliabilityConsumerApp {

    public static void main(String[] args) {
        SpringApplication.run(ReliabilityConsumerApp.class, args);
    }
}
