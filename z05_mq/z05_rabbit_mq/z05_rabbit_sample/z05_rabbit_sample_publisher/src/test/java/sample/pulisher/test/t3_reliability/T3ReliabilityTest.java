package sample.pulisher.test.t3_reliability;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.concurrent.ListenableFutureCallback;
import sample.publisher.t3_reliability.ReliabilityPublisherApp;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author: ZEN
 * @create: 2024-09-09 19:23
 **/
@Slf4j
@SpringBootTest(classes = ReliabilityPublisherApp.class)
public class T3ReliabilityTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @SneakyThrows
    @Test
    public void testPublisherConfirm() {
        // 1.创建CorrelationData
        CorrelationData cd = new CorrelationData(UUID.randomUUID().toString());
        // 2.给Future添加ConfirmCallback
        cd.getFuture().addCallback(new ListenableFutureCallback<CorrelationData.Confirm>() {
            @Override
            public void onFailure(Throwable ex) {
                // 2.1.Future发生异常时的处理逻辑，基本不会触发
                log.error("send message fail", ex);
            }

            @Override
            public void onSuccess(CorrelationData.Confirm result) {
                // 2.2.Future接收到回执的处理逻辑，参数中的result就是回执内容
                if (result.isAck()) { // result.isAck()，boolean类型，true代表ack回执，false 代表 nack回执
                    log.info("发送消息成功，收到 ack!");
                } else { // result.getReason()，String类型，返回nack时的异常描述
                    log.error("发送消息失败，收到 nack, reason : {}", result.getReason());
                }
            }
        });
        // 3.发送消息
        rabbitTemplate.convertAndSend("hmall.direct", "key", "hello", cd);
        // publisher-confirm-type: simple
        TimeUnit.MINUTES.sleep(5);
    }


    /**
     * Case1：发送自定义消息ID，基于 MessagePostProcessor
     * 官方自带唯一消息ID
     */
    @Test
    public void testSendUidExchange() {
        String businessId = UUID.randomUUID().toString(); // 创建业务ID
        System.out.println("业务ID: " + businessId);
        MessagePostProcessor processor = message -> {
            message.getMessageProperties().setHeader("businessId", businessId);
            return message;
        };
        rabbitTemplate.convertAndSend("hmall.direct", "test-uid", "红色警报", processor);
        // 测试消息ID重复
        rabbitTemplate.convertAndSend("hmall.direct", "test-uid", "红色警报2", processor);
    }

    /**
     * Case2：发送自定义消息ID,基于CorrelationData
     */
    @Test
    public void testSendUidExchange1() {
        String businessId = UUID.randomUUID().toString(); // 创建业务ID
        System.out.println("业务ID: " + businessId);

        CorrelationData correlationData = new CorrelationData(businessId);
        rabbitTemplate.convertAndSend("hmall.direct", "test-uid1", "红色警报", correlationData);
        // 测试消息ID重复
        rabbitTemplate.convertAndSend("hmall.direct", "test-uid1", "红色警报1", correlationData);
    }

    @Test
    void testPublisherDelayMessage() {
        // 1.创建消息
        String message = "hello, delayed message";
        // 2.发送消息，利用消息后置处理器添加消息头
        rabbitTemplate.convertAndSend("delay.direct", "delay", message,
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message)
                            throws AmqpException {
                        // 添加延迟消息属性
                        message.getMessageProperties().setDelay(20 * 1000);
                        return message;
                    }
                });
    }

}
