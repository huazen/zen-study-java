package sample.pulisher.test.t1_base;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import sample.publisher.t1_base.Application;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: ZEN
 * @create: 2024-09-08 21:13
 **/
@SpringBootTest(classes = Application.class)
public class T1BaseTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSampleQueue() {
        // 队列名称
        String queueName = "sample.queue";
        // 消息
        String message = "hello, spring amqp!";
        // 发送消息
        rabbitTemplate.convertAndSend(queueName, message);
    }


    /**
     * Work queues，任务模型。简单来说就是让多个消费者绑定到一个队列，共同消费队列中的消息
     * 实现：
     * 管理页面创建队列，向队列中不停发送消息，模拟消息堆积。
     */
    @Test
    public void testWorkQueue() throws InterruptedException {
        // 队列名称
        String queueName = "work.queue";
        // 消息
        String message = "hello, message_";
        for (int i = 0; i < 50; i++) {
            // 发送消息，每20毫秒发送一次，相当于每秒发送50条消息
            rabbitTemplate.convertAndSend(queueName, message + i);
            Thread.sleep(20);
        }
    }

    /**
     * Fanout Exchange发送消息
     */
    @Test
    public void testFanoutExchange() {
        // 交换机名称
        String exchangeName = "hmall.fanout";
        // 消息
        String message = "hello, everyone!";
        // 使用重载方法
        rabbitTemplate.convertAndSend(exchangeName, "", message);
    }

    /**
     * Direct Exchange发送消息
     */
    @Test
    public void testSendDirectExchange() {
        // 交换机名称
        String exchangeName = "hmall.direct";
        // 消息
        String message = "红色警报！日本乱排核废水，导致海洋生物变异，惊现哥斯拉！";
        // 发送消息
        rabbitTemplate.convertAndSend(exchangeName, "key1", message + "key1");
        rabbitTemplate.convertAndSend(exchangeName, "key2", message + "key2");
        rabbitTemplate.convertAndSend(exchangeName, "key3", message + "key3");
    }

    /**
     * topicExchange发送消息
     */
    @Test
    public void testSendTopicExchange() {
        // 交换机名称
        String exchangeName = "hmall.topic";
        // 消息
        String message = "喜报！孙悟空大战哥斯拉，胜!";
        // 发送消息
        rabbitTemplate.convertAndSend(exchangeName, "xx.key1.yy", message + "-xx.key1.yy"); // Q1
        rabbitTemplate.convertAndSend(exchangeName, "xx.key1.key2", message + "-xx.key1.key2"); // Q1、Q2
        rabbitTemplate.convertAndSend(exchangeName, "xx.yy.key2", message + "-xx.yy.key2"); // Q2
        rabbitTemplate.convertAndSend(exchangeName, "key3.xx.key2", message + "-key3.xx.key2"); // Q2
        // 以下匹配不到对应队列
        rabbitTemplate.convertAndSend(exchangeName, "xx.yy.zz", message + "-xx.yy.zz");
        rabbitTemplate.convertAndSend(exchangeName, "key1", message + "-key1");
        rabbitTemplate.convertAndSend(exchangeName, "xx.key1.zz.key2", message + "-xx.key1.zz.key2");
    }

    @Test
    public void testSendMap() throws InterruptedException {
        // 准备消息
        Map<String, Object> msg = new HashMap<>();
        msg.put("name", "柳岩");
        msg.put("age", 21);
        // 发送消息
        rabbitTemplate.convertAndSend("object.queue", msg);
    }

}
