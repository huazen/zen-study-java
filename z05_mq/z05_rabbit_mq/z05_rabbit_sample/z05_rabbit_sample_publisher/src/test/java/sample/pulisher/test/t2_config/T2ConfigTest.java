package sample.pulisher.test.t2_config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sample.publisher.t2_config.ConfigApplication;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: ZEN
 * @create: 2024-09-08 23:42
 **/
@RunWith(SpringRunner.class) // 使用junit4进行测试时，需要加此配置
@SpringBootTest(classes = ConfigApplication.class)
public class T2ConfigTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @Test
    public void testSendMap() throws InterruptedException {
        // 准备消息
        Map<String, Object> msg = new HashMap<>();
        msg.put("name", "柳岩");
        msg.put("age", 28);
        // 发送obj对象，需要将obj对象序列化，测试json序列化
        rabbitTemplate.convertAndSend("object.queue", msg);
    }
}
