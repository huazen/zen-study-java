package sample.publisher.t3_reliability.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

/**
 * @author: ZEN
 * @create: 2024-09-09 18:47
 **/
@Controller
public class PublisherController {

    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 先开启rabbit、启动应用，然后关闭rabbit后访问testRetry
     * fixme 实际应用启动就退出，无报错
     */
    @GetMapping("/testRetry")
    public void testRetry() {
        // 队列名称
        String queueName = "sample.queue";
        // 消息
        String message = "hello, spring amqp!";
        // 发送消息
        rabbitTemplate.convertAndSend(queueName, message);
    }
}
