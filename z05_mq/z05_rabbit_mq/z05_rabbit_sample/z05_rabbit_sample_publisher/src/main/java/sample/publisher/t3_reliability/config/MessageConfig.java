package sample.publisher.t3_reliability.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: ZEN
 * @create: 2024-09-09 22:06
 **/
@Configuration
public class MessageConfig {

    @Bean
    public MessageConverter messageConverter() {
        // 1. 创建Jackson2JsonMessageConverter的实例，用于JSON消息的序列化和反序列化
        Jackson2JsonMessageConverter jjmc = new Jackson2JsonMessageConverter();

        // 2. 通过调用setCreateMessageIds(true)方法，配置该转换器在序列化消息时自动为每条消息创建一个唯一的ID
        // 这个ID可以帮助追踪和识别消息，特别是在分布式系统中处理大量消息时非常有用
        jjmc.setCreateMessageIds(true);

        // 返回配置好的Jackson2JsonMessageConverter实例，Spring容器会将其注册为一个bean
        // 这样就可以在需要消息转换器的地方，通过自动装配等方式来使用它了
        return jjmc;
    }
}
