package sample.publisher.t3_reliability;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: ZEN
 * @create: 2024-09-09 18:46
 **/
@SpringBootApplication
public class ReliabilityPublisherApp {

    public static void main(String[] args) {
        SpringApplication.run(ReliabilityPublisherApp.class, args);
    }
}
