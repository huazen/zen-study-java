package zen.rocket.consumer.t1_base;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 配置消息监听器-push模式
 * 这是Spring整合RocketMQ后提供的注解接口，用于简化消息监听器的定义
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "rocket_base",
        consumerGroup = "consumer_grp_01",
        maxReconsumeTimes = -1,  // 消息最大重试次数
        delayLevelWhenNextConsume = 0  // 延迟消息
)
public class MyRocketListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String message) {
        // 处理broker推送过来的消息
        log.info("接收：" + message);
    }
}
