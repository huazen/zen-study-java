package zen.rocket.consumer.t1_base;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @author: ZEN
 * @create: 2024-11-02 16:07
 **/
@Slf4j
@Component
@RocketMQMessageListener(topic = "SQL92FilterBase", consumerGroup = "consumer_grp_01",
        selectorType = SelectorType.SQL92,
//        selectorExpression="TAGS in ('girl','girl2')",
        selectorExpression = "(age > 18 and age < 30)"
)
public class SQL92RocketListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        log.info("SQL92RocketListener 接收到消息:{}", s);
    }
}
