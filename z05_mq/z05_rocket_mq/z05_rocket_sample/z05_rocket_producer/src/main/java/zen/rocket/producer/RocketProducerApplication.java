package zen.rocket.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: ZEN
 * @create: 2024-10-31 22:02
 **/
@SpringBootApplication
public class RocketProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RocketProducerApplication.class, args);
    }
}
