package zen.rocket.producer.test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import zen.rocket.producer.RocketProducerApplication;

/**
 * @author: ZEN
 * @create: 2024-11-02 07:22
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RocketProducerApplication.class)
public class RocketTest {


}
