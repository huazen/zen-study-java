package zen.rocket.producer.test.t0_source;

import org.apache.rocketmq.client.consumer.DefaultMQPullConsumer;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.PullResult;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.consumer.rebalance.AllocateMessageQueueAveragely;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.junit.Test;

import java.util.List;
import java.util.Set;

public class RocketSourceTest {

    @Test
    public void testSendMessage() throws Exception {
        // 实例化一个生产者实例，并指定一个生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("test-producer-group");

        // 指定NameServer地址
        producer.setNamesrvAddr("192.168.0.116:9876");

        // 启动生产者实例
        producer.start();

        try {
            // 创建一条消息，指定主题Topic、Tag和消息体
            Message msg = new Message("test-topic", "TagA", "Hello, RocketMQ!".getBytes());

            // 发送消息到一个Broker
            SendResult send = producer.send(msg);

            System.out.println("Message sent successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 关闭生产者实例
        producer.shutdown();
    }

    /**
     * 异步发送消息
     *
     * @throws Exception
     */

    @Test
    public void testAsyncSendMessage() throws Exception {
        DefaultMQProducer producer = new DefaultMQProducer("test-producer-group");
        producer.setNamesrvAddr("192.168.0.116:9876");
        producer.start();

        for (int i = 0; i < 10; i++) {
            Message msg = new Message("test-topic", "TagB", ("Hello, RocketMQ " + i).getBytes());
            // 异步发送消息到一个Broker
            producer.send(msg, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                    System.out.println("Sent message with ID: " + sendResult.getMsgId());
                }

                @Override
                public void onException(Throwable throwable) {
                    System.out.println("Failed to send message: " + throwable.getMessage());
                }
            });
        }
        // 由于是异步发送消息，上面循环结束之后，消息可能还没收到broker的响应
        // 如果不sleep一会儿，就报错
        Thread.sleep(10_000);

        producer.shutdown();
    }


    /**
     * 拉取消息-pull方式已经废弃，无需过于关注
     *
     * @throws Exception
     */
    @Test
    public void testPullConsumer() throws Exception {
        DefaultMQPullConsumer consumer = new DefaultMQPullConsumer("test-consumer-group");
        consumer.setNamesrvAddr("192.168.0.116:9876");
        consumer.start();

        // 获取指定主题的消息队列集合
        Set<MessageQueue> messageQueues = consumer.fetchSubscribeMessageQueues("test-topic");

        for (MessageQueue messageQueue : messageQueues) {
            /*
              // 第一个参数是MessageQueue对象，代表了当前主题的一个消息队列
              // 第二个参数是一个表达式，对接收的消息按照tag进行过滤
              // 支持"tag1 || tag2 || tag3"或者 "*"类型的写法；null或者"*"表示不对 消息进行tag过滤
              // 第三个参数是消息的偏移量，从这里开始消费
              // 第四个参数表示每次最多拉取多少条消息
            * */
            PullResult result = consumer.pull(messageQueue, "TagB", 0, 10);
            // 打印消息队列的信息
            System.out.println("message******queue******" + messageQueue);
            // 获取从指定消息队列中拉取到的消息
            final List<MessageExt> msgFoundList = result.getMsgFoundList();
            if (msgFoundList == null) continue;
            for (MessageExt messageExt : msgFoundList) {
                System.out.println(messageExt);
                System.out.println(new String(messageExt.getBody(), "utf-8"));
            }
        }
        consumer.shutdown();
    }

    @Test
    public void testPushConsumer() throws Exception {
        // 实例化一个消费者实例，并指定一个消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("test-consumer-group");

        // 指定NameServer地址
        consumer.setNamesrvAddr("192.168.0.116:9876");

        // 订阅一个或多个Topic，以及Tag来过滤需要消费的消息
        consumer.subscribe("test-topic", "*");

        // 添加消息监听器，一旦有消息推送过来，就进行消费
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("Received message: " + new String(msg.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });

        // 启动消费者实例
        consumer.start();

        System.out.println("Consumer started");

        Thread.sleep(10000);
        consumer.shutdown();
    }


    @Test
    public void testSendMessageConfig() throws Exception {
        DefaultMQProducer producer = new DefaultMQProducer("test-producer-group");
        producer.setNamesrvAddr("192.168.0.116:9876");
        producer.start();

        Message msg = new Message("test-topic", "TagB", ("Hello, RocketMQ").getBytes());
        // 设置延时等级，0-18级，越小越快
        msg.setDelayTimeLevel(0);
        // 指定消息发送到哪个队列, 参数1：主题，参数2：broker名称，参数3：队列编号
        producer.send(msg, new MessageQueue("test-topic", "test-broker", 0));


        System.out.println("Sent message oneway successfully");
        producer.shutdown();
    }

    @Test
    public void testConsumerConfig() throws Exception {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("test-consumer-group");
        consumer.setNamesrvAddr("192.168.0.116:9876");
        consumer.subscribe("test-topic", "*");

        // 设置负载均衡算法
        consumer.setAllocateMessageQueueStrategy(new AllocateMessageQueueAveragely());
        // 设置重新消费的次数，共16个级别，大于16的一律按照2小时重试
        consumer.setMaxReconsumeTimes(20);

        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("Received message: " + new String(msg.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });

        consumer.start();

        Thread.sleep(10000);
        consumer.shutdown();
    }


    /**
     * 查询消息。先尝后买
     *
     * @throws InterruptedException
     * @throws MQClientException
     * @throws MQBrokerException
     * @throws RemotingException
     */
    @Test
    public void testQueryMq() throws InterruptedException, MQClientException, MQBrokerException, RemotingException {
        DefaultMQPullConsumer consumer = new
                DefaultMQPullConsumer("consumer_grp_09_01");
        consumer.setNamesrvAddr("node1:9876");
        consumer.start();
        MessageExt message = consumer.viewMessage("tp_demo_08",
                "0A4E00A7178878308DB150A780BB0000");
        System.out.println(message);
        System.out.println(message.getMsgId());
        consumer.shutdown();
    }

}