package zen.rocket.producer.test.t1_base;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import zen.rocket.producer.test.RocketTest;

/**
 * @author: ZEN
 * @create: 2024-11-02 07:26
 **/
public class T1BaseTest extends RocketTest {


    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Test
    public void testSendMessage() {
        // 用于向broker发送消息
        // 第一个参数是topic名称
        // 第二个参数是消息内容
        this.rocketMQTemplate.convertAndSend(
                "rocket_base",
                "zen: hello hua"
        );
    }

    @Test
    public void testSqlFilterSend() {
        // 设置消息属性: 设置消息的Header属性，这些属性将用于SQL92过滤条件中
        Message msg1 = MessageBuilder.withPayload("美女A,年龄22,体重45")
                .setHeader(RocketMQHeaders.TAGS, "girl")
                .setHeader("age", 22)
                .setHeader("weight", 45)
                .build();
        rocketMQTemplate.send("SQL92FilterBase", msg1);

        Message msg2 = MessageBuilder.withPayload("美女B,年龄33,体重65")
                .setHeader(RocketMQHeaders.TAGS, "girl")
                .setHeader("age", 33)
                .setHeader("weight", 65)
                .build();
        rocketMQTemplate.send("SQL92FilterBase", msg2);

        Message msg3 = MessageBuilder.withPayload("美女C,年龄55,体重99")
                .setHeader(RocketMQHeaders.TAGS, "girl2")
                .setHeader("age", 55)
                .setHeader("weight", 99)
                .build();
        rocketMQTemplate.send("SQL92FilterBase", msg3);
    }
}
