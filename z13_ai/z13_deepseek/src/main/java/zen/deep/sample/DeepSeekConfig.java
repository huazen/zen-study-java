package zen.deep.sample;

/**
 * @author ZEN
 * @since 2025-03-08 17:19
 **/
public class DeepSeekConfig {

    public static final String API_KEY = "sk-xxx";
    public static final String API_URL = "https://api.deepseek.com/v1/chat/completions";
}
