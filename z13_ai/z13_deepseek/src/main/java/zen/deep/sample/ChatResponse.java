
package zen.deep.sample;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ChatResponse {
    private List<Choice> choices;

    @Data
    public static class Choice {
        private ChatRequest.Message message;
        private String finishReason;

        @JsonProperty("finish_reason")
        public String getFinishReason() {
            return finishReason;
        }

        // other getters
    }

    // getters
}