package zen.deep.sample;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;
import java.util.Arrays;

public class DeepSeekService {
    private final OkHttpClient client = new OkHttpClient();
    private final ObjectMapper objectMapper = new ObjectMapper();

    public String chatCompletion(String prompt) throws IOException {
        // 构建请求体
        ChatRequest.Message systemMsg = new ChatRequest.Message("system", "You are a helpful assistant.");
        ChatRequest.Message userMsg = new ChatRequest.Message("user", prompt);
        ChatRequest requestBody = new ChatRequest();
        requestBody.setModel("deepseek-chat");
        requestBody.setMessages(Arrays.asList(systemMsg, userMsg));
        requestBody.setStream(false);

        // 创建 HTTP 请求
        Request request = new Request.Builder()
                .url(DeepSeekConfig.API_URL)
                .addHeader("Authorization", "Bearer " + DeepSeekConfig.API_KEY)
                .addHeader("Content-Type", "application/json")
                .post(RequestBody.create(
                        objectMapper.writeValueAsString(requestBody),
                        MediaType.parse("application/json")
                ))
                .build();

        // 发送请求并处理响应
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code: " + response);
            }

            ChatResponse chatResponse = objectMapper.readValue(
                    response.body().string(),
                    ChatResponse.class
            );

            return chatResponse.getChoices().get(0).getMessage().getContent();
        }
    }
}