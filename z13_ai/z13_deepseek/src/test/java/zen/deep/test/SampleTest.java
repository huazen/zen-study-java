package zen.deep.test;

import zen.deep.sample.DeepSeekService;

import java.io.IOException;

/**
 * @author ZEN
 * @since 2025-03-08 17:23
 **/
public class SampleTest {


    public static void main(String[] args) {
        DeepSeekService service = new DeepSeekService();
        try {
            String response = service.chatCompletion("Java如何实现单例模式？");
            System.out.println("DeepSeek 响应：\n" + response);
        } catch (IOException e) {
            System.err.println("API调用失败: " + e.getMessage());
        }
    }
}
