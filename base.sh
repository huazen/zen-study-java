docker run -d -p 3309:3306 --privileged=true \
-v /Users/hua/docker-data/mysql/8.0base3/log:/var/log/mysql \
-v /Users/hua/docker-data/mysql/8.0base3/data:/var/lib/mysql \
-v /Users/hua/docker-data/mysql/8.0base3/conf:/etc/mysql/conf.d \
-v /Users/hua/docker-data/mysql/8.0base3/mysql-files:/var/lib/mysql-files/ \
-e MYSQL_ROOT_PASSWORD=123456 \
--name mysql_base3 mysql