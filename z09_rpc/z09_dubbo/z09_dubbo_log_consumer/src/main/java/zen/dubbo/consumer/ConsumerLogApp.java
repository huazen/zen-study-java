package zen.dubbo.consumer;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import zen.common.log.ZenLogger;

/**
 * @author: ZEN
 * @create: 2025-03-03 21:22
 **/
@EnableDubbo
@SpringBootApplication
public class ConsumerLogApp {

    public static void main(String[] args) {
        try {
            SpringApplication.run(ConsumerLogApp.class, args);
        } catch (Throwable throwable) {
            ZenLogger.init().error(throwable);
        }

    }
}
