package zen.dubbo.api;

/**
 * 服务接口定义（需独立打包，供提供者和消费者依赖）
 * mvn install
 */
public interface DemoService {


    String sayHello(String name);
}