package zen.dubbo.producer.demo;

import org.apache.dubbo.config.annotation.DubboService;
import zen.dubbo.api.DemoService;

@DubboService
public class DemoServiceImpl implements DemoService {
    @Override
    public String sayHello(String name) {
        return "Hello " + name;
    }
}