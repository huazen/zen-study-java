package zen.dubbo.producer;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: ZEN
 * @create: 2025-03-03 21:41
 **/
@EnableDubbo
@SpringBootApplication
public class ProducerApp {

    public static void main(String[] args) {
        try {
            SpringApplication.run(ProducerApp.class, args);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
