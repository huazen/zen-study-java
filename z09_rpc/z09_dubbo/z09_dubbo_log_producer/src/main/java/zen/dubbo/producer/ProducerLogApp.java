package zen.dubbo.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import zen.common.basic.util.ExceptionUtils;

/**
 * @author: ZEN
 * @create: 2025-03-03 21:41
 **/
@Slf4j
@EnableDubbo
@SpringBootApplication
public class ProducerLogApp {

    public static void main(String[] args) {
        try {
            SpringApplication.run(ProducerLogApp.class, args);
        } catch (Throwable e) {
//            ZenLogger.init().error(throwable);
            log.info("[ProducerLogApp] main error! stack={}", ExceptionUtils.getStackTrace(e));
        }
    }
}
