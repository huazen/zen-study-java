package zen.dubbo.consumer.demo;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import zen.dubbo.api.DemoService;

@Component
public class Consumer implements CommandLineRunner {
    @DubboReference
    private DemoService demoService;

    @Override
    public void run(String... args) throws Exception {
        for (int i = 0; i < 20; i++) {
            demoService.sayHello("World " + i);
            Thread.sleep(1000);
        }
    }
}