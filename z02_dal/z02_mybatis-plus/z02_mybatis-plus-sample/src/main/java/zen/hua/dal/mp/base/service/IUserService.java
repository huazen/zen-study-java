package zen.hua.dal.mp.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import zen.hua.dal.mp.base.entity.User;

/**
 * service接口
 */
public interface IUserService extends IService<User> {

}
