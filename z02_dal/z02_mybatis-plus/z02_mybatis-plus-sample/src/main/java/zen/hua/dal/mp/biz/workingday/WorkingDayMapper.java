package zen.hua.dal.mp.biz.workingday;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @program: zen-study
 * @description: 工作日Mapper
 * @author: HUA
 * @create: 2023-02-11 10:47
 **/
public interface WorkingDayMapper extends BaseMapper<WorkingDayDO> {
}
