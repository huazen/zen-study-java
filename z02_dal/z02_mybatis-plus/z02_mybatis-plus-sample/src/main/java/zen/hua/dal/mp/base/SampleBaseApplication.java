package zen.hua.dal.mp.base;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import static zen.hua.dal.mp.base.SampleBaseApplication.BASE_PACKAGE;


@SpringBootApplication(scanBasePackages = BASE_PACKAGE)
@PropertySource("classpath:application-base-h2.properties")
// mapper包扫描
@MapperScan(BASE_PACKAGE + ".mapper")
public class SampleBaseApplication {

    public static final String BASE_PACKAGE = "zen.hua.dal.mp.base";

    public static void main(String[] args) {
        SpringApplication.run(SampleBaseApplication.class, args);
    }
}
