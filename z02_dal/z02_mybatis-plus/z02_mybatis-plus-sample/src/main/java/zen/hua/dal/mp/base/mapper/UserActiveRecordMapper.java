package zen.hua.dal.mp.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import zen.hua.dal.mp.base.entity.UserActiveRecord;

/**
 * @program: zen-study
 * @description: mapper示例
 * @author: HUA
 * @create: 2023-02-23 21:43
 **/
public interface UserActiveRecordMapper extends BaseMapper<UserActiveRecord> {

}
