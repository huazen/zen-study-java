package zen.hua.dal.mp.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import zen.hua.dal.mp.base.entity.LogicData;

/**
 * @program: zen-study
 * @description: 逻辑删除mapper
 * @author: HUA
 * @create: 2023-02-24 00:04
 **/
public interface LogicDataMapper extends BaseMapper<LogicData> {
}
