package zen.hua.dal.mp.base.c2_serivce;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import zen.hua.dal.mp.base.BaseTest;
import zen.hua.dal.mp.base.entity.User;
import zen.hua.dal.mp.base.service.IUserService;

import java.util.List;

/**
 * @author HUA
 * @since 2023-08-31 21:48
 **/
public class T1_CudTest extends BaseTest {

    @Autowired
    private IUserService userService;

    /**
     * 单条插入
     */
    @Test
    public void testSave() {
        User user = new User();
        user.setAge(27);
        user.setName("hua");
        // INSERT INTO user ( id, name, age ) VALUES ( ?, ?, ? )
        userService.save(user);

        System.out.println(user.getId());
    }

    /**
     * 批量插入
     */
    @Test
    public void testSaveBatch() {
        List<User> userList = CollUtil.newArrayList(
                new User().setAge(17).setName("hua"),
                new User().setAge(27).setName("zen")
        );

        /**
         * case1: 普通场景
         *  INSERT INTO user ( id, name, age ) VALUES ( ?, ?, ? )
         *  单个sql格式，批量提交，底层基于 org.apache.ibatis.session.SqlSession#flushStatements()
         */
//        userService.saveBatch(userList);


        /**
         * case2: 传入batchSize，根据batchSize 拆分后，批量提交
         */
        userService.saveBatch(userList, 1);
    }


    @Test
    public void testSaveOrUpdate() {
        User user = new User();
        user.setAge(27);
        user.setName("hua");
        user.setId(1L);

        /**
         * case1: 根据id查，在判断更新or插入
         * 1  SELECT id,name,age,email FROM user WHERE id=?
         * 2  UPDATE user SET name=?, age=? WHERE id=?
         */
//        userService.saveOrUpdate(user);


        /**
         * case2: updateWrapper
         * 1 根据updateWrapper尝试更新，否则执行case1 逻辑
         * 2 与entity组成复合set，注意column不要冲突
         * 3 entity中的id不参与set
         */
        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
        wrapper.eq("id", 6)
                .set("name", "zen");
        // update user set name=?,age=?,name=? where (id=?)
        userService.saveOrUpdate(user, wrapper);
    }

    @Test
    public void testSaveOrUpdateBatch() {
        List<User> userList = CollUtil.newArrayList(
                new User().setAge(17).setName("hua"),
                new User().setAge(27).setName("zen")
        );

        /**
         * 1 有主键id，查询之后，判断更新or插入；无，则直接插入
         */
        userService.saveOrUpdateBatch(userList);
        userService.saveOrUpdateBatch(userList, 1);
    }

    @Test
    public void testRemove() {
        /**
         * case1: Wrapper
         */
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("id", 1);
        // DELETE FROM user WHERE (id = ?)
//        userService.remove(wrapper);


        /**
         * case2: byId
         */
        //DELETE FROM user WHERE id=2
//        userService.removeById(2l);

        // DELETE FROM user WHERE id=3
//        userService.removeById(new User().setId(3L));


        /**
         * case3: columnMap
         */
        // DELETE FROM user WHERE name = ? AND id = ?
        userService.removeByMap(MapUtil.ofEntries(MapUtil.entry("id", 1), MapUtil.entry("name", "hua")));
    }


    @Test
    public void testUpdate() {
        /**
         * case1: 根据updateWrapper
         */
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("name", "hua")
                .eq("id", 1);
        // UPDATE user SET name=? WHERE (id = ?)
//        userService.update(updateWrapper);

        /**
         * case2: entity + updateWrapper
         *  1. 两者组成set，column不要重复
         *  2. entity的id不参与set
         */
        User user = new User().setAge(17)
                .setEmail("zen@123.com")
                .setId(2L);
        // UPDATE user SET age=17, email=zen@123.com, name=hua WHERE (id = 1)
//        userService.update(user,updateWrapper);


        /**
         * case3: 根据id
         * UPDATE user SET name=? WHERE id=?
         */
        userService.updateById(new User().setName("hua").setId(2L));
    }

    @Test
    public void testUpdateBatchById() {
        List<User> userList = CollUtil.newArrayList(
                new User().setAge(27).setName("hua").setId(1L),
                new User().setAge(17).setName("zen").setId(2L)
        );

        /**
         * UPDATE user SET name=?, age=? WHERE id=?
         * 单类sql，批次提交，类似saveBatch。
         */
        userService.updateBatchById(userList);
        // 多个sql, 批次提交，类似saveBatch
//        userService.updateBatchById(userList,1);
    }
}
