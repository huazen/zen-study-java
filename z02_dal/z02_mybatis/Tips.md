## spring事务失效的大概原因

1. Bean没有交由Spring容器管理
2. 没有配置事务管理器，没有事务注解
3. 异常被捕获了
4. 抛出的异常与@Transactional设置要捕获的异常不符
5. 自身调用，在方法入口处没有事务注解，内部调用的方法才加的事务没有经过Spring代理
6. 数据库本身不支持事务
7. 事务注解配置以不支持事务的方式运行（@Transactional(propagation = Propagation.NOT_SUPPORTED)）
8. 方法不是public（官方文档：@Transactional 只能用于 public 的方法上，否则事务不会失效）
9. 事务管理器不对。在多数据源情况下，配置多个事务管理器，注意指定正确的事务管理器。