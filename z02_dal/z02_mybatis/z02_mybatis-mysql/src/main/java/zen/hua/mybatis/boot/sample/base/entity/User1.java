package zen.hua.mybatis.boot.sample.base.entity;

import lombok.Data;

/**
 * @author: ZEN
 * @create: 2024-09-07 20:29
 **/
@Data
public class User1 {

    private Long id;

    private String name;

    private String cardNo;

    private String hobby;

}
