package zen.hua.mybatis.boot.sample.base.mapper;

import org.apache.ibatis.annotations.Mapper;
import zen.hua.mybatis.boot.sample.base.entity.User1;

/**
 * @author: ZEN
 * @create: 2024-09-07 20:48
 **/
@Mapper
public interface User1Mapper {

    int upsert(User1 user1);
}
