package base.c1_curd;

import zen.hua.mybatis.boot.sample.base.entity.User1;
import zen.hua.mybatis.boot.sample.base.mapper.User1Mapper;

import javax.annotation.Resource;

/**
 * @author: ZEN
 * @create: 2024-09-07 20:51
 **/
public class T4_UpsertTest {

    @Resource
    private User1Mapper user1Mapper;


    /**
     * <pre>
     *     基于：on duplicate key update 语法实现
     *     问题：
     *     1. 唯一键冲突时，先走insert，又走了更新，自增id会加2。注意更新频率高的话可能id更快用完
     *     2. 容易死锁
     * </pre>
     */
    public void testUpsert1() {

        user1Mapper.upsert(new User1());
    }
}
