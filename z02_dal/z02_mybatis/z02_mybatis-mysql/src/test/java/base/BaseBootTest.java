package base;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import zen.hua.mybatis.boot.sample.base.SampleApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleApplication.class)
public class BaseBootTest {

}
