package transactional;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @program: zen-study-java
 * @description:
 * @author: HUA
 * @create: 2023-08-24 21:15
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TransactionalApplication.class)
public class BootTest {
}
