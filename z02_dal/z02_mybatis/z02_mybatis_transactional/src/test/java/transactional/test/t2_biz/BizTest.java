package transactional.test.t2_biz;

import cn.hutool.core.collection.CollUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import transactional.BootTest;
import transactional.entity.User;
import transactional.service.BaseService;

import java.util.List;

/**
 * @program: zen-study-java
 * @description:
 * @author: HUA
 * @create: 2023-08-24 21:16
 **/
public class BizTest extends BootTest {

    @Autowired
    private BaseService baseService;

    /**
     * 测试多线程池下的事务
     */
    @Test
    public void testMultiThreadRollback() {
        List<User> userList = CollUtil.newArrayList(
                new User().setId(1L).setName("111"),
                new User().setId(2L).setName("222")
        );

        baseService.multiThreadRollback(userList);
    }

    /**
     * 事务提交之后执行
     */
    @Test
    public void testCommitExecute() {
        // case1: 事务正常提交执行
        User user1 = new User().setId(1L).setName("111");
//        baseService.commitExecute(user1);

        // case2: 事务回滚后不执行
        User user2 = new User().setId(2L).setName("222");
//        baseService.commitExecute(user2);

        // case3: 不在事务中，却加了这个接口会报错
        // java.lang.IllegalStateException: Transaction synchronization is not active
        baseService.commitExecuteNoTx(user1);
    }


}
