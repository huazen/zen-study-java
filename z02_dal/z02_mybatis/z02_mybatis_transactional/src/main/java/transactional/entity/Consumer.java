package transactional.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @description:
 * @author: HUA
 * @create: 2023-03-13 20:56
 **/
@Data
@Accessors(chain = true)
public class Consumer implements Serializable {
    private Long id;
    private String name;
}
