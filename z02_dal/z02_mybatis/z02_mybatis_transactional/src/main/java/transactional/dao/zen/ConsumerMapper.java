package transactional.dao.zen;

import transactional.entity.Consumer;

/**
 * @description: 消费者mapper
 * @author: HUA
 * @create: 2023-03-13 20:59
 **/
public interface ConsumerMapper {


    boolean insertOne(Consumer consumer);

}
