package transactional.service;

import transactional.entity.User;

import java.util.List;

/**
 * @program: zen-study-java
 * @description:
 * @author: HUA
 * @create: 2023-08-24 21:06
 **/
public interface BaseService {


    /**
     * 多线程下事务回滚(尽量避免)
     *
     * @param userList
     */
    void multiThreadRollback(List<User> userList);


    /**
     * 事务提交后执行
     *
     * @param user
     */
    void commitExecute(User user);

    /**
     * 事务提交后执行，不开启事务，会报错
     */
    void commitExecuteNoTx(User user);
}
