package transactional.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.transaction.support.TransactionTemplate;
import transactional.dao.hua.UserMapper;
import transactional.entity.User;
import transactional.service.BaseService;
import transactional.util.MultiThreadingTransactionManager;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @program: zen-study-java
 * @description:
 * @author: HUA
 * @create: 2023-08-24 21:12
 **/
@Service
public class BaseServiceImpl implements BaseService {

    @Autowired
    private UserMapper userMapper;

    @Resource(name = "huaTransactionTemplate")
    private TransactionTemplate transactionTemplate;

    @Resource(name = "huaTransactionManager")
    private DataSourceTransactionManager transactionManager;

    @Override
    public void multiThreadRollback(List<User> userList) {

        // 创建线程池
        ExecutorService executor = Executors.newFixedThreadPool(5);
        // 创建多线程事务管理器，传入事务管理器，指定超时时间为3秒
        MultiThreadingTransactionManager multiThreadingTransactionManage =
                new MultiThreadingTransactionManager(transactionManager, 3, TimeUnit.SECONDS);
        List<Runnable> runnableList = new ArrayList<>();

        userList.forEach((x) -> {
            runnableList.add(new Runnable() {
                @Override
                public void run() {
                    userMapper.insertOne(x);
//                    if(x.getId()%2==0) {
//                        throw new RuntimeException("异常测试");
//                    }
                }
            });
        });
        // 执行
        multiThreadingTransactionManage.execute(runnableList, executor);

    }

    @Override
    @Transactional(transactionManager = "huaTransactionManager", rollbackFor = Exception.class)
    public void commitExecute(User user) {
        userMapper.insertOne(user);
        if (user.getId() % 2 == 0) {
            throw new RuntimeException("异常测试");
        }

        //事物提交之后执行，必须放在事务里面
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCommit() {
                System.out.println("事物提交之后执行：" + user.getId());
            }
        });
    }

    @Override
    public void commitExecuteNoTx(User user) {
        userMapper.insertOne(user);
        if (user.getId() % 2 == 0) {
            throw new RuntimeException("异常测试");
        }

        //事物提交之后执行，必须放在事务里面
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCommit() {
                System.out.println("事物提交之后执行：" + user.getId());
            }
        });
    }

}
