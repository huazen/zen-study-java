-- 建库(建议两个数据库)
create database hua character set utf8mb4;

-- 建表
-- 用户表
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `id`   INT(11) COMMENT '主键',
    `name` VARCHAR(128) COMMENT '用户名'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='用户表';

-- 消费者表
DROP TABLE IF EXISTS `consumer`;
CREATE TABLE `consumer`
(
    `id`   bigint COMMENT '主键',
    `name` VARCHAR(128) COMMENT '用户名'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='消费者表';
