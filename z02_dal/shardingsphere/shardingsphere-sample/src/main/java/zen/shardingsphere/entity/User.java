package zen.shardingsphere.entity;

import lombok.Data;

/**
 * @author: ZEN
 * @create: 2024-04-05 13:09
 **/
@Data
public class User {

    private Long id;

    private String name;

    private Integer age;

    private String email;
}
