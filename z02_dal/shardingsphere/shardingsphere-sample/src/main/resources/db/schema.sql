-- 三个数据库都创建
CREATE TABLE If Not Exists user
(
    id BIGINT
(
    20
) NOT NULL COMMENT '主键ID',
    name VARCHAR
(
    30
) NULL DEFAULT NULL COMMENT '姓名',
    age INT
(
    11
) NULL DEFAULT NULL COMMENT '年龄',
    email VARCHAR
(
    50
) NULL DEFAULT NULL COMMENT '邮箱',
    PRIMARY KEY
(
    id
)
    );

-- 分别建在三个不同数据库中
CREATE TABLE If Not Exists user_0
(
    id BIGINT
(
    20
) NOT NULL COMMENT '主键ID',
    name VARCHAR
(
    30
) NULL DEFAULT NULL COMMENT '姓名',
    age INT
(
    11
) NULL DEFAULT NULL COMMENT '年龄',
    email VARCHAR
(
    50
) NULL DEFAULT NULL COMMENT '邮箱',
    PRIMARY KEY
(
    id
)
    );
CREATE TABLE If Not Exists user_1
(
    id BIGINT
(
    20
) NOT NULL COMMENT '主键ID',
    name VARCHAR
(
    30
) NULL DEFAULT NULL COMMENT '姓名',
    age INT
(
    11
) NULL DEFAULT NULL COMMENT '年龄',
    email VARCHAR
(
    50
) NULL DEFAULT NULL COMMENT '邮箱',
    PRIMARY KEY
(
    id
)
    );
CREATE TABLE If Not Exists user_2
(
    id BIGINT
(
    20
) NOT NULL COMMENT '主键ID',
    name VARCHAR
(
    30
) NULL DEFAULT NULL COMMENT '姓名',
    age INT
(
    11
) NULL DEFAULT NULL COMMENT '年龄',
    email VARCHAR
(
    50
) NULL DEFAULT NULL COMMENT '邮箱',
    PRIMARY KEY
(
    id
)
    );