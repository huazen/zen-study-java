package zen.shardingsphere.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.shardingsphere.driver.api.ShardingSphereDataSourceFactory;
import org.apache.shardingsphere.infra.config.algorithm.AlgorithmConfiguration;
import org.apache.shardingsphere.readwritesplitting.api.ReadwriteSplittingRuleConfiguration;
import org.apache.shardingsphere.readwritesplitting.api.rule.ReadwriteSplittingDataSourceRuleConfiguration;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;


/**
 * shardingsphere 读写分离
 *
 * @author: ZEN
 * @create: 2024-04-05 14:15
 * url: https://shardingsphere.apache.org/document/current/cn/user-manual/shardingsphere-jdbc/java-api/rules/readwrite-splitting/
 **/
@Component
public class LogisticRsShardingDataSource {

    public static final String DRIVER_CLZ = "com.mysql.cj.jdbc.Driver";

    public static final String DB_URL_MODULE = "jdbc:mysql://localhost:%s/%s?useUnicode=true&characterEncoding=UTF-8&serverTimeZone=UTC";

    public DataSource buildDataSource() throws SQLException {
        // case1: 配置读写分离
        // 1 添加读写分离数据源
        ReadwriteSplittingDataSourceRuleConfiguration dataSourceConfig =
                new ReadwriteSplittingDataSourceRuleConfiguration(
                        "logisticDataSource", "master",
                        Arrays.asList("salve1", "salve2"), "zen_fee_lb");


        // 2 设置负载均衡算法
        Properties algorithmProps = new Properties();
        algorithmProps.setProperty("salve1", "2");
        algorithmProps.setProperty("salve2", "1");
        Map<String, AlgorithmConfiguration> algorithmConfigMap = new HashMap<>(1);
        algorithmConfigMap.put("zen_fee_lb", new AlgorithmConfiguration("WEIGHT", algorithmProps));

        // 3 使用读写分离数据源
        ReadwriteSplittingRuleConfiguration ruleConfig = new ReadwriteSplittingRuleConfiguration(Collections.singleton(dataSourceConfig), algorithmConfigMap);
        Properties props = new Properties();
        props.setProperty("sql-show", Boolean.TRUE.toString());
        return ShardingSphereDataSourceFactory.createDataSource(createDataSourceMap(), Collections.singleton(ruleConfig), props);
    }

    /**
     * 实际场景下自定义
     *
     * @return
     */
    private Map<String, DataSource> createDataSourceMap() {
        Map<String, DataSource> result = new HashMap<>(3, 1);
        result.put("master", createDruidDataSource(3306, "zen_fee_logistic"));
        result.put("salve1", createDruidDataSource(3308, "zen_fee_logistic"));
        result.put("salve2", createDruidDataSource(3309, "zen_fee_logistic"));
        return result;
    }


    private DataSource createDruidDataSource(Integer port, String dbName) {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(DRIVER_CLZ);
        dataSource.setUrl(String.format(DB_URL_MODULE, port, dbName));
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        // ... 其他DruidDataSource配置项
        return dataSource;
    }

}
