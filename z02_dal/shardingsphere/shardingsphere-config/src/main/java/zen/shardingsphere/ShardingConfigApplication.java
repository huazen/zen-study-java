package zen.shardingsphere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: ZEN
 * @create: 2024-04-05 13:08
 **/
@SpringBootApplication
public class ShardingConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShardingConfigApplication.class, args);
    }
}
