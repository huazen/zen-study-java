package zen.shardingsphere.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.shardingsphere.broadcast.api.config.BroadcastRuleConfiguration;
import org.apache.shardingsphere.driver.api.ShardingSphereDataSourceFactory;
import org.apache.shardingsphere.infra.config.algorithm.AlgorithmConfiguration;
import org.apache.shardingsphere.sharding.api.config.ShardingRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.rule.ShardingTableRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.strategy.sharding.StandardShardingStrategyConfiguration;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;

/**
 * <pre>
 *     基于javaAPI搭建的sharding数据源
 * <pre/>
 * @author: ZEN
 * @create: 2024-04-05 17:22
 **/
@Component
public class LogisticShardingDataSource {

    /**
     * 【核心】创建数据源
     */
    public DataSource buildDataSource() throws SQLException {
        return ShardingSphereDataSourceFactory.createDataSource(
                createDataSourceMap(), // 数据源
                Arrays.asList(createShardingRuleConfiguration(),
                        createBroadcastRuleConfiguration()), new Properties());
    }


    /**
     * 配置分表规则
     *
     * @return
     */
    private ShardingRuleConfiguration createShardingRuleConfiguration() {
        ShardingRuleConfiguration result = new ShardingRuleConfiguration();
        // 1 分片表规则列表，可添加多张表的分表规则
        result.getTables().add(getOrderItemTableRuleConfiguration());
        // 2 绑定表规则列表 ，表之间存在关系。
//        result.getBindingTableGroups().add(new ShardingTableReferenceRuleConfiguration("foo", "t_order, t_order_item"));
        // 3 默认分库策略 param1: 分表键，多个列用逗号隔开；param2: 分片算法名称
        result.setDefaultDatabaseShardingStrategy(new StandardShardingStrategyConfiguration("email", "ex_1"));
        // 4 默认分表策略，参数作用同上
        result.setDefaultTableShardingStrategy(new StandardShardingStrategyConfiguration("email", "ex_2"));

        // 5 分片算法配置
        /*
            param1: 分片算法名称自定义，与上面相对即可；
            param2: 分片算法配置
                param： 算法类型，见参考文档
                param: Properties类型，key是算法类型所允许的属性名。
         */
        Properties props = new Properties();
        props.setProperty("algorithm-expression", "db_${Math.abs(email.hashCode() % 3)}");
        result.getShardingAlgorithms().put("ex_1", new AlgorithmConfiguration("INLINE", props));
        Properties props2 = new Properties();
        props2.setProperty("algorithm-expression", "user_${email.length() % 3}");
        result.getShardingAlgorithms().put("ex_2", new AlgorithmConfiguration("INLINE", props2));

        // 6 自定义生成算法
        result.getKeyGenerators().put("snowflake", new AlgorithmConfiguration("SNOWFLAKE", new Properties()));
        // 7 分片审计算法名称和配置
        result.getAuditors().put("sharding_key_required_auditor", new AlgorithmConfiguration("DML_SHARDING_CONDITIONS", new Properties()));
        return result;
    }


    /**
     * 配置分表规则
     *
     * @return
     */
    private ShardingTableRuleConfiguration getOrderItemTableRuleConfiguration() {
        /*
            param1：分片逻辑表名称
            para2: 分片算法表达式, 由数据源名 + 表名组成, 以小数点分隔。
                    多个表以逗号分隔，支持行表达式
         */
        ShardingTableRuleConfiguration result = new ShardingTableRuleConfiguration("user", "db_${0..2}.user_${[0,1,2]}");
        // 配置自增列生成器
//        result.setKeyGenerateStrategy(new KeyGenerateStrategyConfiguration("id", "snowflake"));
//        result.setTableShardingStrategy(new StandardShardingStrategyConfiguration("email", "ex_1"));
        return result;
    }

    private BroadcastRuleConfiguration createBroadcastRuleConfiguration() {
        return new BroadcastRuleConfiguration(Collections.singletonList("t_address"));
    }

    /**
     * 实际场景下自定义
     *
     * @return
     */
    private Map<String, DataSource> createDataSourceMap() {
        Map<String, DataSource> result = new HashMap<>(3, 1);
        // key: 自定义库名，用于数据分片
        result.put("db_0", createDruidDataSource(3306, "zen_fee_logistic"));
        result.put("db_1", createDruidDataSource(3308, "zen_fee_logistic"));
        result.put("db_2", createDruidDataSource(3309, "zen_fee_logistic"));
        return result;
    }


    private DataSource createDruidDataSource(Integer port, String dbName) {
        String DRIVER_CLZ = "com.mysql.cj.jdbc.Driver";
        String DB_URL_MODULE = "jdbc:mysql://localhost:%s/%s?useUnicode=true&characterEncoding=UTF-8&serverTimeZone=UTC";
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(DRIVER_CLZ);
        dataSource.setUrl(String.format(DB_URL_MODULE, port, dbName));
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        // ... 其他DruidDataSource配置项
        return dataSource;
    }
}
