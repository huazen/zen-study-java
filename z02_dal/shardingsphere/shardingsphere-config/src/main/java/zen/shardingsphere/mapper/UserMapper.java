package zen.shardingsphere.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import zen.shardingsphere.entity.User;

public interface UserMapper extends BaseMapper<User> {
}