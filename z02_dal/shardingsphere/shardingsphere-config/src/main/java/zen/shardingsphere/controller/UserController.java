package zen.shardingsphere.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import zen.shardingsphere.entity.User;
import zen.shardingsphere.mapper.UserMapper;
import zen.shardingsphere.service.UserService;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;


    @GetMapping("findAll")
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("findPage")
    public List<User> findPage() {
        return userService.findPage();
    }

    @GetMapping("insert")
    public User insert(@RequestParam("name") String name,
                       @RequestParam("age") Integer age) {
        User user = new User();
        user.setName(name);
        user.setAge(age);
        user.setEmail(name + "@qq.com");
        userMapper.insert(user);
        return user;
    }


    @GetMapping("deleteById")
    public void deleteById(@RequestParam("id") Long id) {
        userService.deleteById(id);
    }


    @GetMapping("deleteAll")
    public void deleteAll() {
        userService.deleteAll();
    }
}
