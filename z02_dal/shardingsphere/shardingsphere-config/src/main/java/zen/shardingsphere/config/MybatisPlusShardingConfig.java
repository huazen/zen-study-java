package zen.shardingsphere.config;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

/**
 * <pre>
 *     数据源配置
 *     1. 整合了MybatisPlus
 *     2. 整合了ShardingSphere
 * </pre>
 **/
@Configuration
// 事务生效
@EnableTransactionManagement
// 扫描指定包下的mapper接口，并为其指定sqlSession工厂，多数据源下
@MapperScan(basePackages = MybatisPlusShardingConfig.BASE_PACKAGE,
        sqlSessionFactoryRef = "logisticSqlSessionFactory")
public class MybatisPlusShardingConfig {

    public static final String BASE_PACKAGE = "zen.shardingsphere.mapper";

    /**
     * mapper文件路径
     */
//    @Value("${logistic.mybatis.mapper.location}")
    private Resource[] mapperLocations;
    /**
     * mybatis配置
     */
//    @Value("${logistic.mybatis.config-location}")
    private Resource configLocation;
    /**
     * 别名的包名
     */
//    @Value("${logistic.mybatis.typealiases.base.package}")
    private String typeAliasesPackage;

    @Autowired
    private LogisticShardingDataSource logisticShardingDataSource;

    /**
     * 创建数据源
     * <pre>
     *     整合ShardingSphere, 创建一个包含分片规则的ShardingSphere数据源
     * </pre>
     */
    @Primary
    @Bean(name = "logisticDataSource")
    public DataSource dataSource() throws Exception {
        // 方式1: 通过YAML文件创建数据源
//        File yamlFile = ResourceUtils.getFile("classpath:logistic-shardingsphere.yaml");
//        return YamlShardingSphereDataSourceFactory.createDataSource(yamlFile);

        // 方式2: 通过JavaAPI创建数据源
        return logisticShardingDataSource.buildDataSource();
    }


    @Primary
    @Bean(name = "logisticJdbcTemplate")
    public JdbcTemplate jdbcTemplate(@Qualifier("logisticDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    /**
     * 创建事务管理器
     *
     * @param dataSource
     * @return
     */
    @Primary
    @Bean(name = "logisticTransactionManager")
    public DataSourceTransactionManager transactionManager(@Qualifier("logisticDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    /**
     * 创建事务模板
     *
     * @param transactionManger
     * @return
     */
    @Primary
    @Bean(name = "logisticTransactionTemplate")
    public TransactionTemplate transactionTemplate(@Qualifier("logisticTransactionManager") DataSourceTransactionManager transactionManger) {
        return new TransactionTemplate(transactionManger);
    }

    /**
     * 创建 sqlSessionFactory
     * <pre>
     *     mybatis配置类，改造成mybatis-plus配置类，核心只需改造 SqlSessionFactory
     * </pre>
     */
    @Primary
    @Bean(name = "logisticSqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory(@Qualifier("logisticDataSource") DataSource dataSource,
                                               @Qualifier("logisticMybatisPlusInterceptor") MybatisPlusInterceptor mybatisPlusInterceptor) throws Exception {
        MybatisSqlSessionFactoryBean factoryBean = new MybatisSqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
//        factoryBean.setConfigLocation(configLocation);
//        factoryBean.setMapperLocations(mapperLocations);

        // 配置类
        MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
        factoryBean.setConfiguration(mybatisConfiguration);
        mybatisConfiguration.setLogImpl(StdOutImpl.class); // 打印日志

        // 全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        factoryBean.setGlobalConfig(globalConfig);


        // 注册MybatisPlusInterceptor，注册拦截器
        factoryBean.setPlugins(new Interceptor[]{mybatisPlusInterceptor});

        return factoryBean.getObject();
    }

    @Bean("logisticMybatisPlusInterceptor")
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        // 分页拦截器
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }


}
