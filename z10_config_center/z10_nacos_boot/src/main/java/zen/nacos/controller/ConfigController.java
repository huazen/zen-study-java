package zen.nacos.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("nacos")
public class ConfigController {

    /**
     * boot时，这里autoRefreshed配置成true
     */
    @NacosValue(value = "${zen.name:defaultValue}", autoRefreshed = true)
    private String name;

    @Resource
    private ExtConfig extConfig;

    @GetMapping("/config")
    public String getConfig() {
        return name;
    }

    @GetMapping("/extConfig")
    public ExtConfig getExtConfig() {
        return extConfig;
    }
}