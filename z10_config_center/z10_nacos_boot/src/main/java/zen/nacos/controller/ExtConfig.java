package zen.nacos.controller;

import com.alibaba.nacos.api.config.ConfigType;
import com.alibaba.nacos.api.config.annotation.NacosConfigurationProperties;
import lombok.Data;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * 1. @NacosConfigurationProperties、ext-config、nacos页面配置三者要保持一致
 * 2. 本处测试两个地方：
 *      1. prefix
 *      2. 读取其他data-id配置
 * </pre>
 *
 * @author ZEN
 * @since 2025-03-06 22:02
 **/
@Data
@Configuration
@NacosConfigurationProperties(dataId = "zen-common-config", prefix = "zen.common",
        autoRefreshed = true, type = ConfigType.PROPERTIES)
public class ExtConfig {

    private String name;

    private Integer age;

    private String sex;
}
