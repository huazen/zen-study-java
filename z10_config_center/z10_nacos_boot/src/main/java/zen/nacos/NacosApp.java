package zen.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ZEN
 * @since 2025-03-06 20:22
 **/
@SpringBootApplication
public class NacosApp {

    public static void main(String[] args) {
        SpringApplication.run(NacosApp.class, args);
    }
}
