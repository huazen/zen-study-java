### 1 下载安装

linux安装

```shell
git clone https://github.com/jenv/jenv.git ~/.jenv
```

windows安装 (需通过 WSL 或手动安装）
下载 https://github.com/jenv/jenv-for-windows 并配置环境变量。

### 2 mac安装

#### 2.1 安装

mac安装， 推荐homebrew

```shell
brew install jenv
```

#### 2.2 配置

将 jEnv 添加到 Shell 环境变量

```shell
echo 'export PATH="$HOME/.jenv/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(jenv init -)"' >> ~/.zshrc
source ~/.zshrc  # 使配置生效
```

> 1. jenv has been updated, process to refresh plugin links,
     > 解决：关闭并重新打开终端

有些情况下， 因为有其他未被 jEnv 覆盖的java配置，导致jenv global不生效

```shell
# 解决方案：启用插件，jEnv 的 export 插件可自动设置 JAVA_HOME，无需手动配置
jenv enable-plugin export

# 是否有其他java命令， 启动插件后是否还有其他java命令
which -a java
```

#### 2.3 添加jdk

```shell
jenv add /Library/Java/JavaVirtualMachines/jdk-11.jdk/Contents/Home
jenv add /usr/lib/jvm/java-8-openjdk-amd64
```

#### 2.4 切换jdk

```shell
# 全局默认版本
jenv global 17.0.1
# 切换当前目录(项目)版本
jenv local 17.0.1  # 生成 .java-version 文件
# 切换当前shell版本
jenv shell 11.0.13  # 仅当前终端生效
```