### 1 mac安装

#### 1 下载文件

https://github.com/apolloconfig/apollo
从Release下载三个核心JAR包 ：
apollo-configservice-*.jar
apollo-adminservice-*.jar
apollo-portal-*.jar
将JAR包放置于同一目录，例如 ~/apollo。

#### 2 下载数据库脚本

下载sql脚本
在源码的 scripts/sql 目录下（适用于旧版本，如1.8.x）
或 scripts/db/migration/configdb/V1.0.0__initialization.sql（适用于1.3.0及以上版本）

新建两个数据库ApolloConfigDB、ApolloPortalDB，执行apolloconfigdb.sql、apolloportaldb.sql。

#### 3 启动服务

依次执行Config Service、Admin Service、Portal

```shell
# 启动Config Service
unzip apollo-configservice-2.4.0-github.zip
cd apollo-configservice-2.4.0-github
nohup java -Xms256m -Xmx256m \
  -Dspring.datasource.url="jdbc:mysql://localhost:3306/ApolloConfigDB?characterEncoding=utf-8&serverTimezone=GMT" \
  -Dspring.datasource.username=root \
  -Dspring.datasource.password= \
  -jar apollo-configservice.jar > config.log 2>&1 &
  
# 启动Admin Service 
unzip apollo-adminservice-2.4.0-github.zip
cd apollo-adminservice-2.4.0-github
nohup java -Xms256m -Xmx256m \
  -Dspring.datasource.url="jdbc:mysql://localhost:3306/ApolloConfigDB?characterEncoding=utf-8&serverTimezone=GMT" \
  -Dspring.datasource.username=root \
  -Dspring.datasource.password= \
  -Dserver.port=8090 \
  -jar apollo-adminservice.jar > admin.log 2>&1 &  
  
### 启动Portal
unzip apollo-portal-2.4.0-github.zip
cd apollo-portal-2.4.0-github
nohup java -Xms256m -Xmx256m \
  -Dspring.datasource.url="jdbc:mysql://localhost:3306/ApolloPortalDB?characterEncoding=utf-8&serverTimezone=GMT" \
  -Dspring.datasource.username=root \
  -Dspring.datasource.password= \
  -Ddev_meta=http://localhost:8080/ \
  -jar apollo-portal.jar > portal.log 2>&1 &  
```

遇到问题：java.io.FileNotFoundException: /opt/logs/apollo-configservice.log (Permission denied)

```shell
# 创建目录（需管理员权限）
sudo mkdir -p /opt/logs

# 将目录所有权赋予当前用户（如hua）
sudo chown -R $(whoami):staff /opt/logs

# 验证权限
ls -ld /opt/logs  # 应显示用户为hua，权限为drwxr-xr-x
```

#### 4 验证

```shell
## mac查看进程
ps aux | grep 'apollo-.*.jar'  # 应看到3个Java进程
```

浏览器打开：http://localhost:8070
默认账号：apollo / 密码：admin

```shell
tail -f config.log  # 检查是否有"Started ApolloConfigService"日志
```

### 2 docker安装

下载docker-compose.yml和 [sql文件夹](https://github.com/apolloconfig/apollo-quick-start/tree/master/sql) 到本地目录，如
docker-quick-start。
> 如果使用的是 arm 架构的机器，例如 mac
>
m1，需要下载[docker-compose-arm64.yml](https://github.com/apolloconfig/apollo-quick-start/blob/master/docker-compose-arm64.yml)

在docker-quick-start目录下执行docker-compose up，第一次执行会触发下载镜像等操作，需要耐心等待一些时间。
> 如果使用的是 arm 架构的机器，例如 mac m1，执行 docker-compose -f docker-compose-arm64.yml up
> 注意：sql文件夹的位置