#### 01_安装

```shell
### 1 准备JDK1.8+
java -version
brew install openjdk@8

### 2 下载nacos
cd ~/Downloads
wget https://github.com/alibaba/nacos/releases/download/2.5.0/nacos-server-2.5.0.tar.gz
### 3 解压并移动到系统目录
tar -zxvf nacos-server-2.5.0.tar.gz
mv nacos /usr/local/  # 推荐移动到系统目录

### 3 配置调优可选
# 端口被占，修改conf/application.properties
server.port=8858
# 数据库配置：替换嵌入式数据库为MySQL（生产环境建议）：
# 创建MySQL数据库nacos并执行conf/nacos-mysql.sql
# 修改application.properties中的数据库连接信息

### 4 单机启动
cd /usr/local/nacos/bin
sh startup.sh -m standalone
```

#### 02_mac卸载

```shell
### 1 停止服务
cd /usr/local/nacos/bin
sh shutdown.sh
### 2 删除文件
sudo rm -rf /usr/local/nacos
### 3 深度清理
## 配置文件清理
rm -rf ~/Library/Application\ Support/nacos
rm -rf ~/Library/Caches/nacos
rm -rf ~/Library/Preferences/com.alibaba.nacos.plist
## 日志清理
rm -rf ~/Library/Logs/nacos
### 4 数据库清理按需
DROP DATABASE nacos;
```







