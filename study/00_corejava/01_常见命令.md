运行jar，指定出现OOM时，打印堆栈信息

```shell
nohup java -Xms200m -Xmx200m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/usr/jdk/dump \
 -jar integation-mybatis-0.0.1-SNAPSHOT.jar > /usr/jdk/error.log & 
```
