# 一、基础

## 1 是什么

### 1 概述

Dockerfile是用来构建Docker镜像的文本文件，是由一条条构建镜像所需的指令和参数构成的脚本。
[官网](https://docs.docker.com/engine/reference/builder/)

### 2 构建三步骤

1. 编写Dockerfile文件。
2. docker build命令构建镜像
3. docker run依镜像运行容器实例

## 2 构建过程

### 1 基础知识

1. 每条保留字指令都必须为大写字母且后面要跟随至少一个参数。
2. 指令按照从上到下，顺序执行。
3. # 表示注释
4. 每条指令都会创建一个新的镜像层并对镜像进行提交

### 2 执行流程

1. docker从基础镜像运行一个容器。
2. 执行一条指令并对容器作出修改。
3. 执行类似docker commit的操作提交一个新的镜像层。
4. docker再基于刚提交的镜像运行一个新容器。
5. 执行dockerfile中的下一条指令直到所有指令都执行完成。

### 3 docker体系

从应用软件的角度来看，Dockerfile、Docker镜像与Docker容器分别代表软件的三个不同阶段，

* Dockerfile是软件的**原材料**
* Docker镜像是软件的**交付品**
* Docker容器则可以认为是软件镜像的运行态，也即依照镜像运行的容器实例

Dockerfile面向开发，Docker镜像成为交付标准，Docker容器则涉及部署与运维，三者缺一不可，合力充当Docker体系的基石。

1.
Dockerfile，需要定义一个Dockerfile，Dockerfile定义了进程需要的一切东西。Dockerfile涉及的内容包括执行代码或者是文件、环境变量、依赖包、运行时环境、动态链接库、操作系统的发行版、服务进程和内核进程(
当应用进程需要和系统服务和内核进程打交道，这时需要考虑如何设计namespace的权限控制)等等;
2. Docker镜像，在用Dockerfile定义一个文件之后，docker build时会产生一个Docker镜像，当运行 Docker镜像时会真正开始提供服务;
3. Docker容器，容器是直接提供服务的。

## 3 常用保留字指令

参考tomcat8的dockerfile[入门](https://github.com/docker-library/tomcat)

### 1 FROM

基础镜像，当前新镜像是基于哪个镜像的，指定一个已经存在的镜像作为模板，第一条必须是from。

```shell
FROM amazoncorretto:8-al2-jdk
```

### 2 MAINTAINER

镜像维护者的姓名和邮箱地址。

```shell
## LABEL MAINTAINER
LABEL MAINTAINER ：添加镜像的作者
```

### 3 RUN

容器构建时需要运行的命令，RUN是在 docker build时运行。

**两种格式**

1. shell格式

```shell
# <命令行命令> 等同于  在终端操作的shell命令
RUN <命令行命令>
# 示例
RUN yum -y install vim
```

2. exec格式

```shell
RUN ["可执行文件","参数","参数"]
# 例如
RUN ["./test.php","dev","offline"]
# 等价于
RUN ./test.php dev offline
```

### 4 EXPOSE

当前容器对外暴露出的端口。

- 帮助镜像使用者理解这个镜像服务的守护端口，以方便配置映射。
- 在运行时使用随机端口映射时，也就是 docker run -P 时，会自动随机映射 EXPOSE 的端口

```shell
# 语法： 
EXPOSE <端口1> [<端口2>...]
# 示例
EXPOSE 8080
```

### 5 WORKDIR

指定在创建容器后，终端默认登陆的进来工作目录，一个落脚点

```shell
ENV CATALINA_HOME /usr/local/tomcat
WORKDIR $CATALINA_HOME
```

### 6 USER

用于指定执行后续命令的用户和用户组，这边只是切换后续命令执行的用户（用户和用户组必须提前已经存在）。如果都不指定，默认是root。

```shell
语法：
USER <用户名>[:<用户组>]
```

### 7 ENV

用来在构建镜像过程中设置环境变量。

```shell
ENV MY_PATHENV MY_PATH /usr/mytest
```

1. 环境变量可以在后续的任何RUN指令中使用，这就如同在命令前面指定了环境变量前缀一样；
2. 也可以在其它指令中直接使用这些环境变量，比如：WORKDIR $MY_PATH

### 8 ARG

ARG ：构建参数，与 ENV 作用一致。不过作用域不一样。ARG 设置的环境变量仅对 Dockerfile 内有效，也就是说只有 docker build
的过程中有效，构建好的镜像内不存在此环境变量。

```shell
ARG <参数名>[=<默认值>]
```

### 8 ADD

将宿主机目录下的文件拷贝进镜像且会自动处理URL和解压tar压缩包。

### 9 COPY

类似ADD，拷贝文件和目录到镜像中。
将从构建上下文目录中 `<源路径>` 的文件/目录复制到新的一层的镜像内的 `<目标路径>` 位置。

```shell
# <src源路径>：源文件或者源目录
# <dest目标路径>：容器内的指定路径，该路径不用事先建好，路径不存在的话，会自动创建。

# 方式1
COPY src dest
# 方式2
COPY ["src", "dest"]
```

### 10 VOLUME

容器数据卷，用于数据保存和持久化工作。

### 11  CMD

指定容器启动后的要干的事情。类似于 RUN 指令，用于运行程序，但二者运行的时间点不同:

CMD 在docker run 时运行。
RUN 是在 docker build。

作用：为启动的容器指定默认要运行的程序，程序运行结束，容器也就结束。CMD 指令指定的程序可被 docker run 命令行参数中指定要运行的程序所覆盖。
注意：如果 Dockerfile 中如果存在多个 CMD 指令，仅最后一个生效。

语法：

```shell
CMD <shell 命令> 
CMD ["<可执行文件或命令>","<param1>","<param2>",...] 
CMD ["<param1>","<param2>",...]  # 该写法是为 ENTRYPOINT 指令指定的程序提供默认参数
```

演示覆盖操作

```shell
# 1 不加参数
docker run -it -p 8080:8080 30ef4019761d
# CMD ["catalina.sh","run"] 生效
# http://localhost:8080/ 可以正常访问

# 2 docker run 加参数
docker run -it -p 8080:8080 30ef4019761d  /bin/bash
# CMD ["catalina.sh","run"] 被替换为 CMD ["/bin/bash","run"]
# http://localhost:8080/ 访问后报该网页无法正常工作
```

### 12 ENTRYPONT

类似于 CMD 指令，但其**不会被 docker run 的命令行参数指定的指令所覆盖**，而且这些命令行参数会被**当作参数送给 ENTRYPOINT
指令指定的程序**。

但是, 如果运行 docker run 时使用了 --entrypoint 选项，将覆盖 ENTRYPOINT 指令指定的程序。

优点：在执行 docker run 的时候可以指定 ENTRYPOINT 运行所需的参数。

注意：如果 Dockerfile 中如果存在多个 ENTRYPOINT 指令，仅最后一个生效。

语法：

```shell
ENTRYPOINT <shell 命令>
ENTRYPOINT ["<executeable>","<param1>","<param2>",...]
```

案例如下：假设已通过 Dockerfile 构建了 nginx:test 镜像：

```shell
FORM nginx
ENTRYPOINT ["nginx","-c"] # 定参
CMD ["/etc/nginx/nginx.conf"] # 变参
```

| 是否传参     | 按照dockerfile编写执行               | 传参运行                                                                         |
|----------|--------------------------------|------------------------------------------------------------------------------|
| Docker命令 | docker run  nginx:test         | docker run  nginx:test -c /etc/nginx/<span style='color:red'>new.conf</span> |
| 衍生出的实际命令 | nginx -c /etc/nginx/nginx.conf | nginx -c /etc/nginx/<span style='color:red'>new.conf</span>                  |

### 13 ONBUILD

ONBUILD ：用于延迟构建命令的执行。简单的说，就是 Dockerfile 里用 ONBUILD 指定的命令，在本次构建镜像的过程中不会执行（假设镜像为
test-build）。当有新的 Dockerfile 使用了之前构建的镜像 FROM test-build ，这时执行新镜像的 Dockerfile 构建时候，会执行
test-build 的 Dockerfile 里的 ONBUILD 指定的命令。

```shell
# 语法：
ONBUILD <其它指令>
```

## 4 案例

### 1 自定义镜像mycentosjava8

#### 1 要求&准备

要求：Centos7镜像具备vim+ifconfig+jdk8
下载地址：

1. [官网下载地址](https://www.oracle.com/java/technologies/downloads/#java8)

   ![image-20230127075023405](../../../../assets/image-20230127075023405.png)

   根据不同硬件系统环境，选择不同的版本。

2. [镜像地址](https://mirrors.yangxingzhen.com/jdk/)

   ![image-20230126200804969](../../../../assets/image-20230126200804969-4734888.png)

   准备

   ```shell
   # 1. 准备好linux镜像
   #  centos8 已经停止维护了，使用centos7
   docker pull centos:centos7
   docker images
   # ==================================
   REPOSITORY              TAG       IMAGE ID       CREATED         SIZE
   centos                  centos7   c9a1fdca3387   11 months ago   301MB
   # ==================================
   # 2 jdk 镜像
   hua@HuaMac base % ls
   jdk-8u361-linux-aarch64.tar.gz
   hua@HuaMac base % pwd
   /Users/hua/Documents/HuaAll/docker_data/dockerfile/base
   ```

#### 2 编写

编写Dockerfile文件

```shell
# jdk同级目录下执行，纯粹方便使用。
# 注意Dockerfile 文件名
vim Dockerfile
```

Dockerfile内容
> 根据实际情况进行修改，情况如下。

```shell
# 底层镜像
FROM centos:centos7
# 指定作者邮箱
MAINTAINER hua
# 设置环境变量
ENV MYPATH /usr/local
# 终端默认登陆进来的工作目录
WORKDIR $MYPATH

#安装vim编辑器
RUN yum -y install vim
#安装ifconfig命令查看网络IP
RUN yum -y install net-tools

#安装java8及lib库
# x86架构下使用
RUN yum -y install glibc.i686
# aarch64架构下修改为
RUN yum -y install glibc

RUN mkdir /usr/local/java
#ADD 是相对路径jar,把jdk-8u171-linux-x64.tar.gz添加到容器中,安装包必须要和Dockerfile文件在同一位置
ADD jdk-8u361-linux-aarch64.tar.gz /usr/local/java/

#配置java环境变量
ENV JAVA_HOME /usr/local/java/jdk1.8.0_361
ENV JRE_HOME $JAVA_HOME/jre
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH
ENV PATH $JAVA_HOME/bin:$PATH

# 暴露端口
EXPOSE 80

CMD echo $MYPATH
CMD echo "success--------------ok"
CMD /bin/bash
```

#### 3 构建

```shell
# TAG与.之间有个空格
docker build -t 新镜像名字:TAG .
```

**演示**

```shell
# 准备好jdk安装包 和 Dockerfile
hua@HuaMac base % ls
Dockerfile jdk-8u361-linux-aarch64.tar.gz

# 执行构建
hua@HuaMac base % docker build -t centosjava8:1.5 .

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
hua@HuaMac base % docker build -t centosjava8:1.5 .

[+] Building 17.4s (12/12) FINISHED                                             

 => [internal] load build definition from Dockerfile                       0.0s

 => => transferring dockerfile: 881B                                       0.0s

 => [internal] load .dockerignore                                          0.0s

 => => transferring context: 2B                                            0.0s

 => [internal] load metadata for docker.io/library/centos:centos7          0.0s

 => [internal] load build context                                          0.0s

 => => transferring context: 427B                                          0.0s

 => [1/7] FROM docker.io/library/centos:centos7                            0.0s

 => CACHED [2/7] WORKDIR /usr/local                                        0.0s

 => CACHED [3/7] RUN yum -y install vim                                    0.0s

 => CACHED [4/7] RUN yum -y install net-tools                              0.0s

 => [5/7] RUN yum -y install glibc                                        15.7s

 => [6/7] RUN mkdir /usr/local/java                                        0.3s

 => [7/7] ADD jdk-8u361-linux-aarch64.tar.gz /usr/local/java/              0.8s 

 => exporting to image                                                     0.4s 

 => => exporting layers                                                    0.4s 

 => => writing image sha256:93fe4ca588c552786f5377e0060413c7efb35916dadfc  0.0s 

 => => naming to docker.io/library/centosjava8:1.5                         0.0s 

  

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# 验证
docker images
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```

#### 4 运行

```shell
docker run -it 新镜像名字:TAG
```

演示

```shell
# 运行build生成的镜像
docker run -it centosjava8:1.5 /bin/bash
# 验证
pwd
vim a.txt
ifconfig
java -version
```

### 2 虚悬镜像

#### 1 是什么

仓库名、标签都是`<none>`的镜像，俗称dangling image。

#### 2 演示生成

通过dockerfile演示生成

```shell
# 1 准备Dockerfile
vim Dockerfile

# ----------------------
from ubuntu
CMD echo 'action is success'
#-------------------------- 

# 2 构建，没有镜像名 和tag
docker build .
```

#### 3 查看虚悬镜像

```shell
hua@HuaMac test % docker image ls -f dangling=true
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
<none>       <none>    4fda8135af3e   15 months ago   65.6MB
```

#### 4 删除虚悬镜像

虚悬镜像已经失去存在价值，可以删除。

```shell
# 1 删除所有虚悬镜像，中间输入y
docker image prune
```