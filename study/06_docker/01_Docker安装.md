# 一、基本安装

[docker 官网](http://www.docker.com)
[Docker Hub官网](https://hub.docker.com/)

[下载地址](https://docs.docker.com/get-started/get-docker/)

## 1 linux下安装

可以在linux的虚拟机上进行对应的安装。

https://docs.docker.com/engine/install/centos/

### 1 安装步骤

```shell
# 确定你是CentOS7及以上版本
cat /etc/redhat-release
# 卸载旧版本
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
# yum安装gcc相关
yum -y install gcc
yum -y install gcc-c++
yum install -y yum-utils
# 安装需要的软件包
yum install -y yum-utils

# 设置stable镜像仓库，仓库作用？？？
# 方式1: 不推荐，连的是外网，超时等错误。
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
# 方式2: 阿里镜像仓库
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 更新yum软件包索引
yum makecache fast

# 安装DOCKER CE
yum -y install docker-ce docker-ce-cli containerd.io

# 启动docker
systemctl start docker
# 测试
docker version
```

### 2 卸载步骤

```shell
systemctl stop docker
yum remove docker-ce docker-ce-cli containerd.io
rm -rf /var/lib/docker
rm -rf /var/lib/containerd
```

## 2 windows下安装

### 1 启动Hyper-V

控制面板 -> 启动或关闭windows功能，依次选中如下后，重启电脑

1. Hyper-V
2. 适用于Linux的Windows子系统
3. 虚拟机平台

### 2 安装wsl

管理员身份运行power shell

```shell
# 安装子系统
wsl --update
# 启动运行WSL并安装Linux的Ubuntu
wsl --install
# 新的用户名和密码
hua
hua 
```

> 其中`wsl --install`安装出现问题的解决办法

问题1：

```
PS C:\WINDOWS\system32> wsl --install
无法从“https://raw.githubusercontent.com/microsoft/WSL/master/distributions/DistributionInfo.json”中提取列表分发。 无法解析服务器的名称或地址
Error code: Wsl/WININET_E_NAME_NOT_RESOLVED
```

解决办法：
修改`C:\Windows\System32\drivers\etc\`路径的hosts文件

```shell
185.199.110.133 raw.githubusercontent.com 
```

安装完wsl，重启下电脑。

### 3 安装docker

从上面地址下载docker desktop，一路next到底即可。

mac系统只需执行这一步即可。

# 二、下载镜像配置

由于众所不知的原因，镜像下载不了，严重影响docker的正常使用，所以列出如下解决方案。
[参考地址](https://mp.weixin.qq.com/s?__biz=MjM5NzEyMzg4MA==&mid=2649507965&idx=8&sn=3ca2bfad61d126e173ac12ac90d4f545&chksm=bfdfc666df40400341e408bcbd8a2e0e3d15895c39218d4419a97ace92934520a441ae4bac67&scene=27)

## 1 镜像加速

以阿里镜像加速为例

### 1 基本步骤

https://promotion.aliyun.com/ntms/act/kubernetes.html

登录 -> 控制台 -> 产品 -》 弹性计算 -〉容器镜像服务 -> 左上角镜像工具 -> 镜像加速器。

```shell
sudo mkdir -p /etc/docker 
sudo tee /etc/docker/daemon.json <<-'EOF' { "registry-mirrors": ["https://qs6ozu4a.mirror.aliyuncs.com"] } EOF 
sudo systemctl daemon-reload 
sudo systemctl restart docker
```

### 2 github action

镜像加速现在几乎全部挂掉，更方便的可行方案是github action。

1. fork对方仓库。
2. Actions选项卡点同意。
3. 选择平台，搜索镜像。
4. 点击 All workflows，进去下载。
5. 两次解压缩，得到tar文件。
6. `docker load -i xxx.tar` 加载离线镜像