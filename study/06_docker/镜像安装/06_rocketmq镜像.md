[网上教程](https://blog.csdn.net/qq_43600166/article/details/136187969)

### 基本安装

#### 1 拉取镜像

```shell
docker pull rocketmqinc/rocketmq
```

#### 2 创建nameserver的日志和数据存放目录

rocketMQ 分为nameserver和broker两部分，在启动时应该先启动nameserver，
因此我们现在先创建nameserver的日志和数据存放目录。这个目录可由我们自己定义路径，这里我将其放到data路径下:

```shell
# logs:是nameserver的日志目录，store:是nameserver的数据目录
mkdir -p /data/namesrv/logs /data/namesrv/store
```

#### 3 构建namesrv容器并启动

```shell
docker run -d  --restart=always --name rmqnamesrv -p 9876:9876 \
 -v  /data/namesrv/logs:/root/logs \
  -v /data/namesrv/store:/root/store \
   -e "MAX_POSSIBLE_HEAP=100000000" \
    rocketmqinc/rocketmq sh mqnamesrv
```

#### 4 创建broker的日志和数据存放目录

```shell
mkdir -p /data/broker/logs  /data/broker/store /data/broker/conf
```

#### 5 创建broker配置文件

```shell
cd /data/broker/conf
vi broker.conf
```

具体内容如下： zennote 是重点标记
> 请将一下信息复制到broker.conf文件中，请注意修改brokerIP1，改为您服务器的IP地址！！！！<br/>
> 请注意修改namesrvAddr，改为您nameserver的IP地址！！！！

```shell
# 所属集群名字
brokerClusterName=DefaultCluster
# broker 名字，注意此处不同的配置文件填写的不一样，如果在 broker-a.properties 使用: broker-a,
# 在 broker-b.properties 使用: broker-b
brokerName=broker-a
# 0 表示 Master，> 0 表示 Slave
brokerId=0
# nameServer地址，分号分割
namesrvAddr=localhost:9876
# 启动IP,如果 docker 报 com.alibaba.rocketmq.remoting.exception.RemotingConnectException: connect to <192.168.0.120:10909> failed
# 解决方式1 加上一句 producer.setVipChannelEnabled(false);，解决方式2 brokerIP1 设置宿主机IP，不要使用docker 内部IP
#producer.setVipChannelEnabled=false
brokerIP1=localhost

# 在发送消息时，自动创建服务器不存在的topic，默认创建的队列数
defaultTopicQueueNums=4
# 是否允许 Broker 自动创建 Topic，建议线下开启，线上关闭 ！！！这里仔细看是 false，false，false
autoCreateTopicEnable=true
# 是否允许 Broker 自动创建订阅组，建议线下开启，线上关闭
autoCreateSubscriptionGroup=true
# Broker 对外服务的监听端口
listenPort=10911
# 删除文件时间点，默认凌晨4点
deleteWhen=04
# 文件保留时间，默认48小时
fileReservedTime=120
# commitLog 每个文件的大小默认1G
mapedFileSizeCommitLog=1073741824
# ConsumeQueue 每个文件默认存 30W 条，根据业务情况调整
mapedFileSizeConsumeQueue=300000
# destroyMapedFileIntervalForcibly=120000
# redeleteHangedFileInterval=120000
# 检测物理文件磁盘空间
diskMaxUsedSpaceRatio=88
# 限制的消息大小
maxMessageSize=65536
# Broker 的角色
# - ASYNC_MASTER 异步复制Master
# - SYNC_MASTER 同步双写Master
# - SLAVE
brokerRole=ASYNC_MASTER
# 刷盘方式
# - ASYNC_FLUSH 异步刷盘
# - SYNC_FLUSH 同步刷盘
flushDiskType=ASYNC_FLUSH
```

#### 6 构建broker容器并启动

```shell
docker run -d --restart=always --name rmqbroker01 \
 --link rmqnamesrv:namesrv -p 10911:10911 -p 10909:10909 \
  -v /data/broker/logs:/root/logs \
  -v /data/broker/store:/root/store \
  -v  /data/broker/conf/broker.conf:/opt/rocketmq-4.4.0/conf/broker.conf \
  -e "NAMESRV_ADDR=namesrv:9876"  -e "MAX_POSSIBLE_HEAP=200000000" \
   rocketmqinc/rocketmq sh mqbroker -c /opt/rocketmq-4.4.0/conf/broker.conf 
```

### rocketmq-console可视化界面

#### 1 拉取镜像

```shell
docker pull pangliang/rocketmq-console-ng
```

#### 2 创建容器并启动

```shell
docker run -d --restart=always --name rmqadmin \
-e "JAVA_OPTS=-Drocketmq.namesrv.addr=124.71.8.46:9876 -Dcom.rocketmqsendMessageWithVIPChannel=false"  \
-p 8999:8080  pangliang/rocketmq-console-ng
```

> 请将一下信息Drocketmq.namesrv.addr后面的IP地址改为您的nameserver IP地址！！！
> 本机测试可以改成localhost:9876

#### 3 访问8999端口

通过访问8999端口查看rocketmq-console是否启动成功

