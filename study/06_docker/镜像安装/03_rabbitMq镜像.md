[官网](https://www.rabbitmq.com/)
基本命令

```shell
docker run \
 -e RABBITMQ_DEFAULT_USER=hua \
 -e RABBITMQ_DEFAULT_PASS=123321 \
 -v /Users/hua/docker-data/rabbitmq/base/plugins:/var/lib/rabbitmq/plugins \
 --name rabbitmq_base \
 --hostname mq \
 -p 15672:15672 \
 -p 5672:5672 \
 -d \
 rabbitmq:3.8-management
```


```shell
docekr pull rabbitmq

## 运行
docker run \
-e RABBITMQ_DEFAULT_USER=hua \
-e RABBITMQ_DEFAULT_PASS=hua \
-v /Users/hua/docker-data/rabbitmq/base/plugins:/var/lib/rabbitmq/plugins \
--name rabbitmq_base \
--hostname localhost \
-p 15672:15672 \
-p 5672:5672 \
-d \
rabbitmq

# 进入容器里后，执行启用web界面管理插件命令, 否则无法访问web管理界面
docker ps
docker exec -it 2da0 bash
rabbitmq-plugins enable rabbitmq_management
```

> 问题
> 官方说插件rabbitmq插件位置在/var/lib/rabbitmq/plugins，但却确乎在/plugins
> 配置容器卷为/plugins会容器退出，配置成/var/lib/rabbitmq/plugins没效果，现在只能不配置，通过上传到容器的方式。


可以看到在安装命令中有两个映射的端口：
● 15672：RabbitMQ提供的管理控制台的端口
● 5672：RabbitMQ的消息发送处理接口
安装完成后，我们访问 http://127.0.0.1:15672 即可看到管理控制台。首次访问需要登录，默认的用户名和
密码在配置命令中已经指定了。登录后即可看到管理控制台总览页面
> 密码见命令
