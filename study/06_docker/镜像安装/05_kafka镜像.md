#### 1 下载镜像

Kafka通常与Zookeeper一起部署，因为Zookeeper提供了分布式协调服务，这对于Kafka集群是必需的。你可以分别拉取Kafka和Zookeeper的镜像。

```shell
docker pull wurstmeister/zookeeper
docker pull wurstmeister/kafka
```

你也可以创建一个网络，将Kafka和Zookeeper容器连接到同一个网络。

```shell
docker network create kafka-net
```

#### 2 运行zookeeper容器

```shell
docker run --name zookeeper --net kafka-net -p 2181:2181 \
-e CONFLUENT_ZOOKEEPER_PASSWORD=password -e CONFLUENT_ZOOKEEPER_USER=admin \
-d wurstmeister/zookeeper
```

这里-p 2181:2181将Zookeeper的默认端口2181映射到主机的相同端口上。

#### 3 运行kafka容器

```shell
docker run -d --net kafka-net --name kafka \
    -p 9092:9092 \
    -e KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181 \
    -e KAFKA_LISTENERS=PLAINTEXT://:9092 \
    -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://localhost:9092 \
    -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1 \
    -e KAFKA_CREATE_TOPICS=test:1:1 \
    wurstmeister/kafka
```

-p 9092:9092 将Kafka的端口9092映射到宿主机的9092端口。
--link zookeeper:zookeeper 将Kafka容器链接到ZooKeeper容器。
-e KAFKA_ADVERTISED_HOST_NAME=localhost 设置Kafka的外部访问地址为localhost。
-e KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181 设置ZooKeeper的连接信息。
-e KAFKA_CREATE_TOPICS=test:1:1 在Kafka启动时创建一个名为test的主题，具有1个分区和1个副本因子。
-e KAFKA_LOG_DIRS=/kafka-logs 设置Kafka的日志存储目录。
-e KAFKA_LOG_RETENTION_HOURS=168 设置日志保留时间为1周

#### 4 测试Kafka

你可以发送一条消息到Kafka主题

```shell
docker exec kafka kafka-console-producer.sh --topic test --broker-list localhost:9092
```

在一个新的终端窗口接收这条消息：

```shell
docker exec kafka kafka-console-consumer.sh --topic test --from-beginning --bootstrap-server localhost:9092
```

