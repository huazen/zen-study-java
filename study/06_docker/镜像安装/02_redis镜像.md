# 一、基本安装

## 1 单机安装

以6.0.8版本为例。
宿主机上先准备redis.conf，通过历史版本下载解压获取默认配置文件。

```shell
# 注释bind，允许redis外地链接
# bind 127.0.0.1
# 将daemonize yes注释起来或者 daemonize no设置，因为该配置和docker run中-d参数冲突，会导致容器一直启动失败
daemonize no
# 密码配置【可选】
requirepass 123456
# redis数据持久化可选
appendonly yes
```

启动redis镜像

```shell
# 拉取镜像
docker pull redis:6.0.8
# 启动镜像，按实践配置数据卷
# 直接执行失败，一般是因为最后的\后有空格
docker run -p 6379:6379 --name redis_base --privileged=true \
-v /docker_data/redis/base/redis.conf:/etc/redis/redis.conf \
-v /docker_data/redis/base/data:/data \
-d redis:6.0.8 redis-server /etc/redis/redis.conf

#验证
docker ps
docker exec -it redis_base /bin/bash
redis-cli
# 可选
auth 密码
127.0.0.1:6379> set k1 k2
OK
127.0.0.1:6379> get k1 
"k2"
```

抽取核心逻辑

```shell
docker run -p 6379:6379 --name redis_base --privileged=true \
-v /docker_data/redis/base/redis.conf:/etc/redis/redis.conf \
-v /docker_data/redis/base/data:/data \
-d redis:6.0.8 redis-server /etc/redis/redis.conf
```

# 二、主从集群安装

## 1 三主三从集群配置

### 1 准备

关闭防火墙，启动docker后台服务

```shell
systemctl start docker
```

mac: docker,直接软件启动。

### 2 启动6个实例

直接复制全部命令，一次性启动六台实例。
容器卷根据自身实际变动，mac docker 要将容器卷设到当前用户目录下，否则会启动失败。

```shell
docker run -d --name redis-node-1 --net host --privileged=true \
-v /data/redis/share/redis-node-1:/data redis:6.0.8 \
--cluster-enabled yes --appendonly yes --port 6381

docker run -d --name redis-node-2 --net host --privileged=true \
-v /data/redis/share/redis-node-2:/data redis:6.0.8 \
--cluster-enabled yes --appendonly yes --port 6382

docker run -d --name redis-node-3 --net host --privileged=true \
-v /data/redis/share/redis-node-3:/data redis:6.0.8 \
--cluster-enabled yes --appendonly yes --port 6383 

docker run -d --name redis-node-4 --net host --privileged=true \
-v /data/redis/share/redis-node-4:/data redis:6.0.8 \
--cluster-enabled yes --appendonly yes --port 6384

docker run -d --name redis-node-5 --net host --privileged=true \
-v /data/redis/share/redis-node-5:/data redis:6.0.8 \
--cluster-enabled yes --appendonly yes --port 6385

docker run -d --name redis-node-6 --net host --privileged=true \
-v /data/redis/share/redis-node-6:/data redis:6.0.8 \
--cluster-enabled yes --appendonly yes --port 6386
```

### 3 构建集群关系

```shell
# 进入某个redis节点容器
docker exec -it redis-node-1 /bin/bash
# 进入容器后才可以执行如下命令
# --cluster create 代表创建集群, 后面跟的是集群中的机器，ip在此为宿主机ip, 测试时可以使用127.0.0.1
# --cluster-replicas 1 表示为每个master创建一个slave节点
redis-cli --cluster create 192.168.111.147:6381 192.168.111.147:6382 \
192.168.111.147:6383 192.168.111.147:6384 192.168.111.147:6385 \
192.168.111.147:6386 --cluster-replicas 1
# 输入yes，enter。
Can I set the above configuration? (type 'yes' to accept): yes
```

注意：

1. 如果ip和端口不能对外开放的话，可以使用127.0.0.1 代替ip进行测试。

执行结果的部分信息:

```
# 几个节点  
>>> Performing hash slots allocation on 6 nodes...  
# 主节点与对应的hash槽  
Master[0] -> Slots 0 - 5460  
Master[1] -> Slots 5461 - 10922  
Master[2] -> Slots 10923 - 16383  
# 从节点 ——> 主节点  
Adding replica 127.0.0.1:6385 to 127.0.0.1:6381  
Adding replica 127.0.0.1:6386 to 127.0.0.1:6382  
Adding replica 127.0.0.1:6384 to 127.0.0.1:6383  
```

### 4 查看集群状态

以6381为切入点，查看节点状态。

```
# 进入6381
redis-cli -p 6381
# 查看集群状态
127.0.0.1:6381> cluster info  
cluster_state:ok
# 分配的槽数
cluster_slots_assigned:16384  
cluster_slots_ok:16384  
cluster_slots_pfail:0  
cluster_slots_fail:0
# 已知节点数据
cluster_known_nodes:6  
cluster_size:3  
cluster_current_epoch:6  
cluster_my_epoch:1  
cluster_stats_messages_ping_sent:225  
cluster_stats_messages_pong_sent:234  
cluster_stats_messages_sent:459  
cluster_stats_messages_ping_received:229  
cluster_stats_messages_pong_received:225  
cluster_stats_messages_meet_received:5  
cluster_stats_messages_received:459
# 查看集群节点信息
127.0.0.1:6381> cluster nodes
# master代表此节点是master节点，myself代表进入的是当前节点。
# slave代表此节点是slave节点，后面跟的是主节点标识。
60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383@16383 master - 0 1673320026451 3 connected 10923-16383  
22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382@16382 master - 0 1673320025000 2 connected 5461-10922  
317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384@16384 slave 719671617900c642faee920c7bb369dbeda843b5 0 1673320027458 1 connected  
c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385@16385 slave 22128787b677f43a61b4a157fb189f9ff5f97665 0 1673320024000 2 connected  
13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386@16386 slave 60944407c8c241e998505e635d5eea0b30f383b8 0 1673320023425 3 connected  
719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381@16381 myself,master - 0 1673320026000 1 connected 0-5460  
127.0.0.1:6381>
```

## 2 集群读写

### 1 集群读写错误

```shell
# 进入某台主机
docker exec -it redis-node-2 /bin/bash
# 以6381为例
root@docker-desktop:/data# redis-cli -p 6381
127.0.0.1:6381> set k1 v1
(error) MOVED 12706 127.0.0.1:6383
127.0.0.1:6381> set k2 v2
OK
127.0.0.1:6381> get k1
(error) MOVED 12706 127.0.0.1:6383
```

因为master1分配的槽数是 Slots 0 - 5460，到key的crc槽位超过这个范围就会报错。
不能使用单机版命令

### 2 集群读写路由

核心：

```shell
# -c 优化路由
redis-cli -p 6381 -c
```

细节：

```shell
# 进入某台主机
docker exec -it redis-node-3 /bin/bash
# -c 优化路由
redis-cli -p 6381 -c

# 测试
127.0.0.1:6381> flushall
OK
127.0.0.1:6381> set k1 v1
# 重定向到槽对应的节点
-> Redirected to slot [12706] located at 127.0.0.1:6383
OK
127.0.0.1:6383> set k2 v2
-> Redirected to slot [449] located at 127.0.0.1:6381
OK
127.0.0.1:6381> set k2 v2
OK
127.0.0.1:6381> get k1
-> Redirected to slot [12706] located at 127.0.0.1:6383
"v1"
127.0.0.1:6383> get k1
"v1"
```

### 3 集群信息

```shell
# 宿主机ip，不对外开发时，可用127.0.0.1进行测试
redis-cli --cluster check 192.168.111.147:6381
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
root@docker-desktop:/data# redis-cli --cluster check 127.0.0.1:6381
# 主机所用的key
127.0.0.1:6381 (71967161...) -> 1 keys | 5461 slots | 1 slaves.
127.0.0.1:6383 (60944407...) -> 1 keys | 5461 slots | 1 slaves.
127.0.0.1:6382 (22128787...) -> 0 keys | 5462 slots | 1 slaves.
[OK] 2 keys in 3 masters.
0.00 keys per slot on average.
# 从机与主机关系
>>> Performing Cluster Check (using node 127.0.0.1:6381)
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381
slots:[0-5460] (5461 slots) master
1 additional replica(s)
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383
slots:[10923-16383] (5461 slots) master
1 additional replica(s)
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382
slots:[5461-10922] (5462 slots) master
1 additional replica(s)
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384
slots: (0 slots) slave
replicates 719671617900c642faee920c7bb369dbeda843b5
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385
slots: (0 slots) slave
replicates 22128787b677f43a61b4a157fb189f9ff5f97665
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386
slots: (0 slots) slave
replicates 60944407c8c241e998505e635d5eea0b30f383b8
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```

## 3 容错切换

### 1 某台主机宕机

停止某台主机，以6381为例

```shell
# 进入某一主机
docker exec -it redis-node-1 /bin/bash
# 进入6381
redis-cli -p 6381
# 查看集群节点信息
cluster nodes
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
127.0.0.1:6381> cluster nodes
60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383@16383 master - 0 1673331322000 3 connected 10923-16383
22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382@16382 master - 0 1673331324950 2 connected 5461-10922
# 当前主节点对应的从节点，6384
317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384@16384 slave 719671617900c642faee920c7bb369dbeda843b5 0 1673331323000 1 connected
c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385@16385 slave 22128787b677f43a61b4a157fb189f9ff5f97665 0 1673331323943 2 connected
13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386@16386 slave 60944407c8c241e998505e635d5eea0b30f383b8 0 1673331322922 3 connected
# 当前主节点
719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381@16381 myself,master - 0 1673331324000 1 connected 0-5460
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# 退出
docker ps
# 停止6381
docker stop redis-node-1
# 等一会，等待心跳通知
```

### 2 新主机查看集群信息

使用6382主机查看集群信息：6381宕机，6384变成主机。

```shell
# 进入某一主机
docker exec -it redis-node-2 /bin/bash
# 进入6382
redis-cli -p 6382

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
127.0.0.1:6382> cluster nodes
# 从机6384，变成主机
317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384@16384 master - 0 1673332144158 7 connected 0-5460
60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383@16383 master - 0 1673332148194 3 connected 10923-16383
# 当前节点
22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382@16382 myself,master - 0 1673332147000 2 connected 5461-10922
# 宕机的node1
719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381@16381 master,fail - 1673331626731 1673331622000 1 disconnected
c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385@16385 slave 22128787b677f43a61b4a157fb189f9ff5f97665 0 1673332147000 2 connected
13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386@16386 slave 60944407c8c241e998505e635d5eea0b30f383b8 0 1673332146178 3 connected
127.0.0.1:6382>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```

### 3 主机复活

```shell
# 6381宕机，6384上位

# 启动6381，复活原主机
docker start redis-node-1
# 确认已经复活
docker ps
# 6382查看集群信息
cluster nodes
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
127.0.0.1:6382> cluster nodes
# 原从机6384仍变为主机
317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384@16384 master - 0 1673333229000 7 connected 0-5460
60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383@16383 master - 0 1673333228000 3 connected 10923-16383
22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382@16382 myself,master - 0 1673333230000 2 connected 5461-10922
# 原主机6381虽复活，却变为从机
719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381@16381 slave 317dd0b77df3d1c5180112673379f7e26d5470e6 0 1673333231000 7 connected
c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385@16385 slave 22128787b677f43a61b4a157fb189f9ff5f97665 0 1673333231619 2 connected
13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386@16386 slave 60944407c8c241e998505e635d5eea0b30f383b8 0 1673333230612 3 connected
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```

复原为原来主从关系

```shell
# 停止6384
docker start redis-node-4
# 启动6384
docker start redis-node-4
# 6382查看集群信息: 6381归位，6384退位。
cluster nodes
```

## 4 主从扩容

流量高峰上来了，新增一主一从。

### 1 新增两台redis实例

```shell
docker run -d --name redis-node-7 --net host --privileged=true \
-v /data/redis/share/redis-node-7:/data redis:6.0.8 \
--cluster-enabled yes --appendonly yes --port 6387

docker run -d --name redis-node-8 --net host --privileged=true \
-v /data/redis/share/redis-node-8:/data redis:6.0.8 \
--cluster-enabled yes --appendonly yes --port 6388

# 确定新的的主从节点安装成功
docker ps
```

### 2 加入集群

将新增的6387作为master节点加入集群

```shell
# 1 加入集群
redis-cli --cluster add-node 自己实际IP地址:6387 自己实际IP地址:6381
# 6387 就是将要作为master新增节点
# 6381 就是原来集群节点里面的领路人，相当于6387拜拜6381的码头从而找到组织加入集群
# ip不开放端口，可以本机可以用127.0.0.1

# 2 查看集群信息
docker exec -it redis-node-1
redis-cli --cluster check 127.0.0.1:6381
```

演示

```python
hua@B-25KHML85-2342 docker_data % docker exec -it redis-node-7 /bin/bash
root@docker-desktop:/data# redis-cli --cluster add-node 127.0.0.1:6387 127.0.0.1:6381

# # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>> Adding node 127.0.0.1:6387 to cluster 127.0.0.1:6381
>>> Performing Cluster Check (using node 127.0.0.1:6381)
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381
slots:[0-5460] (5461 slots) master
1 additional replica(s)
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382
slots:[5461-10922] (5462 slots) master
1 additional replica(s)
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385
slots: (0 slots) slave
replicates 22128787b677f43a61b4a157fb189f9ff5f97665
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386
slots: (0 slots) slave
replicates 60944407c8c241e998505e635d5eea0b30f383b8
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384
slots: (0 slots) slave
replicates 719671617900c642faee920c7bb369dbeda843b5
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383
slots:[10923-16383] (5461 slots) master
1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
>>> Send CLUSTER MEET to node 127.0.0.1:6387 to make it join the cluster.
# 新节点正确加入
[OK] New node added correctly.
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
root@docker-desktop:/data# redis-cli --cluster check 127.0.0.1:6381
127.0.0.1:6381 (71967161...) -> 1 keys | 5461 slots | 1 slaves.
# 新节点已经加入集群，并且作为主机。
# 0 slots 代表没有槽位
127.0.0.1:6387 (e0b5c519...) -> 0 keys | 0 slots | 0 slaves.
127.0.0.1:6382 (22128787...) -> 0 keys | 5462 slots | 1 slaves.
127.0.0.1:6383 (60944407...) -> 1 keys | 5461 slots | 1 slaves.
[OK] 2 keys in 4 masters.
0.00 keys per slot on average.
>>> Performing Cluster Check (using node 127.0.0.1:6381)
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381
slots:[0-5460] (5461 slots) master
1 additional replica(s)
M: e0b5c51992b38b88443d45179a95bd4e40365a0c 127.0.0.1:6387
slots: (0 slots) master
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382
slots:[5461-10922] (5462 slots) master
1 additional replica(s)
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385
slots: (0 slots) slave
replicates 22128787b677f43a61b4a157fb189f9ff5f97665
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386
slots: (0 slots) slave
replicates 60944407c8c241e998505e635d5eea0b30f383b8
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384
slots: (0 slots) slave
replicates 719671617900c642faee920c7bb369dbeda843b5
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383
slots:[10923-16383] (5461 slots) master
1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```

### 3 重新分配槽号

```shell
redis-cli --cluster reshard IP地址: 端口号
# 不是所有槽位重新分配，而是老主机们匀一些槽位给新主机，同时包括槽位上的数据。
```

演示

```python
redis-cli --cluster reshard 192.168.111.147:6381

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
root@docker-desktop:/data# redis-cli --cluster reshard 127.0.0.1:6381
>>> Performing Cluster Check (using node 127.0.0.1:6381)  
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381  
slots:[0-5460] (5461 slots) master  
1 additional replica(s)
# 没有槽位的新主机
M: e0b5c51992b38b88443d45179a95bd4e40365a0c 127.0.0.1:6387  
slots: (0 slots) master  
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382  
slots:[5461-10922] (5462 slots) master  
1 additional replica(s)  
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385  
slots: (0 slots) slave  
replicates 22128787b677f43a61b4a157fb189f9ff5f97665  
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386  
slots: (0 slots) slave  
replicates 60944407c8c241e998505e635d5eea0b30f383b8  
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384  
slots: (0 slots) slave  
replicates 719671617900c642faee920c7bb369dbeda843b5  
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383  
slots:[10923-16383] (5461 slots) master  
1 additional replica(s)  
[OK] All nodes agree about slots configuration.
>>> Check for open slots...  
>>> Check slots coverage...  
[OK] All 16384 slots covered.
# 1 第一步，分配的槽位数：16384/总主机数，均匀分配
# 不要有空格，否则 会造成截取现象，如409 slots。
How many slots do you want to move (from 1 to 16384)? 4096
# 2 新主机对应的id，即6379对应的id
What is the receiving node ID? e0b5c51992b38b88443d45179a95bd4e40365a0c  
Please enter all the source node IDs.  
Type 'all' to use all the nodes as source nodes for the hash slots.  
Type 'done' once you entered all the source nodes IDs.
# 3 填入all
Source node #1: all

Ready to move 409 slots.  
Source nodes:  
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381  
slots:[0-5460] (5461 slots) master  
1 additional replica(s)  
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382  
slots:[5461-10922] (5462 slots) master  
1 additional replica(s)  
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383  
slots:[10923-16383] (5461 slots) master  
1 additional replica(s)  
Destination node:  
M: e0b5c51992b38b88443d45179a95bd4e40365a0c 127.0.0.1:6387  
slots: (0 slots) master  
Resharding plan:  
Moving slot 5461 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5462 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5463 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5464 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5465 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5466 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5467 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5468 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5469 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5470 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5471 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5472 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5473 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5474 from 22128787b677f43a61b4a157fb189f9ff5f97665  
……  
Moving slot 11057 from 60944407c8c241e998505e635d5eea0b30f383b8  
Moving slot 11058 from 60944407c8c241e998505e635d5eea0b30f383b8
# 4 填入yes
Do you want to proceed with the proposed reshard plan (yes/no)? yes


# 查看集群情况
redis-cli --cluster check 127.0.0.1:6381
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
root@docker-desktop:/data# redis-cli --cluster check 127.0.0.1:6381   
127.0.0.1:6381 (71967161...) -> 1 keys | 5325 slots | 1 slaves.
# 已分配槽位
127.0.0.1:6387 (e0b5c519...) -> 0 keys | 409 slots | 0 slaves.  
127.0.0.1:6382 (22128787...) -> 0 keys | 5325 slots | 1 slaves.  
127.0.0.1:6383 (60944407...) -> 1 keys | 5325 slots | 1 slaves.  
[OK] 2 keys in 4 masters.  
0.00 keys per slot on average.  
\>>> Performing Cluster Check (using node 127.0.0.1:6381)  
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381  
slots:[136-5460] (5325 slots) master  
1 additional replica(s)  
M: e0b5c51992b38b88443d45179a95bd4e40365a0c 127.0.0.1:6387
# 之前的每个主机匀了部分槽位给新主机， 为什么不是4096 slots?
slots:[0-135],[5461-5597],[10923-11058] (409 slots) master  
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382  
slots:[5598-10922] (5325 slots) master  
1 additional replica(s)  
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385  
slots: (0 slots) slave  
replicates 22128787b677f43a61b4a157fb189f9ff5f97665  
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386  
slots: (0 slots) slave  
replicates 60944407c8c241e998505e635d5eea0b30f383b8  
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384  
slots: (0 slots) slave  
replicates 719671617900c642faee920c7bb369dbeda843b5  
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383  
slots:[11059-16383] (5325 slots) master  
1 additional replica(s)  
[OK] All nodes agree about slots configuration.
>>> Check for open slots...  
>>> Check slots coverage...  
[OK] All 16384 slots covered.
```

### 4 添加新从机

将从机6388添加到6487上。

```shell
redis-cli --cluster add-node ip:新slave端口 ip:新master端口 \
--cluster-slave --cluster-master-id 新主机节点ID
```

示例

```shell
# 主机节点id，可以通过查看集群情况获取
redis-cli --cluster add-node 192.168.111.147:6388 192.168.111.147:6387 \  
--cluster-slave --cluster-master-id e0b5c51992b38b88443d45179a95bd4e40365a0c

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>> Adding node 127.0.0.1:6388 to cluster 127.0.0.1:6387  
>>> Performing Cluster Check (using node 127.0.0.1:6387)  
M: e0b5c51992b38b88443d45179a95bd4e40365a0c 127.0.0.1:6387  
slots:[0-1501],[5461-6962],[10923-12423] (4505 slots) master  
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381  
slots:[1502-5460] (3959 slots) master  
1 additional replica(s)  
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383  
slots:[12424-16383] (3960 slots) master  
1 additional replica(s)  
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382  
slots:[6963-10922] (3960 slots) master  
1 additional replica(s)  
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385  
slots: (0 slots) slave  
replicates 22128787b677f43a61b4a157fb189f9ff5f97665  
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384  
slots: (0 slots) slave  
replicates 719671617900c642faee920c7bb369dbeda843b5  
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386  
slots: (0 slots) slave  
replicates 60944407c8c241e998505e635d5eea0b30f383b8  
[OK] All nodes agree about slots configuration.  
>>> Check for open slots...  
>>> Check slots coverage...  
[OK] All 16384 slots covered.  
>>> Send CLUSTER MEET to node 127.0.0.1:6388 to make it join the cluster.  
Waiting for the cluster to join

>>> Configure node as replica of 127.0.0.1:6387.  
[OK] New node added correctly.
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```

## 5 主从缩容

流量下去了, redis集群缩容，删除6387和6389，恢复3主3从。

### 1 先删除从节点

```shell
# 删除某个节点
redis-cli --cluster del-node ip:端口 节点ID
```

演示

```shell
# 1 查看集群情况获取，从节点6388对应的id
# aa400ef7debf7aac4dc1a1c98a045d9f9685919f
redis-cli --cluster check 127.0.0.1:6382
# 2 删除节点 redis-cli --cluster del-node 127.0.0.1:6388 aa400ef7debf7aac4dc1a1c98a045d9f9685919f
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>> Removing node aa400ef7debf7aac4dc1a1c98a045d9f9685919f from cluster 127.0.0.1:6388  
>>> Sending CLUSTER FORGET messages to the cluster...  
>>> Sending CLUSTER RESET SOFT to the deleted node.
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# 3 检查集群情况:已没有6388节点
redis-cli --cluster check 127.0.0.1:6382
```

### 2 清空主机槽位

将6387的槽号清空，重新分配，本例将清出来的槽号都给6381

```shell
redis-cli --cluster reshard 192.168.111.147:6381

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>> Performing Cluster Check (using node 127.0.0.1:6381)  
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381  
slots:[1502-5460] (3959 slots) master  
1 additional replica(s)  
M: e0b5c51992b38b88443d45179a95bd4e40365a0c 127.0.0.1:6387  
slots:[0-1501],[5461-6962],[10923-12423] (4505 slots) master  
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382  
slots:[6963-10922] (3960 slots) master  
1 additional replica(s)  
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385  
slots: (0 slots) slave  
replicates 22128787b677f43a61b4a157fb189f9ff5f97665  
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386  
slots: (0 slots) slave  
replicates 60944407c8c241e998505e635d5eea0b30f383b8  
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384  
slots: (0 slots) slave  
replicates 719671617900c642faee920c7bb369dbeda843b5  
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383  
slots:[12424-16383] (3960 slots) master  
1 additional replica(s)  
[OK] All nodes agree about slots configuration.  
>>> Check for open slots...  
>>> Check slots coverage...  
[OK] All 16384 slots covered.
# 1 移动多少槽位
How many slots do you want to move (from 1 to 16384)? 4505
# 2 谁来接手，此处为6381的节点
What is the receiving node ID? 719671617900c642faee920c7bb369dbeda843b5  
Please enter all the source node IDs.  
Type 'all' to use all the nodes as source nodes for the hash slots.  
Type 'done' once you entered all the source nodes IDs.
# 3 槽位由谁出，此处设为6487节点
Source node #1: e0b5c51992b38b88443d45179a95bd4e40365a0c
# 4 执行
Source node #2: done

Ready to move 4505 slots.  
Source nodes:  
M: e0b5c51992b38b88443d45179a95bd4e40365a0c 127.0.0.1:6387  
slots:[0-1501],[5461-6962],[10923-12423] (4505 slots) master  
Destination node:  
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381  
slots:[1502-5460] (3959 slots) master  
1 additional replica(s)  
Resharding plan:  
Moving slot 5461 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5462 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5463 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5464 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5465 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5466 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5467 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5468 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5469 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5470 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5471 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5472 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5473 from 22128787b677f43a61b4a157fb189f9ff5f97665  
Moving slot 5474 from 22128787b677f43a61b4a157fb189f9ff5f97665  
……  
Moving slot 11057 from 60944407c8c241e998505e635d5eea0b30f383b8  
Moving slot 11058 from 60944407c8c241e998505e635d5eea0b30f383b8
# 5 填入yes
Do you want to proceed with the proposed reshard plan (yes/no)? yes
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# 查看集群情况，确认执行结果。
redis-cli --cluster check 127.0.0.1:6381

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
root@docker-desktop:/data# redis-cli --cluster check 127.0.0.1:6381
# 获得了6387的槽位 127.0.0.1:6381 (71967161...) -> 1 keys | 8464 slots | 1 slaves.
# 6387已经没有槽位了
127.0.0.1:6387 (e0b5c519...) -> 0 keys | 0 slots | 0 slaves.  
127.0.0.1:6382 (22128787...) -> 0 keys | 3960 slots | 1 slaves.  
127.0.0.1:6383 (60944407...) -> 1 keys | 3960 slots | 1 slaves.  
[OK] 2 keys in 4 masters.  
0.00 keys per slot on average.
>>> Performing Cluster Check (using node 127.0.0.1:6381)  
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381  
slots:[0-6962],[10923-12423] (8464 slots) master  
1 additional replica(s)
# 6387已经没有槽位了
M: e0b5c51992b38b88443d45179a95bd4e40365a0c 127.0.0.1:6387  
slots: (0 slots) master  
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382  
slots:[6963-10922] (3960 slots) master  
1 additional replica(s)  
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385  
slots: (0 slots) slave  
replicates 22128787b677f43a61b4a157fb189f9ff5f97665  
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386  
slots: (0 slots) slave  
replicates 60944407c8c241e998505e635d5eea0b30f383b8  
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384  
slots: (0 slots) slave  
replicates 719671617900c642faee920c7bb369dbeda843b5  
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383  
slots:[12424-16383] (3960 slots) master  
1 additional replica(s)  
[OK] All nodes agree about slots configuration.
>>> Check for open slots...  
>>> Check slots coverage...  
[OK] All 16384 slots covered.
```

### 3 删除6387

````shell
# 1 查看集群情况获取，从节点6388对应的id
# e0b5c51992b38b88443d45179a95bd4e40365a0c
redis-cli --cluster check 127.0.0.1:6382
# 2 删除节点 redis-cli --cluster del-node 127.0.0.1:6387 e0b5c51992b38b88443d45179a95bd4e40365a0c
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>> Removing node e0b5c51992b38b88443d45179a95bd4e40365a0c from cluster 127.0.0.1:6387  
>>> Sending CLUSTER FORGET messages to the cluster...  
>>> Sending CLUSTER RESET SOFT to the deleted node.
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
````

### 4 查看缩容情况

```shell

# 3 检查集群情况:已没有6388节点,恢复3主3从。
redis-cli --cluster check 127.0.0.1:6382

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
27.0.0.1:6381 (71967161...) -> 1 keys | 8464 slots | 1 slaves.  
127.0.0.1:6382 (22128787...) -> 0 keys | 3960 slots | 1 slaves.  
127.0.0.1:6383 (60944407...) -> 1 keys | 3960 slots | 1 slaves.  
[OK] 2 keys in 3 masters.  
0.00 keys per slot on average.
>>> Performing Cluster Check (using node 127.0.0.1:6381)  
M: 719671617900c642faee920c7bb369dbeda843b5 127.0.0.1:6381  
slots:[0-6962],[10923-12423] (8464 slots) master  
1 additional replica(s)  
M: 22128787b677f43a61b4a157fb189f9ff5f97665 127.0.0.1:6382  
slots:[6963-10922] (3960 slots) master  
1 additional replica(s)  
S: c5598abec5015b3fa71d6acd74500653d6fca575 127.0.0.1:6385  
slots: (0 slots) slave  
replicates 22128787b677f43a61b4a157fb189f9ff5f97665  
S: 13b58552c5c21f03522ddbfe8ab86b43acfaea9d 127.0.0.1:6386  
slots: (0 slots) slave  
replicates 60944407c8c241e998505e635d5eea0b30f383b8  
S: 317dd0b77df3d1c5180112673379f7e26d5470e6 127.0.0.1:6384  
slots: (0 slots) slave  
replicates 719671617900c642faee920c7bb369dbeda843b5  
M: 60944407c8c241e998505e635d5eea0b30f383b8 127.0.0.1:6383  
slots:[12424-16383] (3960 slots) master  
1 additional replica(s)  
[OK] All nodes agree about slots configuration.  
>>> Check for open slots...  
>>> Check slots coverage...  
[OK] All 16384 slots covered.
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```