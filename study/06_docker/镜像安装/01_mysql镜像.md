# 一、基本安装

## 1 5.7单机安装

### 1 准备镜像

```shell
docker pull mysql:5.7
## mac m1 报错如下
no matching manifest for linux/arm64/v8 in the manifest list entries
## 使用如下命令替换
docker pull --platform linux/x86_64 mysql:5.7
```

### 2 安装命令

```shell
docker run -d -p 3306:3306 --privileged=true \
-v /docker_data/mysql/5.7base/log:/var/log/mysql \
-v /docker_data/mysql/5.7base/data:/var/lib/mysql \
-v /docker_data/mysql/5.7base/conf:/etc/mysql/conf.d \
-e MYSQL_ROOT_PASSWORD=123456 \
--name mysql mysql:5.7
```

### 3 配置my.cnf

目的：解决中文数据插入变更失败问题  
原因：mysql5.7的之前，默认编码是lant1。  
解决：在conf目录下新建并配置my.cnf。

```shell
[client]
default_character_set=utf8
[mysqld]
collation_server = utf8_general_ci
character_set_server = utf8
```

### 4 验证

```shell
## 1. 重启并进入容器
docker restart 容器id
docker exec -it 容器id /bin/bash

## 2 进入mysql
mysql -uroot -p123456

## 3 验证
create database db01;  
use db01;  
create table bb(id int, name varchar(32));  
insert into bb values (1,'lisi');  
insert into bb values (2,'张三');  
select * from bb;
```

## 2 8.0单机安装

### 1 准备镜像

```shell
docker pull mysql:8.0.3
```

### 2 安装命令

```shell
docker run -d -p 3306:3306 --privileged=true \
-v /docker_data/mysql/8.0base/log:/var/log/mysql \
-v /docker_data/mysql/8.0base/data:/var/lib/mysql \
-v /docker_data/mysql/8.0base/conf:/etc/mysql/conf.d \
-v /docker_data/mysql/8.0base/mysql-files:/var/lib/mysql-files/ \
-e MYSQL_ROOT_PASSWORD=123456 \
--name mysql_base mysql:8.0.32
```

### 3 配置my.cnf

在conf目录下添加文件:my.cnf

```shell
[client]
default-character-set=utf8mb4
[mysqld]
# 指定认证插件
authentication_policy=mysql_native_password
```

> 1. mysql8服务端默认编码格式：utf8mb4，所以字符编码可以不设置。
> 2. 身份认证插件:在mysql5.7中插件为"mysql_navtive_password"，而在mysql8使用的插件是" caching_sha2_password"
     ，为了避免客户端连接时出现问题，所以需要设置一下。

### 4 验证

```shell
docker restart f19ccd623a7f
```

# 二、5.7主从安装

经典的一主一从安装，基于mysql5.7 版本。

## 1 搭建主服务器3307

### 1 安装命令

```shell
docker run -d -p 3307:3306 --privileged=true \  
-v /Users/hua/Documents/HuaAll/docker_data/mysql/5.7master/log:/var/log/mysql \  
-v /Users/hua/Documents/HuaAll/docker_data/mysql/5.7master/data:/var/lib/mysql \  
-v /Users/hua/Documents/HuaAll/docker_data/mysql/5.7master/conf:/etc/mysql \  
-e MYSQL_ROOT_PASSWORD=123456 \  
--name mysql_master mysql:5.7
```

### 2 新增并配置my.cnf

配置如下

```shell
[mysqld]
## 设置server_id，同一局域网中需要唯一
server_id=101
## 指定不需要同步的数据库名称
binlog-ignore-db=mysql
## 开启二进制日志功能
log-bin=mall-mysql-bin
## 设置二进制日志使用内存大小（事务）
binlog_cache_size=1M
## 设置使用的二进制日志格式（mixed,statement,row）
binlog_format=mixed
## 二进制日志过期清理时间。默认值为0，表示不自动清理。
expire_logs_days=7
## 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
## 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
slave_skip_errors=1062
```

### 3 重启master

```shell
docker restart 容器id
## 验证容器是否启动
docker ps
docker exec -it 容器id /bin/bash
```

### 4 新建同步用户

实际中不会用master进行同步，需要新建用户并分配权限，用于主从同步。

```shell
## 创建用户
CREATE USER 'slave'@'%' IDENTIFIED BY '123456';
## 分配权限
GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'slave'@'%';
```

### 5 查看主从同步状态

配置主从同步时需要使用

```shell
show master status;
```

返回结果

| File                  | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
|-----------------------|----------|--------------|------------------|-------------------|
| mall-mysql-bin.000001 | 617      |              | mysql            |                   |

## 2 搭建从服务器3308

### 1 安装命令

```shell
docker run -d -p 3308:3306 --privileged=true \  
-v /Users/hua/Documents/HuaAll/docker_data/mysql/5.7slave/log:/var/log/mysql \  
-v /Users/hua/Documents/HuaAll/docker_data/mysql/5.7slave/data:/var/lib/mysql \  
-v /Users/hua/Documents/HuaAll/docker_data/mysql/5.7slave/conf:/etc/mysql/conf.d \  
-e MYSQL_ROOT_PASSWORD=123456 \  
--name mysql_slave mysql:5.7
```

### 2 新建并配置my.cnf

```shell
[mysqld]
## 设置server_id，同一局域网中需要唯一
server_id=102
## 指定不需要同步的数据库名称
binlog-ignore-db=mysql
## 开启二进制日志功能，以备Slave作为其它数据库实例的Master时使用
log-bin=mall-mysql-slave1-bin
## 设置二进制日志使用内存大小（事务）
binlog_cache_size=1M
## 设置使用的二进制日志格式（mixed,statement,row）
binlog_format=mixed
## 二进制日志过期清理时间。默认值为0，表示不自动清理。
expire_logs_days=7
## 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
## 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
slave_skip_errors=1062
## relay_log配置中继日志
relay_log=mall-mysql-relay-bin
## log_slave_updates表示slave将复制事件写进自己的二进制日志
log_slave_updates=1
## slave设置为只读（具有super权限的用户除外）
read_only=1
```

### 3 重启master

```shell
docker restart 容器id
## 验证容器是否启动
docker ps
docker exec -it 容器id /bin/bash
```

### 4 配置主从同步

```shell
change master to master_host='192.168.1.1', master_user='slave', master_password='123456', master_port=3307, master_log_file='mall-mysql-bin.000001', master_log_pos=617, master_connect_retry=30;
```

> 配置介绍：
>
>
>
> 1. master_host：主数据库的IP地址，在此为宿主机ip；
> 2. master_port：主数据库的运行端口；
> 3. master_user：在主数据库创建的用于同步数据的用户账号；
> 4. master_password：在主数据库创建的用于同步数据的用户密码；
> 5. master_log_file：指定从数据库要复制数据的日志文件，通过查看主数据的状态，获取File参数；
> 6. master_log_pos：指定从数据库从哪个位置开始复制数据，通过查看主数据的状态，获取Position参
> 7. master_connect_retry：连接失败重试的时间间隔，单位为秒。

### 5 开启主从复制

```shell
start slave;
# 查看从数据库主从同步状态
```

# 三、8.0主从安装

经典的一主一从安装，基于mysql8.0以上版本。

## 1 搭建主服务器3307

### 1 安装命令

```shell
docker run -d -p 3307:3306 --privileged=true \
-v /Users/hua/Documents/HuaAll/docker_data/mysql/8.0master/log:/var/log/mysql \
-v /Users/hua/Documents/HuaAll/docker_data/mysql/8.0master/data:/var/lib/mysql \
-v /Users/hua/Documents/HuaAll/docker_data/mysql/8.0master/conf:/etc/mysql/conf.d \
-v /Users/hua/Documents/HuaAll/docker_data/mysql/8.0master/mysql-files:/var/lib/mysql-files/ \
-e MYSQL_ROOT_PASSWORD=123456 \
--name mysql_master mysql:8.0.32
```

### 2 新增并配置my.cnf

配置如下

```shell
[client]
default-character-set=utf8mb4

[mysqld]
## serverId,局域网内唯一
server_id=01

## 开启二进制日志功能
log-bin=ms-mysql-bin

## 指定不需要同步的数据库名称, 可指定多个
binlog-ignore-db=mysql
binlog-ignore-db=sys
binlog-ignore-db=information_schema
binlog-ignore-db=performance_schema


## 指定同步的数据库，可指定多个
# binlog-do-db=db01
# binlog-do-db=db02

## 二进制日志使用内存大小（事务）
binlog_cache_size=1M

## 二进制日志格式(mixed, statement,row)
# statement: 将写操作语句添加到binlog,然后在slave机运行。缺点：语句中有now()时，会造成数据不一致
# row: 记录每一行的改变,然后在slave机更新。缺点：批量更新时，会在slave机执行大量update
# mixed: 有类似now()这种会造成数据不一致的函数时，使用row模式，否则还是使用statement模式。
binlog_format=mixed

## 二进制日志过期清理时间，默认2592000秒（30天），604800秒（7天）
binlog_expire_logs_seconds=604800

## 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave复制中断
replica_skip_errors=1062

# 指定认证插件
authentication_policy=mysql_native_password
```

### 3 重启master

```shell
docker restart 容器id
## 验证容器是否启动
docker ps
docker exec -it 容器id /bin/bash
```

### 4 新建同步用户

实际中不会用master进行同步，需要新建用户并分配权限，用于主从同步。

```shell
## 创建用户
CREATE USER 'slave'@'%' IDENTIFIED BY '123456';
## 分配权限
GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'slave'@'%';
## 实际不用执行
FLUSH PRIVILEGES;
```

### 5 查看主从同步状态

配置主从同步时需要使用

```shell
show master status;
```

返回结果

| File                | Position | Binlog_Do_DB | Binlog_Ignore_DB                                | Executed_Gtid_Set |
|---------------------|----------|--------------|-------------------------------------------------|-------------------|
| ms-mysql-bin.000001 | 849      |              | mysql,sys,information_schema,performance_schema |                   |

## 2 搭建从服务器3308

### 1 安装命令

```shell
docker run -d -p 3307:3306 --privileged=true \
-v /Users/hua/Documents/HuaAll/docker_data/mysql/8.0master/log:/var/log/mysql \
-v /Users/hua/Documents/HuaAll/docker_data/mysql/8.0master/data:/var/lib/mysql \
-v /Users/hua/Documents/HuaAll/docker_data/mysql/8.0master/conf:/etc/mysql/conf.d \
-v /Users/hua/Documents/HuaAll/docker_data/mysql/8.0master/mysql-files:/var/lib/mysql-files/ \
-e MYSQL_ROOT_PASSWORD=123456 \
--name mysql_slave mysql:8.0.32
```

### 2 新建并配置my.cnf

```shell
[client]
default-character-set=utf8mb4

[mysqld]
## serverId,局域网内唯一
server_id=11

## 指定不需要同步的数据库名称
binlog-ignore-db=mysql

## 开启二进制日志功能
log-bin=ms-mysql-slave11-bin

## 二进制日志使用内存大小（事务）
binlog_cache_size=1M

## 二进制日志格式(mixed, statement,row)
binlog_format=mixed

## 二进制日志过期清理时间，默认2592000秒（30天），604800秒（7天）
binlog_expire_logs_seconds=604800

## 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave复制中断
replica_skip_errors=1062

## relay_log配置slave的中继日志
relay_log=ms-mysql-relay-bin

## 将复制事件写进自己的二进制日志
log_slave_updates=1

## 设置为只读（super权限用户除外）
read_only=1

# 指定认证插件
authentication_policy=mysql_native_password
```

### 3 重启slave

```shell
docker restart 容器id
## 验证容器是否启动
docker ps
docker exec -it 容器id /bin/bash
```

### 4 配置主从同步

```shell
# 停止slave
stop slave;

# 如果原来做过主从复制，可能还需要执行以下命令
## reset master;

# 连接到master
# host 是宿主机ip
ifconfig | grep 'inet'
# master_log_file和 master_log_pos的值参考master机状态查询的结果：
change master to master_host='192.168.0.101', master_user='slave', master_password='123456', master_port=3307,master_log_file='ms-mysql-bin.000001',master_log_pos=849,master_connect_retry=30;

# 启动slave
start slave;
```

> 配置介绍：
>
>
>
> 1. master_host：主数据库的IP地址，在此为宿主机ip；
> 2. master_port：主数据库的运行端口；
> 3. master_user：在主数据库创建的用于同步数据的用户账号；
> 4. master_password：在主数据库创建的用于同步数据的用户密码；
> 5. master_log_file：指定从数据库要复制数据的日志文件，通过查看主数据的状态，获取File参数；
> 6. master_log_pos：指定从数据库从哪个位置开始复制数据，通过查看主数据的状态，获取Position参
> 7. master_connect_retry：连接失败重试的时间间隔，单位为秒。

### 5 查看状态

```shell
show slave status \G;
```

重点：

Slave_IO_Running: Yes  
Slave_SQL_Running: Yes  
当两个值都为“Yes”时，说明连接成功，可在Master机上进行数据操作，而后在slave机看到数据同步效果。

其他  
如果查询slave
状态时，Slave_IO_Running的值一直是Connecting，有可能是账号密码问题或者权限问题，一般在下面可看到提示，例如：Last_IO_Error:
error connecting to master 'slave@192.168.10.121:13306' - retry-time: 30 retries: 1 message: Authentication plugin '
caching_sha2_password' reported error: Authentication requires secure connection.  
根据提示调整即可

### 6 测试

```shell
## master执行如下命令
create database db01;  
use db01;  
create table bb(id int, name varchar(32));  
insert into bb values (1,'lisi');  
insert into bb values (2,'张三');  
insert into bb values (3,'王五');  
select * from bb;

## slave验证
use db01;  
select * from bb;
```

