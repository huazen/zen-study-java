#### 单机模式

docker pull nacos/nacos-server:2.2.0

docker run -d --name nacos -p 8848:8848 -p 9848:9848 -p 9849:9849 \
-e MODE=standalone \
nacos/nacos-server:2.2.0

#### 集群模式

```yml
version: '3'
services:
  nacos1:
    image: nacos/nacos-server:2.2.0
    container_name: nacos-standalone-1
    environment:
      - PREFER_HOST_MODE=hostname
      - MODE=cluster
      - NACOS_SERVERS=nacos1:8848 nacos2:8848 nacos3:8848
    volumes:
      - ./nacos1/logs:/home/nacos/logs
      - ./nacos1/data:/home/nacos/data
    ports:
      - "8848:8848"
      - "9848:9848"
      - "9849:9849"
    depends_on:
      - mysql

  nacos2:
    image: nacos/nacos-server:2.2.0
    container_name: nacos-standalone-2
    environment:
      - PREFER_HOST_MODE=hostname
      - MODE=cluster
      - NACOS_SERVERS=nacos1:8848 nacos2:8848 nacos3:8848
    volumes:
      - ./nacos2/logs:/home/nacos/logs
      - ./nacos2/data:/home/nacos/data
    ports:
      - "8849:8848"
      - "9850:9848"
      - "9851:9849"
    depends_on:
      - mysql

  nacos3:
    image: nacos/nacos-server:2.2.0
    container_name: nacos-standalone-3
    environment:
      - PREFER_HOST_MODE=hostname
      - MODE=cluster
      - NACOS_SERVERS=nacos1:8848 nacos2:8848 nacos3:8848
    volumes:
      - ./nacos3/logs:/home/nacos/logs
      - ./nacos3/data:/home/nacos/data
    ports:
      - "8850:8848"
      - "9852:9848"
      - "9853:9849"
    depends_on:
      - mysql

  mysql:
    image: mysql:8.0
    container_name: mysql
    environment:
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_DATABASE=nacos_devtest
      - MYSQL_USER=nacos
      - MYSQL_PASSWORD=nacos
    volumes:
      - ./mysql/data:/var/lib/mysql
    ports:
      - "3306:3306"
```

echo 'export PATH="/opt/anaconda3/bin:$PATH"' >> ~/.zshrc