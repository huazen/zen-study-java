### 配置文件

```shell
# 看bash文件是 ~/.bash_profile 还是 ~/.zshrc
echo $SHELL
# 打开bash文件
open ~/.zshrc
```

### 端口IP命令

### 端口进程命令

```shell
# 查看指定端口的占用
sudo lsof -i tcp:port
sudo lsof -i tcp:8010

# 看到进程的PID，可以将进程杀死。
# 如：sudo kill -9 3210
sudo kill -9 PID
```

#### 查看IP

```shell
# 查看mac本机ip
ifconfig | grep 'inet'
# 如果失败，就安装ifconfig命令
yum -y install net-tools
ping 192.168.0.100
```

#### IP畅通

ping 测试ip是否畅通

```shell
ping www.baidu.com
ping -a <host_IP>

```

#### 端口畅通

```shell
# 方式1：telnet 查看端口
# ping的进阶版，可以看指定ip的端口能否ping通。
brew install telnet
telnet <ip> <port>

## 方式2
nc -vz -w 2 <host_IP> <port_number>

# 示例
hua@HuaMac Logs % nc -vz -w 2 10.211.55.7 3306
Connection to 10.211.55.7 port 3306 [tcp/mysql] succeeded!
```

### 远程交互

#### ssh 登录远程服务器

```shell
ssh user@remote-server.com
# 示例
ssh root@47.111.148.35
```

#### 文件交互

```shell
#### scp 特点：简单快速，无需额外安装。
# 上传本地文件到远程服务器
scp /local/path/file.txt user@remote-server:/remote/path/
# 下载远程文件到本地
scp user@remote-server:/remote/path/file.txt /local/path/

#### rsync 特点：仅传输差异文件，适合大目录同步
# 同步本地目录到远程服务器
rsync -avz /local/dir/ user@remote-server:/remote/dir/
# 从远程同步到本地
rsync -avz user@remote-server:/remote/dir/ /local/dir/

#### sftp 特点：支持交互式操作，适合大目录同步
sftp user@remote-server
sftp> put local-file.txt  # 上传
sftp> get remote-file.txt # 下载
````

```shell
scp -r local-file user@remote-server:remote-folder
# 示例
scp -r /Users/zhangsan/Downloads root@47.111.148.35:/data
scp mongodb-linux-x86_64-4.0.10.tgz root@47.111.148.35:/root
```

## 1 安装homebrew

[安装homebrew](https://gitee.com/cunkai/HomebrewCN?_from=gitee_search)

[切换镜像](https://mirrors.tuna.tsinghua.edu.cn/help/homebrew/)

[homebrew3.0以上版本开始支持m1](https://blog.csdn.net/LNF568611/article/details/123196726)

[m1 mac 报错解决](https://blog.csdn.net/Morris_/article/details/125182905)
