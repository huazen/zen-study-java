### 01_git配置
1. 安装完git后，可以使用下面命令更新git
```shell
git clone https://github.com/git/git
```

2. 配置git

```shell
git config --global user.name 'hua'
git config --global user.email '913078@qq.com'
```

3. SSH密钥方式
   mac下安装

```shell
# 查看是否有密钥
cd ~/.ssh
# 安装秘钥，三次enter
ssh-keygen -t rsa -C "913078@qq.com"
# 进入User目录下的.ssh文件夹
cd /Users/用户名/.ssh
# 打开并查看id_rsa.pub
cat id_rsa.pub
```

4. 连接密钥 <br/>
   如连接Gitlab ，网址是：http://gitlab.xxx.com/profile/keysm, 进入页面后点击`ADD SSH KEY`，就可以进入添加ssh key界面。

```shell
# 查看自己是否成功添加ssh key
ssh git@gitlab.alibaba-inc.com
```

> github、gitCode操作类似

5. 修改为https方式
   一个项目用ssh的方式clone，但后续可以换成http方式

```shell
# 后面的仓库地址根据实际使用替换
git remote set-url origin http://gitlab.xxx.com/bbb.git
```
