
package zen.base.spel;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 */
@Data
@AllArgsConstructor
public class MonitorData {
    /**
     * 应用名称
     */
    private String applicationName;
    /**
     * cpu 占用
     */
    private Integer cpu;
    /**
     * 内存占用
     */
    private Integer mem;
    /**
     * 线程数
     */
    private Integer threadNum;
}
