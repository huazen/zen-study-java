
package zen.base.spel;

import lombok.Setter;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringValueResolver;

/**
 * 表达式demo 可以用作用户自定义一些判断条件
 * 常用的三个对象的含义
 * Expression： 表达式对象
 * SpELExpressionParser：表达式解析器
 * EvaluationContext：上下文
 *
 * @author 散装java
 */
@Component
public class SpElSample {

    @Setter // zennote 实现了EmbeddedValueResolverAware的setEmbeddedValueResolver，解析属性占位符或表示式
    private StringValueResolver embeddedValueResolver;


    public void test() {
        String str = "#monitorData.cpu > 50 && #monitorData.mem > 50";
        MonitorData monitorData = new MonitorData("服务1", 60, 80, 300);
        // EvaluationContext：上下文
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("monitorData", monitorData);


        // SpELExpressionParser：表达式解析器 zennote 线程安全，可为常量
        ExpressionParser parser = new SpelExpressionParser();
        // Expression： 表达式对象
        Expression expression = parser.parseExpression(str);
        Boolean value = expression.getValue(context, Boolean.class);

        if (value != null && value) {
            System.out.println("========满足条件，执行后续操作,发送告警邮件");
        } else {
            System.out.println("========不满足条件，不通知");
        }
    }

    public void test2() {
        int cpu = 50;
        int mem = 50;
        boolean value = false;
        MonitorData monitorData = new MonitorData("服务1", 50, 20, 300);
        if (monitorData.getCpu() > cpu
                && monitorData.getMem() > mem) {
            value = true;
        }
        if (value) {
            System.out.println("========满足条件，执行后续操作,发送告警邮件");
        } else {
            System.out.println("========不满足条件，不通知");
        }
    }
}
