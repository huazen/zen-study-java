package zen.base.spel.method;

import lombok.Setter;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.context.expression.MapAccessor;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.expression.BeanResolver;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import org.springframework.util.ConcurrentReferenceHashMap;
import org.springframework.util.StringValueResolver;

import java.lang.reflect.Method;
import java.util.Map;
/*
    1. EmbeddedValueResolverAware接口
    org.springframework.context.EmbeddedValueResolverAware 是 Spring 框架中的一个接口，它允许 bean 感知并访问 Spring 的 EmbeddedValueResolver。
    EmbeddedValueResolver 是一个简单的工具，用于解析嵌入在字符串中的属性值，这些值通常用 ${...} 语法表示，类似于在 XML 配置文件或 @Value 注解中使用的占位符。

    当一个 bean 实现了 EmbeddedValueResolverAware 接口并覆盖了 setEmbeddedValueResolver 方法时，Spring 容器会在 bean 初始化时注入一个 EmbeddedValueResolver 实例。
    这样，bean 就可以在自己的代码中解析这些占位符，而不需要依赖 Spring 的其他组件（如 PropertyPlaceholderConfigurer）来自动解析它们。
*/

/**
 * <p>基于方法调用的SpEL表达式计算器，支持在表达式中通过下述方式引用相关参数：
 * <ul>
 *     <li>#p0, #p1...：按照参数顺序引用调用；</li>
 *     <li>#a0, #a1...：按照参数顺序引用调用；</li>
 *     <li>#参数名1, #参数名2...：按照参数名引用调用参数；</li>
 *     <li>#root：引用方法对象；</li>
 *     <li>@beanName：引用Spring容器中的Bean；</li>
 * </ul>
 *
 * @author huangchengxing
 * @see MethodBasedEvaluationContext
 * @see BeanFactoryResolver
 */
public class SpelMethodExpressionEvaluator
        implements MethodExpressionEvaluator, EmbeddedValueResolverAware, BeanFactoryAware {

    private static final MapAccessor MAP_ACCESSOR = new MapAccessor();
    private final Map<String, Expression> expressionCache = new ConcurrentReferenceHashMap<>(16);
    private final ExpressionParser expressionParser = new SpelExpressionParser();
    private final ParameterNameDiscoverer parameterNameDiscoverer = new DefaultParameterNameDiscoverer();
    private BeanResolver beanResolver;
    @Setter // zennote 实现了EmbeddedValueResolverAware的setEmbeddedValueResolver，解析属性占位符或表示式
    private StringValueResolver embeddedValueResolver;

    /**
     * 执行表达式，返回执行结果
     *
     * @param method     方法
     * @param arguments  调用参数
     * @param expression 表达式
     * @param resultType 返回值类型
     * @param variables  表达式中的变量
     * @return 表达式执行结果
     */
    @Override
    public <T> T getValue(
            Method method, Object[] arguments, String expression, Class<T> resultType, @NonNull Map<String, Object> variables) {
        // 1 创建表达式执行上下文
        EvaluationContext context = createEvaluationContext(method, arguments);
        if (!variables.isEmpty()) {
            // zennote entry-foreach
            // 2 添加表达式变量
            variables.forEach(context::setVariable);
        }
        // 3 解析表达式获取表达式对象
        Expression exp = parseExpression(expression, expressionParser);
        return exp.getValue(context, resultType);
    }

    /**
     * 创建表达式执行上下文
     *
     * @param method 调用方法
     * @param args   调用参数
     * @return 表达式执行上下文
     */
    protected EvaluationContext createEvaluationContext(Method method, Object[] args) {
        MethodBasedEvaluationContext context = new MethodBasedEvaluationContext(method, method, args, parameterNameDiscoverer);
        context.setBeanResolver(beanResolver);
        context.addPropertyAccessor(MAP_ACCESSOR);
        return context;
    }

    /**
     * 解析表达式
     *
     * @param expression 表达式
     * @param parser     表达式解析器
     * @return 表达式对象
     */
    protected Expression parseExpression(String expression, ExpressionParser parser) {
        return expressionCache.computeIfAbsent(expression, exp -> {
            // 解析属性占位符或表达式
            exp = embeddedValueResolver.resolveStringValue(exp);
            Assert.notNull(exp, "Expression must not be null: " + exp);
            return parser.parseExpression(exp);
        });
    }

    @Override
    public void setBeanFactory(@NonNull BeanFactory beanFactory) {
        beanResolver = new BeanFactoryResolver(beanFactory);
    }
}
