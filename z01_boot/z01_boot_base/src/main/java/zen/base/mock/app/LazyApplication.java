package zen.base.mock.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author: ZEN
 * @create: 2024-09-21 22:28
 **/
public class LazyApplication {
    public static void main(String[] args) {
        // 方式1：
        SpringApplication sa = new SpringApplication(LazyApplication.class);

        sa.setLazyInitialization(true);
        sa.run(args);

        // 方式2：
        SpringApplicationBuilder sab = new SpringApplicationBuilder(LazyApplication.class);

        sab.lazyInitialization(true).run(args);
    }
}
