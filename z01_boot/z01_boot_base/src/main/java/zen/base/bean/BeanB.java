package zen.base.bean;

import org.springframework.stereotype.Component;

/**
 * @author: ZEN
 * @create: 2024-09-07 21:30
 * <pre>
 *     要求：注入静态bean
 * </pre>
 **/
@Component
public class BeanB {

    // 方式1： 通过 @Resource 或 @Autowired注入 (不可行)
//    @Resource
    private static BeanA beanA;


    // 方式2： 通过构造器注入 （可行）
    public BeanB(BeanA beanA) {
        BeanB.beanA = beanA;
    }

    // 方式3： ApplicationContext 注入 （可行）
}
