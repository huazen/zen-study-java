package zen.base.async;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2022-09-11
 */
@RestController
public class AsyncDemoController {
    @Resource
    AsyncDemo asyncDemo;

    @GetMapping("/asyncTest")
    public String asyncTest() {
        asyncDemo.m1();
        return "ok";
    }
}
