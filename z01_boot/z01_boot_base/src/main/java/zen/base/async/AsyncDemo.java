package zen.base.async;

import lombok.SneakyThrows;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 分享一个 Async 注解，使用时的小问题 - 失效问题
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2022-09-11
 */
@Component
public class AsyncDemo {
    @Resource
    AsyncComponent asyncComponent;
    @Lazy
    @Resource
    AsyncDemo asyncDemo;

    public void m1() {
        task();
        // 巴拉巴拉的业务代码
        System.out.println(Thread.currentThread().getName() + "=====m1-执行完毕");
    }

    public void m2() {
        asyncComponent.task1();
        // 巴拉巴拉的业务代码
        System.out.println(Thread.currentThread().getName() + "=====m2-执行完毕");
    }


    public void m3() {
        asyncDemo.task();
        // 巴拉巴拉的业务代码
        System.out.println(Thread.currentThread().getName() + "=====m3-执行完毕");
    }

    @SneakyThrows
    @Async
    public void task() {
        TimeUnit.SECONDS.sleep(1);
        System.out.println(Thread.currentThread().getName() + "=====task-执行完毕");
    }
}
