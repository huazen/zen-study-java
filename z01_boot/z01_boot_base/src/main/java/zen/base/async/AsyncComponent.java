package zen.base.async;

import lombok.SneakyThrows;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2022-09-11
 */
@Component
public class AsyncComponent {

    @SneakyThrows
    @Async
    public void task1() {
        TimeUnit.SECONDS.sleep(1);
        System.out.println(Thread.currentThread().getName() + "=====task1-执行完毕");
    }
}
