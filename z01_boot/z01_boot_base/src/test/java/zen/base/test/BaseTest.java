package zen.base.test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import zen.base.BaseApplication;

/**
 * @author: ZEN
 * @create: 2024-09-07 23:07
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BaseApplication.class)
public class BaseTest {


}
