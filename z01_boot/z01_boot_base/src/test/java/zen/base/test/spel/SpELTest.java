package zen.base.test.spel;

import org.aopalliance.intercept.MethodInvocation;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import zen.base.spel.SpElSample;
import zen.base.spel.method.MethodExpressionEvaluator;
import zen.base.test.BaseTest;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpELTest extends BaseTest {

    @Autowired
    SpElSample spElSample;

    @Resource
    private MethodExpressionEvaluator methodExpressionEvaluator;

    @Test
    public void spElTest() {
        spElSample.test();
    }


    /**
     * 通过 aop 的方法拦截，获取参数值，进行解析
     * 不做实现，只是参考代码
     */
    @Test
    public void methodSpElTest() {
        MethodInvocation invocation = null; // aop 拦截到的方法
        String[] definitionKeys = null;  // 需要解析的spEl字符串列表
        Method method = invocation.getMethod();
        Object[] arguments = invocation.getArguments();
        Stream.of(definitionKeys)
                .filter(StringUtils::hasText)
                .map(k -> methodExpressionEvaluator.getValue(method, arguments, k, String.class))
                .collect(Collectors.joining("."));
    }
}