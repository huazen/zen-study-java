package zen.base.test.async;

import lombok.SneakyThrows;
import org.junit.Test;
import zen.base.async.AsyncDemo;
import zen.base.test.BaseTest;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author: ZEN
 * @create: 2024-09-07 23:45
 **/
public class AsyncTest extends BaseTest {

    @Resource
    private AsyncDemo asyncDemo;


    /**
     * 自调用导致异步失效-没有走spring代理
     */
    @SneakyThrows
    @Test
    public void test1() {
        asyncDemo.m1();

        TimeUnit.MINUTES.sleep(1);
    }

    /**
     * 调用其他类的异步方法：成功
     */
    @SneakyThrows
    @Test
    public void test2() {
        asyncDemo.m2();
        TimeUnit.MINUTES.sleep(1);
    }

    /**
     * 把自身实现作为参数，调用其异步方法：成功
     */
    @SneakyThrows
    @Test
    public void test3() {
        asyncDemo.m3();
        TimeUnit.MINUTES.sleep(1);
    }
}
