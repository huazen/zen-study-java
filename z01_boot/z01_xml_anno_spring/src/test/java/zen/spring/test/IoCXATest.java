package zen.spring.test;


import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import zen.spring.dao.AccountDao;
import zen.spring.pojo.Account;
import zen.spring.service.TransferService;

/**
 * @author 应癫
 */
public class IoCXATest {


    @Test
    public void testIoC() throws Exception {

        // 方式1: 通过读取classpath下的xml文件来启动容器（xml模式SE应用下推荐）
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        // 方式2: 不推荐使用
        //ApplicationContext applicationContext1 = new FileSystemXmlApplicationContext("文件系统的绝对路径");


        /*================  创建bean的几种方式     ===============*/

        AccountDao accountDao = (AccountDao) applicationContext.getBean("accountDao");
        System.out.println("accountDao：" + accountDao);
        AccountDao accountDao1 = (AccountDao) applicationContext.getBean("accountDao");
        System.out.println("accountDao1：" + accountDao1);

        Account account = accountDao.queryAccountByCardNo("6029621011001");
        System.out.println("查询到的account: " + account);

        Object connectionUtils = applicationContext.getBean("connectionUtils");
        System.out.println(connectionUtils);


        // 关闭容器时，执行 destroy-method
        applicationContext.close();
    }


    /**
     * 测试bean的lazy-init属性
     */
    @Test
    public void testBeanLazy() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        System.out.println("============== IoC容器初始化完成  =================");
        // case1: xml方式
        Object lazyResult = applicationContext.getBean("lazyResult");
        System.out.println(lazyResult);

        // case2: 注解方式，注意要和 @Component同类注解 一起使用
        Object annoLazyResult = applicationContext.getBean("annoLazyResult");
        System.out.println(annoLazyResult);

        applicationContext.close();
    }

    @Test
    public void testBeanProcess() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        System.out.println("============== IoC容器初始化完成  =================");

        applicationContext.getBean("beanProcessTest");

        applicationContext.close();
    }


    /**
     * 测试基于Aop的官方事务管理器
     */
    @Test
    @SneakyThrows
    public void testTx() {
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        TransferService transferService = applicationContext.getBean("transferService", TransferService.class);
        transferService.transfer("6029621011000", "6029621011001", 100);
    }
}