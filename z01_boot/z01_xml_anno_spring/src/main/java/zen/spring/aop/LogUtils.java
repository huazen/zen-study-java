package zen.spring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author 应癫
 */
@Component
@Aspect // {@code 代替`<aop:config>`， 配置切面}
@Order(1) // {@code 代替 <aop:around order> ， 指定切面的优先级，值越小优先级越高}
public class LogUtils {


    /**
     * {@code  代替`<aop:pointcut>`， 配置切点}
     */
    @Pointcut("execution(* zen.spring.service.impl.TransferServiceImpl.*(..))")
    public void pt1() {

    }


    /**
     * 业务逻辑开始之前执行
     * {@code 代替`<aop:before>`， 前置通知 }
     */
    @Before("pt1()")
    public void beforeMethod(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            System.out.println(arg);
        }
        System.out.println("业务逻辑开始执行之前执行.......");
    }


    /**
     * 业务逻辑结束时执行（无论异常与否）
     * {@code 代替`<aop:after>`， 后置通知 }
     */
    @After("pt1()")
    public void afterMethod() {
        System.out.println("业务逻辑结束时执行，无论异常与否都执行.......");
    }


    /**
     * 异常时时执行
     * {@code 代替`<aop:after-throwing>`， 异常通知 }
     */
    @AfterThrowing("pt1()")
    public void exceptionMethod() {
        System.out.println("异常时执行.......");
    }


    /**
     * 业务逻辑正常时执行
     * {@code 代替`<aop:after-returning>`， 返回通知 }
     */
    @AfterReturning(value = "pt1()", returning = "retVal")
    public void successMethod(Object retVal) {
        System.out.println("业务逻辑正常时执行.......");
    }


    /**
     * 环绕通知
     * {@code 代替`<aop:around>`， 环绕通知 }
     */
    /*@Around("pt1()")*/
    public Object arroundMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕通知中的beforemethod....");

        Object result = null;
        try {
            // 控制原有业务逻辑是否执行
            // result = proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
        } catch (Exception e) {
            System.out.println("环绕通知中的exceptionmethod....");
        } finally {
            System.out.println("环绕通知中的after method....");
        }

        return result;
    }

}
