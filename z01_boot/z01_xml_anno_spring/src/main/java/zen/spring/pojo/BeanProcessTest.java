package zen.spring.pojo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author: ZEN
 * @create: 2024-09-21 22:44
 **/
@Component("beanProcessTest")
public class BeanProcessTest implements
        BeanNameAware,
        BeanFactoryAware,
        ApplicationContextAware,
        InitializingBean,
        DisposableBean,
        SmartInitializingSingleton {

    @Override
    public void setBeanName(String name) {

        System.out.println("BeanNameAware接口执行，获取注册bean时定义的id：" + name);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("BeanFactoryAware接口执行：获取的beanfactory");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("ApplicationContextAware接口执行：获取高级容器applicationContext");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBean接口执行......");
    }


    @PostConstruct
    public void postCoustrcut() {
        System.out.println("@PostConstruct注解方法....");
    }


    @PreDestroy
    public void preDestroy() {
        System.out.println("@PreDestroy注解方法...");
    }


    @Override
    public void destroy() throws Exception {
        System.out.println("DisposableBean接口执行.....");
    }

    @Override
    public void afterSingletonsInstantiated() {
        System.out.println("SmartInitializingSingleton接口执行（所有单例 Bean 被实例化之后调用）: .....");
    }

}
