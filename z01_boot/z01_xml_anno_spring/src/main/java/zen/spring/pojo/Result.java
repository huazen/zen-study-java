package zen.spring.pojo;

import javax.annotation.PostConstruct;

/**
 * @author 应癫
 */
public class Result {

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Result{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }


    @PostConstruct
    public void initMethod() {
        System.out.println("init-method....");
    }

}
