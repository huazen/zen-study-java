package zen.spring.pojo;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author 应癫
 */
@Lazy
@Component("annoLazyResult")
public class AnnoLazyResult {

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "AnnoLazyResult{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    @PostConstruct
    public void initMethod() {
        System.out.println("init-method....");
    }

}
