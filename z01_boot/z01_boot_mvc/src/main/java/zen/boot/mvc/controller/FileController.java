package zen.boot.mvc.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author: ZEN
 * @create: 2024-08-11 19:23
 **/
@Controller
@RequestMapping("/file")
public class FileController {


    @Value(value = "${upload.file.path}")
    private String uploadPath;

    /**
     * 文件上传
     *
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/upload")
    public String upload(@RequestParam("uploadFile") MultipartFile uploadFile,
                         HttpSession session) throws IOException {

        // 处理上传文件
        // 重命名，原名123.jpeg ，获取后缀
        String originalFilename = uploadFile.getOriginalFilename();// 原始名称
        // 扩展名  jpg
        String ext = originalFilename.substring(originalFilename.lastIndexOf(".") + 1,
                originalFilename.length());
        String newName = UUID.randomUUID().toString() + "." + ext;

        // 存储,要存储到指定的文件夹，/uploads/yyyy-MM-dd，考虑文件过多的情况按照日期，
        // 生成一个子文件夹
        File filePath = ResourceUtils.getFile(uploadPath);

        // 存储文件到目录
        uploadFile.transferTo(new File(filePath, newName));

        return "OK";
    }
}
