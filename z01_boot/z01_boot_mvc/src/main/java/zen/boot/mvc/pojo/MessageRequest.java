package zen.boot.mvc.pojo;

import lombok.Data;

import java.util.List;

@Data
public class MessageRequest {
    private List<String> msPlatform;
    private SourcePlatform sourcePlatform;
    private List<String> receiver;
    private List<String> receiveDepts;
    private MsgParams msgParams;
    private Mssms mssms;
    private Msfeishu msfeishu;
    private Msmail msmail;
    private boolean isMass;

    @Data
    public static class SourcePlatform {
        private String id;
        private String name;
    }

    @Data
    public static class MsgParams {
        private String p1;
        private String p2;
        private String p3;
    }

    @Data
    public static class Mssms {
        private MsSmsTemplate msSmsTemplate;
        private String msgType;
    }

    @Data
    public static class Msfeishu {
        private MsSmsTemplate msSmsTemplate;
        private String msgType;
        private String appId;
        private String mode;
        private String templateVersion;
    }

    @Data
    public static class Msmail {
        private MsSmsTemplate msSmsTemplate;
        private List<String> carbonCopy;
        private String title;
        private String attachments;
    }

    @Data
    public static class MsSmsTemplate {
        private int id;
    }
}