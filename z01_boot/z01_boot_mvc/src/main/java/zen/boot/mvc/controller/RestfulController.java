package zen.boot.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import zen.boot.mvc.pojo.MessageRequest;

import java.util.Date;

/**
 * @author: ZEN
 * @create: 2024-08-08 20:41
 **/
@Controller
@RequestMapping("/restful")
public class RestfulController {

    /*
     * restful  get  /demo/handle/15
     */
    @RequestMapping(value = "/handle/{id}", method = {RequestMethod.GET})
//    @GetMapping(value = "/handle/{id}") // 等价
    public ModelAndView handleGet(@PathVariable("id") Integer id) {

        Date date = new Date();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("date", date);
        modelAndView.setViewName("success");
        return modelAndView;
    }


    /*
     * restful  post  /demo/handle
     */
    @RequestMapping(value = "/handle", method = {RequestMethod.POST})
//    @PostMapping(value = "/handle") // 等价
    public ModelAndView handlePost(String username) {

        Date date = new Date();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("date", date);
        modelAndView.setViewName("success");
        return modelAndView;
    }


    /*
     * restful  put  /demo/handle/15/lisi
     */
    @RequestMapping(value = "/handle/{id}/{name}", method = {RequestMethod.PUT})
//    @PutMapping(value = "/handle/{id}/{name}") // 等价
    public ModelAndView handlePut(@PathVariable("id") Integer id, @PathVariable("name") String username) {

        Date date = new Date();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("date", date);
        modelAndView.setViewName("success");
        return modelAndView;
    }


    /*
     * restful  delete  /demo/handle/15
     */
    @RequestMapping(value = "/handle/{id}", method = {RequestMethod.DELETE})
//    @DeleteMapping(value = "/handle/{id}") // 等价
    public ModelAndView handleDelete(@PathVariable("id") Integer id) {

        Date date = new Date();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("date", date);
        modelAndView.setViewName("success");
        return modelAndView;
    }


    /**
     * 测试post请求
     * <pre>
     * {@code @ResponseBody } 注解，将返回的数据直接写入http response body中，而不是跳转页面
     * </pre>
     *
     * @param messageRequest
     * @return
     */
    @PostMapping(value = "/handlePost2")
    @ResponseBody
    public MessageRequest handlePost2(@RequestBody MessageRequest messageRequest) {
        return messageRequest;
    }
}
