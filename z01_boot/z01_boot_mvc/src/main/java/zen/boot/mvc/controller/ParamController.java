package zen.boot.mvc.controller;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.assertj.core.util.Maps;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: ZEN
 * @create: 2024-08-08 20:45
 **/
// ResponseBody(返回json字符串) + Controller
@RestController
@RequestMapping("/param")
public class ParamController {
    /*
     * SpringMVC 对原生servlet api的支持  url：/param/handle02?id=1
     * <p>
     * 如果要在SpringMVC中使用servlet原生对象，
     * 比如HttpServletRequest\HttpServletResponse\HttpSession，
     * 直接在Handler方法形参中声明使用即可
     */
    @RequestMapping("/handleRequest")
    public Map<String, String> handleRequest(HttpServletRequest request, HttpServletResponse response,
                                             HttpSession session) {
        String id = request.getParameter("id");
        Map<String, String> map = Maps.newHashMap("request", id);
        return map;
    }


    /*
     * SpringMVC 接收简单数据类型参数  -- 参数名和形参名称保持一致
     * usl1: /param/handleSimple01?id=1&flag=true
     * url2：/param/handleSimple01?id=1&flag=1
     * 上面两种url效果一样
     */
    @RequestMapping("/handleSimple01")
    public Map<String, Object> handleSimple01(Integer id, Boolean flag) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("flag", flag);
        return map;
    }


    /*
     * SpringMVC 接收简单数据类型参数
     * url：/param/handleSimple01?ids=1
     *
     * @RequestParam 可用于解决形参参数名和传递参数名不⼀致的情况
     */
    @RequestMapping("/handleSimple02")
    public Map<String, Object> handleSimple02(@RequestParam("ids") Integer id) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        return map;
    }

    /*
     * SpringMVC接收pojo类型参数  url：?id=1&name=zhangsan
     *
     * 接收pojo类型参数，直接形参声明即可，类型就是Pojo的类型，形参名无所谓
     * 但是要求传递的参数名必须和Pojo的属性名保持一致
     */
    @RequestMapping("/handlePojo01")
    public User handlePojo01(User user) {
        return user;
    }

    /*
     * SpringMVC接收pojo包装类型参数  url：?user.id=1&user.name=zhangsan
     * 不管包装Pojo与否，它首先是一个pojo，那么就可以按照上述pojo的要求来
     * 1、绑定时候直接形参声明即可
     * 2、传参参数名和pojo属性保持一致，如果不能够定位数据项，
     * 那么通过属性名 + "." 的方式进一步锁定数据
     *
     */
    @RequestMapping("/handlePojo02")
    public QueryVo handlePojo02(QueryVo queryVo) {
        return queryVo;
    }

    /**
     * 绑定日期类型参数
     *
     * @param birthday
     * @return
     */
    @RequestMapping("/handleDate01")
    public String handleDate01(@DateTimeFormat(pattern = "yyyy-MM-dd") Date birthday) {
        return DateUtil.formatDateTime(birthday);
    }

    @RequestMapping("/handleDate02")
    public QueryVo handleDate02(QueryVo queryVo) {
        return queryVo;
    }

    @RequestMapping("/token")
    public String token(User user) {
        return "token:" + user.getName();
    }


    @RequestMapping("/authorization")
    public String authorization(HttpServletRequest request) {
        return "Authorization:" + request.getHeader("Authorization");
    }


    @Data
    public static class QueryVo {

        private User user;
        // 响应，因与接收格式不一致，时分秒为16:00:00
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        // 接收
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date birthday;
    }

    @Data
    public static class User {
        private Integer id;
        private String name;
        private String password;
    }
}
