package zen.configuration;

import lombok.Data;

/**
 * @author: ZEN
 * @create: 2024-09-07 21:14
 **/
@Data
//@ConfigrationProperties(prefix = "zen.config")
public class ZenProperties {

    private String name;

    private Integer age;

}
