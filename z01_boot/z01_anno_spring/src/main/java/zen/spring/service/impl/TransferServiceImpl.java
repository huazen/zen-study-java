package zen.spring.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zen.spring.dao.AccountDao;
import zen.spring.pojo.Account;
import zen.spring.service.TransferService;


/**
 * @author 应癫
 */
@Service("transferService")
public class TransferServiceImpl implements TransferService {


    // 最佳状态
    // @Autowired 按照类型注入 ,如果按照类型无法唯一锁定对象，可以结合@Qualifier指定具体的id
    @Autowired
    @Qualifier("jdbcTemplateDao")
    private AccountDao accountDao;


    @Override
    @Transactional(transactionManager = "dataSourceTransactionManager")
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {


        Account from = accountDao.queryAccountByCardNo(fromCardNo);
        Account to = accountDao.queryAccountByCardNo(toCardNo);

        from.setMoney(from.getMoney() - money);
        to.setMoney(to.getMoney() + money);

        accountDao.updateAccountByCardNo(to);
        int c = 1 / 0;
        accountDao.updateAccountByCardNo(from);

    }
}
