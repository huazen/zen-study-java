package zen.spring.test;


import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import zen.spring.SpringConfig;
import zen.spring.dao.AccountDao;
import zen.spring.service.TransferService;

/**
 * @author 应癫
 */
public class IoCATest {


    @Test
    public void testIoC() throws Exception {

        // 通过读取配置类信息来启动容器
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);

        AccountDao accountDao = (AccountDao) applicationContext.getBean("accountDao");

        System.out.println(accountDao);
    }


    @Test
    @SneakyThrows
    public void testTx() {
        // 通过读取配置类信息来启动容器
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        TransferService transferService = applicationContext.getBean(TransferService.class);
        transferService.transfer("6029621011000", "6029621011001", 100);
    }
}
