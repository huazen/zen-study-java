package zen.spring.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import zen.spring.SpringConfig;
import zen.spring.dao.AccountDao;
import zen.spring.pojo.Account;

import javax.annotation.Resource;

/**
 * @author: ZEN
 * @create: 2024-09-22 01:39
 **/
@RunWith(SpringJUnit4ClassRunner.class)
// 配置文件方式
//@ContextConfiguration("classpath:applicationContext.xml")
@ContextConfiguration(classes = SpringConfig.class)
public class SpringATest {

    @Resource(name = "jdbcTemplateDao")
    private AccountDao accountDao;

    @Test
    public void test() throws Exception {
        Account account = accountDao.queryAccountByCardNo("6029621011000");
        System.out.println(account);
    }
}
