package zen.file.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * service
 */
public interface FileUploadService {
    /**
     * 上传并且切图
     *
     * @param file 文件
     * @param uuid uuid
     */
    void uploadAndCut(MultipartFile file, String uuid);

    List<String> getImgList(String uuid);
}
