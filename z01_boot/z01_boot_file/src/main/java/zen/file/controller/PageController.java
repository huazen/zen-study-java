package zen.file.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author sxd
 */
@Controller
public class PageController {

    @GetMapping("/page/{path}")
    public String toPage(@PathVariable String path) {
        return path;
    }
}
