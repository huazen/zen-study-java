package zen.file.controller;

import cn.hutool.core.lang.UUID;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import zen.file.service.FileUploadService;

import java.util.List;

/**
 * 上传文件 控制层
 *
 * @author 散装java
 * @date 2022-08-19
 */
@RestController
@AllArgsConstructor
public class FileUploadController {
    private final FileUploadService fileUploadService;

    @PostMapping("/imgUpload")
    public String upload(@RequestParam("file") MultipartFile file) {
        String uuid = UUID.fastUUID().toString();
        fileUploadService.uploadAndCut(file, uuid);
        return uuid;
    }

    @GetMapping("/getImgList/{uuid}")
    public List<String> getImgList(@PathVariable("uuid") String uuid) {
        return fileUploadService.getImgList(uuid);
    }
}
