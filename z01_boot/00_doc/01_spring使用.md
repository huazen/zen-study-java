# spring相关
## 总结

### 启动IoC容器的方式

[JavaSE下纯xml-两种#:testIoC](../z01_xml_spring/src/test/java/zen/spring/test/IoCTest.java), <br/>
[JavaSE下纯anno:testIoC](../z01_anno_spring/src/test/java/zen/spring/test/IoCATest.java), <br/>
web环境：<br/>
[web环境下纯xml](../z01_xml_spring/src/main/webapp/WEB-INF/web.xml)<br/>
[web环境下配置类](../z01_anno_spring/src/main/webapp/WEB-INF/web.xml)<br/>

### Aop接入
[纯xml接入AOP](../z01_xml_spring/src/main/resources/applicationContext_aop.xml)<br/>
[anno方式接入AOP](../z01_xml_anno_spring/src/main/java/zen/spring/aop/LogUtils.java)<br/>

### 事务接入

[纯xml接入tx](../z01_xml_spring/src/main/resources/applicationContext_aop_tx.xml)<br/>
[anno方式接入tx](../z01_xml_anno_spring/src/main/java/zen/spring/service/impl/TransferServiceImpl.java)<br/>

### 高级特性
#### 生命周期
配置文件方式
```xml

<bean id="exampleBean" class="com.example.ExampleBean" scope="prototype"/>
```
Java 配置类中使用 `@Scope` 注解来指定作用域：

#### 延迟加载
ApplicationContext 容器的默认⾏为是在启动服务器时将所有 singleton bean 提前进⾏实例化。
1. 开启延迟加载⼀定程度提⾼容器启动和运转性能
2. 对于不常使⽤的 Bean 设置延迟加载，这样偶尔使⽤的时候再加载，不必要从⼀开始该 Bean 就占⽤资源

配置文件方式
```xml
<bean id="exampleBean" class="com.example.ExampleBean" lazy-init="true"/>
```
Java 配置类中使用 `@Lazy` 注解来指定延迟加载：<br/>
> 注意要和 @Component同类注解 一起使用 <br/>
> 示例:[testBeanLazy](../z01_xml_anno_spring/src/test/java/zen/spring/test/IoCXATest.java)

默认延迟实例化：
```xml
<beans default-lazy-init="true">
   <!-- no beans will be eagerly pre-instantiated... -->
</beans>
```
或者springboot中进行配置
```properties
spring.main.lazy-initialization=true
```

或者在启动类[实现](../z01_boot_base/src/main/java/zen/base/mock/app/LazyApplication.java)

### 前置后置处理器

> BeanFactoryPostProcessor、BeanPostProcessor、ApplicationListener一定要添加类似@Component注解，才能生效。<br/>
> [相关示例类](../z01_xml_anno_spring/src/main/java/zen/spring/pojo)：BeanProcessTest、StartupListener、
> MyBeanFactoryPostProcessor、MyBeanPostProcessor <br/>
> [执行示例：testBeanProcess](../z01_xml_anno_spring/src/test/java/zen/spring/test/IoCXATest.java)

## 架构介绍

### 1. 纯xml：z01_xml_spring

首先是纯原生spring.xml的项目`z01_xml_spring`,
具体实现：

1. spring.xml实现IoC容器，实现对象管理。
2. 自动义代理实现事务管理。
   重要入口如下

#### JavaSE下IoC容器启动

[JavaSE下IoC容器启动](../z01_xml_spring/src/test/java/zen/spring/test/IoCTest.java), <br/>

```xml
 <!--引入Spring Ioc 容器功能-->
<dependency>
   <groupId>org.springframework</groupId>
   <artifactId>spring-context</artifactId>
   <version>5.1.2.RELEASE</version>
</dependency>
```

#### JavaWeb下IoC容器启动

> 现在`applicationContext.xml` 只配置了IoC容器相关。

1. [配置web.xml](../z01_xml_spring/src/main/webapp/WEB-INF/web.xml) <br/>

```xml
  <!--引入spring web功能-->
<dependency>
   <groupId>org.springframework</groupId>
   <artifactId>spring-web</artifactId>
   <version>5.1.12.RELEASE</version>
</dependency>
```

2. [修改servlet](../z01_xml_spring/src/main/java/zen/spring/servlet/TransferServlet.java) <br/>
3. tomcat的maven插件启动:`mvn tomcat7:run`

```xml

<plugin>
   <groupId>org.apache.tomcat.maven</groupId>
   <artifactId>tomcat7-maven-plugin</artifactId>
   <version>2.2</version>
   <configuration>
      <port>8080</port>
      <path>/</path>
   </configuration>
</plugin>
```

测试：http://localhost:8080/

#### 纯xml接入AOP

`applicationContext_aop.xml` IoC容器+AOP

```xml
<!--spring aop的jar包支持-->
<dependency>
   <groupId>org.springframework</groupId>
   <artifactId>spring-aop</artifactId>
   <version>5.1.12.RELEASE</version>
</dependency>

        <!--第三方的aop框架aspectj的jar-->
<dependency>
<groupId>org.aspectj</groupId>
<artifactId>aspectjweaver</artifactId>
<version>1.8.13</version>
</dependency>
```

启动IOC容器，测试：http://localhost:8080/

#### 纯xml事务配置

`applicationContext_aop_tx.xml` IoC容器+AOP+事务

1. 在context、aop依赖基础上，引入事务依赖

```xml

<dependency>
   <groupId>org.springframework</groupId>
   <artifactId>spring-context</artifactId>
   <version>5.1.12.RELEASE</version>
</dependency>
<dependency>
<groupId>org.springframework</groupId>
<artifactId>spring-jdbc</artifactId>
<version>5.1.12.RELEASE</version>
</dependency>
```

2. 改造AccountDao实现类，使用jdbcTemplate，使执行sql和事务管理用的同一个数据源连接。

### 2. xml+anno: z01_xml_anno_spring

#### IoC相关改造

1. 配置文件种配置扫描哪些包：`<context:component-scan>`
2. 类上添加注解代替bean标签配置：
   - `@Component`同类 代替`<bean`标签
   - `@Scope("prototype")`作用域 代替`<bean scope="prototype">`
   - `@Autowired`同类注解注入，byName注入， 代替`<property ref="xxx">`
   - `@PostConstruct`注解 代替`<bean init-method`
   - `@PreDestroy`注解 代替`<bean destroy-method`
3. `@Resource` 是`@Autowired`同类注解
   - 如果同时指定了 name 和 type，则从Spring上下⽂中找到唯⼀匹配的bean进⾏装配，找不到则抛出异常。
   - 如果指定了 name，则从上下⽂中查找名称（id）匹配的bean进⾏装配，找不到则抛出异常。
   - 如果指定了 type，则从上下⽂中找到类型匹配的唯⼀bean进⾏装配，找不到或是找到多个， 都会抛出异常。
   - 如果既没有指定name，⼜没有指定type，则⾃动按照byName⽅式进⾏装配；

> @Resource 在 Jdk 11中已经移除，如果要使⽤，需要单独引⼊jar包
> ```xml
> <dependency>
>  <groupId>javax.annotation</groupId>
>  <artifactId>javax.annotation-api</artifactId>
>  <version>1.3.2</version>
> </dependency>
> ```

#### AOP相关改造

1. xml配置开启注解AOP的支持

```xml
<!--开启spring对注解aop的⽀持, proxy-target-class="true"则强制开启cglib动态代理-->
<aop:aspectj-autoproxy/> 
```

2. 配置方式改为修改切面类[切面类示例](../z01_xml_anno_spring/src/main/java/zen/spring/aop/LogUtils.java)

#### 事务相关改造

1. 配置文件中保留：transactionManager等配置
1. 开启注解事务支持

```xml
<!-- 之前省略datsSource、jdb cTe mpl ate、平台事务管理器的配置 -->
<!--组件扫描-->
<context:component-scan base-package="zen.hua"/>
        <!--开启spring对注解事务的⽀持-->
<tx:annotation-driven transaction-manager="transactionManager"/>
```

2. 只改造事务切面[service改造](../z01_xml_anno_spring/src/main/java/zen/spring/service/impl/TransferServiceImpl.java)

> `@Transactional` 替换掉 `<tx:advice + <aop:advisor`

### 2. 纯anno: z01_anno_spring

#### IoC相关改造

1. 创建配置类`@Configuration`，代替`applicationContext.xml`。
2. IoC的启动方式有改动：`AnnotationConfigApplicationContext` 代替 `ClassPathXmlApplicationContext`。
3. web环境下，配置文件`web.xml` 启动方式改为通过配置类。

除之前的anno，有其他新增anno：

1. `@Configuration` 表名当前类是一个配置类
2. `@ComponentScan` 代替`<context:component-scan>` ，扫哪些包
3. `@Bean` 代替`<bean>`
4. `@Value` 代替`<property>`
5. `@ImportResource` 代替`<import>`， 用于整合 XML 配置文件中的 Bean 定义。
6. `@Import` 代替`<import>`， 引入其他配置类
7. `@PropertySource` 代替`<context:property-placeholder>`， 用于加载属性文件。

#### AOP改造

1. `@EnableAspectJAutoProxy` 代替`<aop:aspectj-autoproxy>`， 开启AOP

#### 事务改造
1. 配置类中进行：`DataSource、JdbcTemplate、PlatformTransactionManager`等配置
2. `@EnableTransactionManagement` 代替`<tx:annotation-driven>`，开启注解事务




