## 1 Spring MVC 概述

### 1 请求处理流程

![](../img/springmvc请求链路.png)

1. 用户发起HTTP请求。
2. DispatcherServlet作为前端控制器接收请求。
3. DispatcherServlet通过HandlerMapping查询合适的处理器（Handler）。
4. HandlerMapping返回一个处理器执行链（HandlerExecutionChain），包括多个拦截器（Interceptor）和处理器。
5. 拦截器按照特定顺序执行，处理请求前后的工作。
6. 当所有拦截器执行完毕后，处理器（Handler）开始处理请求。
7. 处理器执行完成后，返回ModelAndView对象。
8. DispatcherServlet使用ViewResolver解析视图。
9. ViewResolver返回一个视图对象。
10. 视图对象渲染视图内容。
11. DispatcherServlet响应用户。

Spring MVC的核心组件相互协作，形成了一个强大的Web应用程序框架。以下是各组件之间的详细关系和作用：

1. **DispatcherServlet**：
    - 接收用户的HTTP请求。
    - 查询`HandlerMapping`以找到合适的处理器（Handler）。
    - 使用`HandlerAdapter`调用处理器。
    - 根据处理器返回的结果，选择合适的`ViewResolver`解析视图。
    - 将解析后的视图呈现给用户。

2. **HandlerMapping**：
    - 根据请求路径匹配处理器（Handler）。
    - 可能返回一个处理器执行链（`HandlerExecutionChain`），其中包含了多个拦截器（Interceptor）和处理器。

3. **HandlerExecutionChain**：
    - 处理器执行链中的拦截器按顺序执行，处理请求前后的操作。
    - 当所有拦截器执行完毕后，处理器（Handler）执行请求处理逻辑。

4. **HandlerAdapter**：
    - 适配各种类型的处理器，确保它们都能被`DispatcherServlet`正确地调用。
    - 处理器执行完成后，返回`ModelAndView`对象。

5. **HandlerExceptionResolver**：
    - 处理处理器（Handler）执行过程中可能出现的异常。
    - 根据异常情况设置`ModelAndView`，以便后续的错误处理和视图渲染。

6. **ViewResolver**：
    - 根据`ModelAndView`中的逻辑视图名，解析为实际的视图对象。
    - 支持多种视图技术，例如JSP、FreeMarker等。

7. **RequestToViewNameTranslator**：
    - 从请求中提取视图名，当处理器未指定视图名时使用。

8. **LocaleResolver**：
    - 解析请求中的地区信息，支持国际化的视图渲染。

9. **ThemeResolver**：
    - 解析请求中的主题信息，支持多主题切换。

10. **MultipartResolver**：
    - 处理文件上传请求，将普通请求转化为`MultipartHttpServletRequest`，方便获取上传文件。

11. **FlashMapManager**：
    - 在重定向过程中传递临时数据，避免数据丢失。

这些组件共同构成了Spring MVC的请求处理流程，使得开发者能够专注于业务逻辑，而无需关心底层细节。
通过灵活的配置和扩展，开发者可以根据需求定制自己的Web应用程序。

### 3 ModelAndView对象

MVC模型中的，M为模型（Model），V为视图(View)。ModelAndView 是一种变量类型，这类类型包含了 **模型和视图**的数据. 返回ModelAndView
时，即把模型数据和视图数据一起返回。<br/>
一般把ModelAndView 数据返回给浏览器，若浏览器根据模型数据和视图数据来渲染网页。等于ModelAndView把Model 和 View 的数据打包在一起。

#### 1 指定返回页面

1. 构造方法可以指定返回的页面名称
   `new ModelAndView("redirect:/m07.jsp");`
2. 通过setViewName()方法跳转到指定的页面
    - 跳转新的视图名,默认forward方法, 以下等价于 forward:/m07.jsp: `mav.setViewName("/m07.jsp");`
    - redirect方式需要显式配置。 : `mav.setViewName("redirect:/m07.jsp")`
3. 可以结合视图解析器，省略视图名的前缀和后缀。
   `modelAndView.setViewName("success");` 此处等价于 `forward:/WEB-INF/jsp/success.jsp`

```xml
  <!--配置springmvc的视图解析器-->
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
    <property name="prefix" value="/WEB-INF/jsp/"/>
    <property name="suffix" value=".jsp"/>
</bean>
```

#### 2 传递数据

1. addObject()设置需要返回的值，addObjects() 传入Map对象，可传递多个数据
2. 可以使用带参数的构造方法，在构造方法中设置需要返回的值
3. 三个等价对象(ModelMap、Model、Map)，与modelAndView.addObject() 效果相同，都是添加到request作用域。
   [传递数据](../z01_source_springmvc/src/main/java/zen/springmvc/controller/ModelViewController.java)

