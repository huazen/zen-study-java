## 1 基本使用

## 1 纯springmvc搭建基本流程

1. 创建web项目

> 添加`spring-webmvc`、`javax.servlet-api`依赖

2. 配置web.xml。[具体配置](../z01_source_springmvc/src/main/webapp/WEB-INF/web.xml)

> 核心配置前端控制器`DispatcherServlet`

3. 配置springmvc的配置文件

> 配置注解扫描、视图解析器, web.xml中扫描`springmvc.xml`

4. 编写controller类

> 创建controller类，添加`@Controller`注解

5. 编写前端页面

> 在webapp/WEB-INF目录下，创建jsp文件夹，创建success.jsp文件

6. 测试

> 启动tomcat(tomcat7-maven-pluginq启动)，访问controller接口`http://localhost:8080/demo`

## 2 相关配置

### 1 配置视图解析器

#### springmvc配置

使用示例：[demo方法](../z01_source_springmvc/src/main/java/zen/springmvc/controller/DemoController.java)

```xml
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
    <property name="prefix" value="/WEB-INF/jsp/" />
    <property name="suffix" value=".jsp"/>
</bean>
```

#### springboot配置

```properties
# 指定视图解析器的前缀和后缀
spring.mvc.view.prefix=/WEB-INF/jsp/
spring.mvc.view.suffix=.jsp
```

### 2 配置资源访问

#### springmvc配置

web.xml中配置`url-pattern`标签，建议配置成`/`。
配置为/ 不会拦截 .jsp，但是会拦截.html等静态资源，具体介绍看配置文件。
> 静态资源：除了servlet和jsp之外的js、css、png等

#### springboot配置

在 Spring Boot 中，默认情况下，Spring MVC 已经被配置好，并且可以通过 @Controller 注解的类来处理 HTTP 请求。
Spring Boot 默认不会拦截 .jsp, .html, .js, .css, .png 等静态资源文件，除非你配置它这样做。
> 实现 `org.springframework.web.servlet.config.annotation.WebMvcConfigurer` 接口，重写 `configurePathMatch` 方法进行路径配置策略。

### 3 静态资源访问

#### springmvc配置

方案一：

```xml
 <mvc:default-servlet-handler/>
```

原理：
> 添加该标签配置之后，会在SpringMVC上下文中定义一个DefaultServletHttpRequestHandler对象。
> 对进入DispatcherServlet的url请求进行过滤筛查，
> 如果发现是一个静态资源请求，那么会把请求转由web应用服务器（tomcat）默认的DefaultServlet来处理，
> 如果不是静态资源请求，那么继续由SpringMVC框架处理。

缺点：
> 静态资源只能放在webapp根目录下，不能放在如webapp/WEB-INF下，如resources目录下。

方案二:

```xml
<!--静态资源配置，方案二，SpringMVC框架自己处理静态资源
    mapping:约定的静态资源的url规则，如/js开头的uri。
    location：指定的静态资源的存放位置
-->
<mvc:resources location="classpath:/"  mapping="/resources/**"/>
<mvc:resources location="/WEB-INF/js/" mapping="/js/**"/>
```

#### springboot配置

方案一：

```properties
# 配置静态资源 URL 模式
spring.mvc.static-path-pattern=/resources/**

# 配置静态资源实际位置
spring.resources.static-locations=classpath:/static/,classpath:/resources/,file:/var/www/static/
```

方案二：WebMvcConfigurer#addResourceHandlers方法重写
