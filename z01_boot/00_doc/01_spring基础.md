## 1 Spring Bean相关

### 1 Spring Bean配置常用属性

1. id属性： ⽤于给bean提供⼀个唯⼀标识。在⼀个标签内部，标识必须唯⼀。
2. class属性： ⽤于指定创建Bean对象的全限定类名。
3. name属性： ⽤于给bean提供⼀个或多个名称。多个名称⽤空格分隔。
4. factory-bean属性：
5. ⽤于指定创建当前bean对象的⼯⼚bean的唯⼀标识。当指定了此属性之后，class属性失效。
6. factory-method属性： ⽤于指定创建当前bean对象的⼯⼚⽅法，如配合factory-bean属性使⽤，则class属性失效。如配合class属性使⽤，则⽅法必须是static的。
7. scope属性： ⽤于指定bean对象的作⽤范围。通常情况下就是singleton。当要⽤到多例模式时，可以配置为prototype。
8. init-method属性： ⽤于指定bean对象的初始化⽅法，此⽅法会在bean对象装配后调⽤。必须是⼀个⽆参⽅法。
9. destory-method属性： ⽤于指定bean对象的销毁⽅法，此⽅法会在bean对象销毁前执⾏。它只能为scope是singleton时起作⽤。

### 2 Spring Bean 的作用域

在 Spring 框架中，Bean 的作用域（scope）决定了 Bean 实例的生命周期和可见性。
Spring 支持多种作用域，可以根据应用程序的需求来选择合适的作用域。
以下是 Spring 支持的一些常见作用域：

1. **singleton**：
   - 默认情况下，Spring 中的 Bean 都是单例模式（Singleton）的。
   - 即使多次请求同一个 Bean，Spring 容器也只会创建一次该 Bean 的实例，并且在容器中始终只有一个实例存在。
   - 单例模式的 Bean 在整个应用程序生命周期中只创建一次。

2. **prototype**：
   - 每次请求该 Bean 时，Spring 容器都会创建一个新的实例。
   - Prototype 作用域适用于那些需要每次请求都创建新实例的情况。
   - 由于每次请求都会创建一个新的实例，所以 Prototype 作用域的 Bean 在容器中没有固定的生命周期。

3. **request**：
   - 在 Web 应用程序中，该作用域表示 Bean 的实例会在一个 HTTP 请求中存在。
   - 每个请求都会创建一个新的实例，且该实例仅在本次请求的生命周期内有效。
   - 适合于那些需要在整个请求过程中共享数据的场景。

4. **session**：
   - 在 Web 应用程序中，该作用域表示 Bean 的实例在一个 HTTP 会话中存在。
   - 每个会话都会创建一个新的实例，且该实例仅在本次会话的生命周期内有效。
   - 适合于那些需要在整个会话过程中共享数据的场景。

5. **global-session**：
   - 该作用域类似于 session 作用域，但它支持在集群环境中的全局会话。
   - 这意味着当用户从一个服务器迁移到另一个服务器时，全局会话中的 Bean 实例仍然可用。
   - 该作用域主要用于支持跨服务器的会话共享。

6. **application**：
   - 在 Web 应用程序中，该作用域表示 Bean 的实例在整个应用程序上下文（Web 应用程序）中存在。
   - 该作用域类似于 singleton，但在 Web 应用程序中更具有意义，因为它在整个应用程序范围内共享一个实例。
   - 注意：这不是 Spring 内置的作用域，而是由某些框架或容器提供的扩展作用域。

除了上述作用域外，Spring 还支持自定义作用域。你可以通过实现 `org.springframework.context.ApplicationContext`
接口中的 `registerScope` 方法来注册自定义的作用域。

在配置文件中，你可以通过 `<bean>` 标签的 `scope` 属性来指定 Bean 的作用域。例如：

```xml

<bean id="exampleBean" class="com.example.ExampleBean" scope="prototype"/>
```

或者在 Java 配置类中使用 `@Scope` 注解来指定作用域：

```java
@Bean
@Scope("prototype")
public ExampleBean exampleBean(){
        return new ExampleBean();
        }
```

通过这些作用域的配置，你可以根据实际的应用场景选择最适合的 Bean 生命周期管理方式。

### 2 前置后置处理器

#### 初始化执行顺序：

0. BeanFactoryPostProcessor接口方法执行：一般用于修改BeanDeﬁnition，此时bean还没有实例化。
1. 执行xxxAware接口方法
   1. `BeanNameAware`接口执行，获取注册bean时定义的id。
   2. `BeanFactoryAware`接口执行,获取beanfactory。
   3. `ApplicationContextAware`接口执行，获取applicationcontext。
2. 执行`BeanPostProcessor`#postProcessBeforeInitialization：初始化之前执行
3. 执行初始化方法
   1. 执行`@PostConstruct`注解方法
   2. `InitializingBean`接口方法执行
4. 执行`BeanPostProcessor`#postProcessAfterInitialization：初始化之后执行
5. `SmartInitializingSingleton`接口执行：所有单例 Bean 被实例化之后调用
6. `ApplicationListener<ContextRefreshedEvent>` 实现执行：容器启动事件

#### 销毁执行，关闭容器：

1. `@PreDestroy`注解方法
2. `DisposableBean`接口方法执行

## 2 Aop相关

AspectJ是⼀个基于Java语⾔的AOP框架，Spring框架从2.0版本之后集成了AspectJ框架中切⼊点表达式的部分，开始⽀持AspectJ切⼊点表达式。

### 1 切入点表达式

`全限定⽅法名	访问修饰符 返回值	包名.包名.包名.类名.⽅法名(参数列表)`

1. 全匹配⽅式：
   `public void zen.spring.service.impl.TransferServiceImpl.update(zen.spring.pojo.Account)`
2. 访问修饰符可以省略
   `void zen.spring.service.impl.TransferServiceImpl.update(zen.spring.pojo.Account)`
3. 返回值可以使⽤*，表示任意返回值，包括void
   `* zen.spring.service.impl.TransferServiceImpl.update(zen.spring.pojo.Account)`
4. 包名可以使⽤*表示任意包，但是有⼏级包，必须写⼏个
   `* *.*.*.*.TransferServiceImpl.updateAccountByCardNo(zen.spring.pojo.Account)`
5. 包名可以使⽤`*..`表示当前包及其⼦包
   `* *..TransferServiceImpl.updateAccountByCardNo(zen.spring.pojo.Account)`
6. 类名和⽅法名，都可以使⽤*表示任意类，任意⽅法
   `* *..*.*(com.lagou.pojo.Account)`
7. 参数列表，可以使⽤具体类型
   1. 基本类型直接写类型名称 ： `int`
   2. 引⽤类型必须写全限定类名：`java.lang.String`
   3. 参数列表可以使⽤*，表示任意参数类型，但是必须有参数 `* *..*.*(*)`
   4. 参数列表可以使⽤..，表示有⽆参数均可, 有参数可以是任意类型 `* *..*.*(..)`
8. 全通配⽅式：`* *..*.*(..)`
9. 实际开发常用 : `zen.hua.service.impl.*.*(..)`

### 2 指定代理方式

Spring在选择创建代理对象时，会根据实际情况来选择的。被代理对象实现了接⼝，则采⽤基于接⼝的动态代理。当被代理对象没有实现任何接⼝的时候，Spring会⾃动切换到基于⼦类的动态代理⽅式。

但只要不是ﬁnal修饰的类都可以采⽤cglib提供的⽅式创建代理对象，即基于子类的动态代理。
配置文件方式：

```xml
<aop:config proxy-target-class="true">

   <!--
      此标签是基于XML和注解组合配置AOP时的必备标签，表示Spring开启注解配置AOP的⽀持。
      proxy-target-class作用同上
   -->
   <aop:aspectj-autoproxy proxy-target-class="true"/>
```

配置类方式：`@EnableAspectJAutoProxy(proxyTargetClass = true)`

## 3 Spring事务相关




