package zen.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * @author: ZEN
 * @create: 2024-09-28 13:53
 **/
@Controller()
public class DemoController {


    @RequestMapping("/demo")
    public ModelAndView demo() {
        Date date = new Date();// 服务器时间
        // 返回服务器时间到前端页面
        // 封装了数据和页面信息的 ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // addObject 其实是向请求域中request.setAttribute("date",date);
        modelAndView.addObject("date", date);
        // 视图信息(封装跳转的页面信息) 逻辑视图名
        // 未配置视图解析器前缀和后缀
//        modelAndView.setViewName("forward:/WEB-INF/jsp/success.jsp");
        modelAndView.setViewName("success");
        return modelAndView;
    }
}
