package zen.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import zen.springmvc.pojo.Product;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author: ZEN
 * @create: 2024-09-28 14:16
 **/
@Controller
@RequestMapping("/modelview")
public class ModelViewController {

    /**
     * 直接声明形参ModelMap，封装数据
     * url: http://localhost:8080/demo/handle11
     * <p>
     * =================modelmap:class
     * org.springframework.validation.support.BindingAwareModelMap
     */
    @RequestMapping("/handle11")
    public String handle11(ModelMap modelMap) {
        Date date = new Date();// 服务器时间
        modelMap.addAttribute("date", date);
        System.out.println("=================modelmap:" + modelMap.getClass());
        // 若未配置视图解析器
        // return "forward:/WEB-INF/jsp/success.jsp";
        return "success";
    }


    /**
     * 直接声明形参Model，封装数据
     * url: http://localhost:8080/demo/handle12
     * =================model:class
     * org.springframework.validation.support.BindingAwareModelMap
     */
    @RequestMapping("/handle12")
    public String handle12(Model model) {
        Date date = new Date();
        model.addAttribute("date", date);
        System.out.println("=================model:" + model.getClass());
        return "success";
        // 若未配置视图解析器
        // return "forward:/WEB-INF/jsp/success.jsp";
    }


    /**
     * 直接声明形参Map集合，封装数据
     * url: http://localhost:8080/demo/handle13
     * =================map:class
     * org.springframework.validation.support.BindingAwareModelMap
     */
    @RequestMapping("/handle13")
    public String handle13(Map<String, Object> map) {
        Date date = new Date();
        map.put("date", date);
        System.out.println("=================map:" + map.getClass());
        return "success";
        // 若未配置视图解析器
        // return "forward:/WEB-INF/jsp/success.jsp";
    }

    @RequestMapping("selectAll")
    public String selectAll(HttpServletRequest request, Model model,
                            ModelMap modelMap, Map map) {
        List<Product> list = new ArrayList<>();
        list.add(new Product("aaa", 1000.0));
        list.add(new Product("bbb", 2000.0));
        list.add(new Product("ccc", 3000.0));

        /*request.setAttribute("list",list);*/
        //向model、modelMap、map对象中存储数据相当于存储在request作用域
        /*model.addAttribute("list",list);*/
        /* modelMap.addAttribute("list",list);*/
        map.put("list", list);
        return "forward:/show.jsp";
    }
}
