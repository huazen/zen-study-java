package zen.springmvc.config;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 自定义类型转换器
 * S：source，源类型
 * T：target：目标类型
 */
public class DateStrConverter implements Converter<Date, String> {
    @Override
    public String convert(Date source) {
        // 完成字符串向日期的转换
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            return simpleDateFormat.format(source);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
