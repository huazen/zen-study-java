package zen.springmvc.pojo;

/**
 * @author: ZEN
 * @create: 2024-09-28 14:18
 **/

public class Product {

    private String name;

    private double price;

    public Product() {
    }

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}
