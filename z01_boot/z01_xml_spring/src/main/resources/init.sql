create
database bank character set utf8mb4;
use
bank;
CREATE TABLE account
(
    id     INT AUTO_INCREMENT PRIMARY KEY,
    cardNo VARCHAR(255) NOT NULL UNIQUE,
    name   VARCHAR(255) NOT NULL,
    money  INT          NOT NULL
);

INSERT INTO account (name, money, cardNo)
VALUES ('韩梅梅', 1000, '6029621011001');
INSERT INTO account (name, money, cardNo)
VALUES ('李大雷', 1000, '6029621011000');
