package zen.spring.service.impl;

import zen.spring.aop.A;
import zen.spring.service.AopTestService;

/**
 * @author: ZEN
 * @create: 2024-09-21 16:30
 **/
public class AopTestServiceImpl implements AopTestService {


    @A
    @Override
    public void testAop(String name) {
        System.out.println("AopTestServiceImpl: " + name);
    }
}
