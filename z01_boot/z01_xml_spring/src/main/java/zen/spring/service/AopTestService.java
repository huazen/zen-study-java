package zen.spring.service;

/**
 * @author: ZEN
 * @create: 2024-09-21 16:29
 **/
public interface AopTestService {
    void testAop(String name);
}
