package zen.spring.service.impl;

import zen.spring.dao.AccountDao;
import zen.spring.pojo.Account;
import zen.spring.service.TransferService;

/**
 * @author 应癫
 */
public class TransferServiceImpl implements TransferService {

    //private AccountDao accountDao = new JdbcAccountDaoImpl();

    // private AccountDao accountDao = (AccountDao) BeanFactory.getBean("accountDao");

    // 最佳状态
    private AccountDao accountDao;

    // 构造函数传值/set方法传值

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }


    @Override
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {

        // 基于动态代理方式实现事务
//        try{
//            // 开启事务(关闭事务的自动提交)
//            transactionManager.beginTransaction();

        Account from = accountDao.queryAccountByCardNo(fromCardNo);
        Account to = accountDao.queryAccountByCardNo(toCardNo);

        from.setMoney(from.getMoney() - money);
        to.setMoney(to.getMoney() + money);

        accountDao.updateAccountByCardNo(to);
        int c = 1 / 0;
        accountDao.updateAccountByCardNo(from);

//           // 提交事务
//            transactionManager.commit();
//        }catch (Exception e) {
//            e.printStackTrace();
//            // 回滚事务
//            transactionManager.rollback();
//
//            // 抛出异常便于上层servlet捕获
//            throw e;
//
//        }


    }


}
