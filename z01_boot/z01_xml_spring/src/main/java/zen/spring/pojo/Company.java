package zen.spring.pojo;

import lombok.Data;

@Data
public class Company {
    private String name;
    private String address;
    private int scale;
}