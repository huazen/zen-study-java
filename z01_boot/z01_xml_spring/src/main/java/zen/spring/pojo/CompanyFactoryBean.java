package zen.spring.pojo;

import org.springframework.beans.factory.FactoryBean;

/**
 * @author 应 癫
 * <pre>
 *     ⾃定义Bean的创建过程（完成复杂Bean的定义）
 * </pre>
 */
public class CompanyFactoryBean implements FactoryBean<Company> {

    private String companyInfo; // 公司名称,地址,规模

    public void setCompanyInfo(String companyInfo) {
        this.companyInfo = companyInfo;
    }

    /**
     * 返回FactoryBean创建的Bean实例，如果isSingleton返回true，
     * 则该实例会放到Spring容器的单例对象缓存池中Map
     */
    @Override
    public Company getObject() throws Exception {

        // 模拟创建复杂对象Company
        Company company = new Company();
        String[] strings = companyInfo.split(",");
        company.setName(strings[0]);
        company.setAddress(strings[1]);
        company.setScale(Integer.parseInt(strings[2]));
        return company;
    }

    /**
     * 返回FactoryBean创建的Bean类型
     *
     * @return
     */
    @Override
    public Class<?> getObjectType() {
        return Company.class;
    }

    /**
     * 返回作⽤域是否单例
     *
     * @return
     */
    @Override
    public boolean isSingleton() {
        return true;
    }
}