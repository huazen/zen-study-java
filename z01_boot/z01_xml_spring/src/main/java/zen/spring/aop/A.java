package zen.spring.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) // 注解的保留策略为运行时，这样可以通过反射访问
@Target(ElementType.METHOD) // 注解的目标为方法
public @interface A {
}
