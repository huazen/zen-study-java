package zen.spring.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * 方式2: 实现MethodInterceptor接口，简化了声明切面相关的配置
 */
public class MyAdvice implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation mi) throws Throwable {
        System.out.println("MyAdvice-前置通知");     //额外功能 【前置通知】
        //注意：procee方法的返回值就是你目标类方法的返回值
        Object o = mi.proceed();     //放行， 继续执行目标类中的方法
        System.out.println("MyAdvice-后置通知");     //额外功能 【后置通知】
        return o;
    }
}