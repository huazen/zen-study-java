package zen.spring.test;

import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import zen.spring.dao.AccountDao;
import zen.spring.pojo.Account;
import zen.spring.service.AopTestService;
import zen.spring.service.TransferService;

/**
 * @author 应癫
 */
public class IoCTest {


    @Test
    public void testIoC() throws Exception {

        // 方式1: 通过读取classpath下的xml文件来启动容器（xml模式SE应用下推荐）
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        // 方式2: 不推荐使用
        //ApplicationContext applicationContext1 = new FileSystemXmlApplicationContext("文件系统的绝对路径");


        /*================  创建bean的几种方式     ===============*/

        // 第一次getBean该对象
        Object accountPojo = applicationContext.getBean("accountPojo");

        AccountDao accountDao = (AccountDao) applicationContext.getBean("accountDao");
        System.out.println("accountDao：" + accountDao);
        AccountDao accountDao1 = (AccountDao) applicationContext.getBean("accountDao");
        System.out.println("accountDao1：" + accountDao1);

        Account account = accountDao.queryAccountByCardNo("6029621011001");
        System.out.println("查询到的account: " + account);

        Object connectionUtils = applicationContext.getBean("connectionUtils");
        System.out.println(connectionUtils);

        // 获取FactoryBean产⽣的对象
        Object companyBean = applicationContext.getBean("companyBean");
        System.out.println("companyBean:" + companyBean);
        // 在id之前添加“&”，获取的是FactoryBean本身
        Object companyFactoryBean = applicationContext.getBean("&companyBean");
        System.out.println("companyFactoryBean:" + companyFactoryBean);


        // 关闭容器时，执行 destroy-method
        applicationContext.close();
    }

    @Test
    public void testAop() {
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:applicationContext_aop.xml");
        AopTestService aopTestService = applicationContext.getBean(AopTestService.class);
        aopTestService.testAop("6029621011000");
    }

    /**
     * 测试基于Aop的官方事务管理器
     */
    @Test
    @SneakyThrows
    public void testTx() {
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:applicationContext_aop_tx.xml");
        TransferService transferService = applicationContext.getBean("transferService", TransferService.class);
        transferService.transfer("6029621011000", "6029621011001", 100);
    }

}
