package zen.hua.fast_excel.test.cofig;

import cn.idev.excel.context.AnalysisContext;
import cn.idev.excel.read.listener.ReadListener;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import zen.hua.fast_excel.test.model.DemoData;

/**
 * 数据监听器
 * DemoDataListener 是一个自定义监听器，用于处理从 Excel 中读取的数据。
 * 注意：监听器不能被 Spring 管理，每次读取 Excel 文件时需要重新实例化。
 */
@Slf4j
public class DemoDataListener implements ReadListener<DemoData> {

    @Override
    public void invoke(DemoData data, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(data));
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        log.info("所有数据解析完成！");
    }
}