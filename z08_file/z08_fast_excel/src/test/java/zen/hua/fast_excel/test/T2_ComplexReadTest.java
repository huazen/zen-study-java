package zen.hua.fast_excel.test;

import cn.idev.excel.ExcelReader;
import cn.idev.excel.FastExcel;
import cn.idev.excel.context.AnalysisContext;
import cn.idev.excel.read.listener.ReadListener;
import cn.idev.excel.read.metadata.ReadSheet;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import zen.hua.fast_excel.test.cofig.ConvertDataListener;
import zen.hua.fast_excel.test.cofig.CustomStringStringConverter;
import zen.hua.fast_excel.test.cofig.DemoDataListener;
import zen.hua.fast_excel.test.model.ConverterData;
import zen.hua.fast_excel.test.model.DemoData;
import zen.hua.fast_excel.test.model.IndexOrNameData;

/**
 * @author: ZEN
 * @create: 2025-03-02 11:22
 **/
@Slf4j
public class T2_ComplexReadTest {

    static String fileName = "/Users/hua/Downloads/HuaAll/HuaCode/zen-study-java/z08_file/z08_fast_excel/src/test/resources/";


    /**
     * 您可以通过指定列名或列下标来读取 Excel 数据。
     * 这使得与动态生成的 Excel 文件交互更加灵活。
     */
    @Test
    public void testRepeatedRead() {

        // 指定列名或列下标读取
        FastExcel.read(fileName + "name-demo.xlsx", IndexOrNameData.class, new ReadListener<IndexOrNameData>() {
            @Override
            public void invoke(IndexOrNameData o, AnalysisContext analysisContext) {
                log.info("读取到一条数据: {}", JSONObject.toJSONString(o));
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {

            }
        }).sheet().doRead();
    }

    /**
     * 可以读取 Excel 文件中的多个 Sheet，且同一个 Sheet 不可重复读取。
     */
    @Test
    public void testReadSheets() {

        // 读取全部 Sheet
        FastExcel.read(fileName + "demo.xlsx", DemoData.class, new DemoDataListener()).doReadAll();

        // 读取指定 Sheet
        try (ExcelReader excelReader = FastExcel.read(fileName + "demo.xlsx").build()) {
            ReadSheet sheet1 = FastExcel.readSheet(0).head(DemoData.class).registerReadListener(new DemoDataListener()).build();
            ReadSheet sheet2 = FastExcel.readSheet(1).head(DemoData.class).registerReadListener(new DemoDataListener()).build();
            excelReader.read(sheet1, sheet2);
        }
    }


    /**
     * 自定义格式读取
     */
    @Test
    public void converterRead() {

        // 自定义格式读取
        FastExcel.read(fileName + "convert-demo.xlsx", ConverterData.class, new ConvertDataListener())
                .registerConverter(new CustomStringStringConverter())
                .sheet().doRead();
    }


    /**
     * 暂未理解，后续研究
     * 通过设置 headRowNumber 参数或根据实体类的表头注解自动解析多行表头。
     */
    @Test
    public void complexHeaderRead() {
        String fileName = "path/to/demo.xlsx";

        FastExcel.read(fileName, DemoData.class, new DemoDataListener())
                .sheet()
                // 设置多行表头的行数，默认为 1
                .headRowNumber(2)
                .doRead();
    }


    /**
     * 读取表头数据
     */
    @Test
    public void headerRead() {
        FastExcel.read(fileName + "convert-demo.xlsx", ConverterData.class, new ConvertDataListener())
                .registerConverter(new CustomStringStringConverter())
                .sheet().doRead();

    }


}
