package zen.hua.fast_excel.test;

import cn.idev.excel.ExcelReader;
import cn.idev.excel.FastExcel;
import cn.idev.excel.context.AnalysisContext;
import cn.idev.excel.read.listener.PageReadListener;
import cn.idev.excel.read.listener.ReadListener;
import cn.idev.excel.read.metadata.ReadSheet;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import zen.hua.fast_excel.test.cofig.DemoDataListener;
import zen.hua.fast_excel.test.cofig.NoModelDataListener;
import zen.hua.fast_excel.test.model.DemoData;

import java.util.List;
import java.util.Map;

/**
 * @author: ZEN
 * @create: 2025-03-02 10:35
 **/
@Slf4j
public class T1_SimpleReadTest {

    static String fileName = "/Users/hua/Downloads/HuaAll/HuaCode/zen-study-java/z08_file/z08_fast_excel/src/test/resources/demo.xlsx";


    @Test
    public void simpleRead() {

        // 使用方式1：Lambda表达式直接处理数据
        FastExcel.read(fileName, DemoData.class, new PageReadListener<DemoData>(dataList -> {
            for (DemoData demoData : dataList) {
                log.info("读取到一条数据: {}", JSON.toJSONString(demoData));
            }
        })).sheet().doRead();

        // 使用方式2：匿名内部类
        FastExcel.read(fileName, DemoData.class, new ReadListener<DemoData>() {
            @Override
            public void invoke(DemoData data, AnalysisContext context) {
                log.info("读取到一条数据: {}", JSON.toJSONString(data));
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext context) {
            }
        }).sheet().doRead();

        // 使用方式3：自定义监听器
        FastExcel.read(fileName, DemoData.class, new DemoDataListener()).sheet().doRead();

        // 使用方式4：多 Sheet 读取
        try (ExcelReader excelReader = FastExcel.read(fileName, DemoData.class, new DemoDataListener()).build()) {
            ReadSheet readSheet = FastExcel.readSheet(0).build();
            excelReader.read(readSheet);
        }

    }

    /**
     * 自定义没有定义Model的监听器, 不传定义的Model类
     */
    @Test
    public void readNoModelTest() {
        FastExcel.read(fileName, new NoModelDataListener()).sheet().doRead();
    }


    /**
     * 使用 doReadSync 方法直接将 Excel 数据读取为内存中的列表，这种方法适用于数据量较小的场景。
     * 读取的数据可以是 POJO 对象列表或 Map 列表。
     */
    @Test
    public void synchronousReadToObjectList() {

        // case1:使用 FastExcel 同步读取 Excel 数据为对象列表
        List<DemoData> list = FastExcel.read(fileName).head(DemoData.class).sheet().doReadSync();

        // 处理读取的数据列表
        for (DemoData data : list) {
            log.info("读取到的数据: {}", JSON.toJSONString(data));
        }


        // case2: 直接读取为 Map 列表
        List<Map<Integer, String>> mapList = FastExcel.read(fileName).sheet().doReadSync();

        // 处理读取的数据列表
        for (Map<Integer, String> data : mapList) {
            log.info("读取到的数据: {}", JSON.toJSONString(data));
        }
    }

}
