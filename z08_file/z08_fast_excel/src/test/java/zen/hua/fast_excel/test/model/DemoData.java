package zen.hua.fast_excel.test.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: ZEN
 * @create: 2025-03-02 10:31
 **/
@Getter
@Setter
@EqualsAndHashCode
public class DemoData {
    private Long id;

    private String name;

    private Integer age;

    private String email;
}