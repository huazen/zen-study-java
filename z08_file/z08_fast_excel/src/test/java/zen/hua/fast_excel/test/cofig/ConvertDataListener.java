
package zen.hua.fast_excel.test.cofig;

import cn.idev.excel.context.AnalysisContext;
import cn.idev.excel.metadata.data.ReadCellData;
import cn.idev.excel.read.listener.ReadListener;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import zen.hua.fast_excel.test.model.ConverterData;

import java.util.Map;

/**
 * 数据监听器
 * DemoDataListener 是一个自定义监听器，用于处理从 Excel 中读取的数据。
 * 注意：监听器不能被 Spring 管理，每次读取 Excel 文件时需要重新实例化。
 */
@Slf4j
public class ConvertDataListener implements ReadListener<ConverterData> {

    /**
     * 用于读取表头，可选
     */
    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
//        log.info("解析到表头数据: {}", JSON.toJSONString(headMap));
    }

    @Override
    public void invoke(ConverterData data, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(data));
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        log.info("所有数据解析完成！");
    }
}