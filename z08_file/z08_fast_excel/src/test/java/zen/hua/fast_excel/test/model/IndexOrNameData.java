package zen.hua.fast_excel.test.model;

import cn.idev.excel.annotation.ExcelIgnore;
import cn.idev.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author: ZEN
 * @create: 2025-03-02 10:31
 **/
@Getter
@Setter
@EqualsAndHashCode
public class IndexOrNameData {
    /**
     * 索引从0开始
     */
    @ExcelProperty(index = 0)
    private Long id;
    @ExcelProperty("姓名")
    private String name;
    @ExcelProperty("出生日期")
    private Date birth;
    @ExcelIgnore
    private String email;
}