package zen.hua.fast_excel.test.model;

import cn.idev.excel.annotation.ExcelProperty;
import cn.idev.excel.annotation.format.DateTimeFormat;
import cn.idev.excel.annotation.format.NumberFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import zen.hua.fast_excel.test.cofig.CustomStringStringConverter;

@Getter
@Setter
@EqualsAndHashCode
public class ConverterData {
    /**
     * 自定义转换器
     */
    @ExcelProperty(converter = CustomStringStringConverter.class, index = 1)
    private String name;

    @DateTimeFormat("yyyy年MM月dd日HH时mm分ss秒")
    @ExcelProperty(index = 2)
    private String birth;

    /**
     * 转换为百分比
     */
    @NumberFormat("#.##%")
    @ExcelProperty(index = 3)
    private String doubleData;
}