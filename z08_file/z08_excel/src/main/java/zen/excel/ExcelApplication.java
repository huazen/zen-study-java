package zen.excel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: ZEN
 * @create: 2025-03-02 00:41
 **/
@SpringBootApplication
public class ExcelApplication {


    public static void main(String[] args) {
        SpringApplication.run(ExcelApplication.class, args);
    }
}
