package zen.excel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

// 在原有的ExcelController中添加页面跳转方法
@Controller
public class PageController {

    @GetMapping("/")
    public String index() {
        return "excel-demo"; // 对应templates/excel-demo.html
    }
}