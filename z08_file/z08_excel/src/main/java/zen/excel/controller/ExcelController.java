package zen.excel.controller;

import com.alibaba.excel.EasyExcel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import zen.excel.config.UserDataListener;
import zen.excel.model.UserVO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/excel")
@CrossOrigin(origins = "*") // 生产环境应配置具体域名
public class ExcelController {

    @PostMapping("/upload")
    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) {
        try {
            // 使用监听器模式读取Excel
            EasyExcel.read(file.getInputStream(), UserVO.class, new UserDataListener())
                    .sheet()
                    .doRead();
            return ResponseEntity.ok("导入成功");
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("文件上传失败: " + e.getMessage());
        }
    }

    @GetMapping("/download")
    public void download(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=users.xlsx");

//        response.setContentType("application/vnd.ms-excel");
//        response.setCharacterEncoding("UTF-8");
//        response.setHeader("Content-Disposition", "attachment; filename=users.xlsx");


        // 模拟数据库查询数据
        List<UserVO> userList = Arrays.asList(
                new UserVO(1L, "张三", 25, "zhangsan@example.com"),
                new UserVO(2L, "李四", 30, "lisi@example.com")
        );

        // 旧版写法 2.x版本
        EasyExcel.write(response.getOutputStream(), UserVO.class)
                .sheet("用户列表")
                .doWrite(userList);

        // 新版推荐写法 3.x版本
//        try (ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream(), UserVO.class).build()) {
//            WriteSheet sheet = EasyExcel.writerSheet("用户列表").build();
//            excelWriter.write(userList, sheet);
//        }
    }

    @GetMapping("/template")
    public void downloadTemplate(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment; filename=template.xlsx");
        EasyExcel.write(response.getOutputStream())
                .head(UserVO.class)
                .sheet("模板")
                .doWrite(Collections.emptyList());
    }
}