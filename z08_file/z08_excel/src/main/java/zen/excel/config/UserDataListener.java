package zen.excel.config;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson2.JSONObject;
import zen.excel.model.UserVO;

import java.util.ArrayList;
import java.util.List;


/**
 * 数据监听器（核心导入逻辑）
 */
public class UserDataListener extends AnalysisEventListener<UserVO> {
    private static final int BATCH_SIZE = 100;
    private List<UserVO> cachedDataList = new ArrayList<>(BATCH_SIZE);

    @Override
    public void invoke(UserVO user, AnalysisContext context) {
        // 数据校验
        if (user.getName() == null || user.getName().trim().isEmpty()) {
            throw new RuntimeException("姓名不能为空");
        }
        cachedDataList.add(user);

        // 达到批量处理阈值时保存
        if (cachedDataList.size() >= BATCH_SIZE) {
            saveData();
            cachedDataList.clear();
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 处理剩余数据
        if (!cachedDataList.isEmpty()) {
            saveData();
        }
    }

    private void saveData() {
        // 实际应调用Service保存到数据库
        System.out.println("保存批量数据：" + JSONObject.toJSONString(cachedDataList));
    }
}