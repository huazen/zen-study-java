package zen.trace.test.service;

/**
 * @author 散装java
 * @date 2024-06-19
 */
public interface UserService {

    String queryNameById(String id);

    String queryNameById2(String id);
}
