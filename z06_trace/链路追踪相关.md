## 背景

在实际项目中，一个方法在执行的过程中，往往需要打印多个日志。
但在spring项目中，大部分的方法执行都是并发的，致使打印的内容狠杂乱，
导致我们无法快速定位出执行链路中的所有日志来排查问题。

链路追踪就可以很好的解决这个问题，我们只需要找到 唯一的 traceId，
然后通过 traceId 来查询其他日志的

## 链路追踪

### z06_self_trace

MDC（Mapped Diagnostic Context，映射调试上下文）是 slf4j 提供的一种轻量级的日志跟踪工具。<br>
Log4j、Logback或者Log4j2等日志中最常见区分同一个请求的方式是通过线程名，而如果请求量大，
线程名在相近的时间内会有很多重复的而无法分辨，
因此引出了trace-id，即在接收到的时候生成唯一的请求id，在整个执行链路中带上此唯一id.

MDC.java本身不提供传递traceId的能力，真正提供能力的是MDCAdapter接口的实现。

比如Log4j的是Log4jMDCAdapter，Logback的是LogbackMDCAdapter。

但LogbackMDCAdapter是用ThreadLocal 实现的，所以在使用多线程的时候，
还是会出现（父子线程传值问题）。

#### 基本实现

1. TtlMDCAdapter修改原MDCAdapter的实现，采用transmittable-thread-local，保证父子线程值复制。
2. 全限定类名的方式重写org.slf4j.impl.StaticMDCBinder，修改其MDCAdapter为TtlMDCAdapter。
3. 提供MdcUtil，提供traceId的获取和设置方法。
4. 修改日志配置，使用%X{traceId}来打印traceId
5. agent启动transmittable-thread-local，对代码无侵入。

#### http请求使用

1. LogInterceptor实现拦截器HandlerInterceptor，从请求头设置和获取traceId，响应头中塞入traceId。
2. WebConfiguration实现WebMvcConfigurer，配置拦截器，对需要打印日志的接口进行拦截。

#### aop切面使用

1. 以定时任务多线程为例，ScheduledAop对定时注解进行处理，生成或移除？
2. 在切面中获取traceId并设置到MDC中。

### z06_spring_sleuth

spring-sleuth是spring官方提供的链路追踪框架，使用起来非常简单，且高效。

启动Zipkin服务（Docker快速部署）

```shell
docker run -d -p 9411:9411 openzipkin/zipkin
```

查看可视化链路：http://localhost:9411/zipkin/

#### 相关示例

zen.hua.controller.Service1Controller.callService2