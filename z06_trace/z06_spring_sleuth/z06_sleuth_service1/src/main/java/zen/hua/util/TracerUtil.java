package zen.hua.util;

import org.slf4j.MDC;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.SpanNamer;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.cloud.sleuth.instrument.async.TraceCallable;
import org.springframework.cloud.sleuth.instrument.async.TraceRunnable;
import org.springframework.cloud.sleuth.instrument.async.TraceableExecutorService;
import org.springframework.cloud.sleuth.internal.DefaultSpanNamer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

@Component
public class TracerUtil implements ApplicationContextAware {


    private static final ThreadLocal<SpanContext> spanHolder = new ThreadLocal<>();

    // 通过静态变量持有 Tracer 实例（解决静态方法无法直接注入的问题）
    private static Tracer tracer;

    private static ApplicationContext context;


    private static SpanNamer spanNamer = new DefaultSpanNamer();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        tracer = applicationContext.getBean(Tracer.class);
        context = applicationContext;
    }

    /**
     * 获取或创建 Trace ID（同一线程内复用）
     */
    public static String getTraceId() {
        return Optional.ofNullable(tracer.currentSpan())
                .map(span -> span.context().traceId())
                .orElseGet(() -> {
                    // 检查线程缓存中是否存在手动 Span
                    SpanContext context = spanHolder.get();
                    if (context != null && context.span != null) {
                        return context.span.context().traceId();
                    }
                    // 创建新 Span 并绑定到线程
                    Span newSpan = tracer.nextSpan().name("thread-local-trace").start();
                    Tracer.SpanInScope scope = tracer.withSpan(newSpan);
                    MDC.put("traceId", newSpan.context().traceId());
                    spanHolder.set(new SpanContext(newSpan, scope));
                    return newSpan.context().traceId();
                });
    }

    /**
     * 显式结束 Span 并清理资源（必须调用！）
     */
    public static void close() {
        SpanContext context = spanHolder.get();
        if (context != null) {
            context.scope.close();  // 关闭作用域（恢复线程上下文）
            context.span.end();     // 结束 Span
            MDC.remove("traceId");
            spanHolder.remove();
        }
    }

    // 内部类用于绑定 Span 和其作用域
    private static class SpanContext {
        final Span span;
        final Tracer.SpanInScope scope;

        SpanContext(Span span, Tracer.SpanInScope scope) {
            this.span = span;
            this.scope = scope;
        }
    }


    /**
     * 构建 TraceableExecutorService
     */
    public static ExecutorService traceExecutor(ExecutorService executor) {
        return new TraceableExecutorService(context, executor);
    }

    /**
     * 构建 TraceRunnable
     */
    public static TraceRunnable traceRunnable(Runnable runnable) {
        return new TraceRunnable(tracer, spanNamer, runnable);
    }

    /**
     * 构建 TraceRunnable
     */
    public static <V> TraceCallable<V> traceCallable(Callable<V> callable) {
        return new TraceCallable(tracer, spanNamer, callable);
    }
}