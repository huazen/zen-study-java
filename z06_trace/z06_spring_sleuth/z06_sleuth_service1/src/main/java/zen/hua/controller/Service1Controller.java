package zen.hua.controller;

import brave.Span;
import brave.Tracer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author: ZEN
 * @create: 2025-03-02 16:45
 **/

@RestController
public class Service1Controller {

    // 要先bean进行依赖管理
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Tracer tracer;

    @GetMapping("/call")
    public String callService2() {
        // 调用service2服务
        String response = restTemplate.getForObject("http://localhost:8382/hello", String.class);
        return "Service A called Service B and got response: " + response;
    }


    /**
     * 获取当前Span的Trace ID和Span ID
     * <pre>{@code
     *
     *  <pattern>%d{yyyy-MM-dd HH:mm:ss} [%X{X-B3-TraceId:-},%X{X-B3-SpanId:-}] %msg%n</pattern>
     * }</pre>
     *
     * @return
     */
    @GetMapping("/call2")
    public String call2() {
        // 获取当前Span
        Span currentSpan = tracer.currentSpan();
        if (currentSpan != null) {
            // 提取Trace ID和Span ID
            long traceId = currentSpan.context().traceId();
            long spanId = currentSpan.context().spanId();
            return "Trace ID: " + traceId + ", Span ID: " + spanId;
        }
        return "No active span";
    }
}
