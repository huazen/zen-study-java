package zen.hua.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import zen.hua.AppTest;
import zen.hua.util.TracerUtil;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: ZEN
 * @create: 2025-03-02 18:59
 **/
@Slf4j
public class AsyncTest extends AppTest {


    /**
     * TraceableExecutorService 包装为支持 Trace 的线程池
     */
    @Test
    public void testThreadPool() {
        String traceId = TracerUtil.getTraceId();
        log.info("parent_thread traceId: {}", traceId);
        // 创建原始线程池
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.submit(new Runnable() {
            @Override
            public void run() {
                log.info("原生线程池traceId: {}", TracerUtil.getTraceId());
            }
        });
        // 父线程traceId
        log.info("parent_thread_2 traceId: {}", TracerUtil.getTraceId());


        // 包装为支持 Trace 的线程池
        executor = TracerUtil.traceExecutor(executor);

        executor.submit(new Runnable() {
            @Override
            public void run() {
                // 是父线程traceId
                log.info("包装线程池traceId: {}", TracerUtil.getTraceId());
            }
        });

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     *
     */
    @Test
    public void testAsyncTask() {
        log.info("parent_thread traceId: {}", TracerUtil.getTraceId());
        // 原始任务
        Runnable rawTask = () -> {
            log.info("Async task traceId: {}", TracerUtil.getTraceId());
            // 业务逻辑
        };

        // 使用 TraceRunnable 包装
        Runnable tracedTask = TracerUtil.traceRunnable(rawTask);

        // 提交任务（使用默认 ForkJoinPool）
        CompletableFuture.runAsync(tracedTask);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
