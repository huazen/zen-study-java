## MongoDB 数据库操作

### 总结

```
选择切换数据库：use articledb 
插入数据：db.comment.insertOne({bson数据}) 
查询所有数据：db.comment.find(); 
条件查询数据：db.comment.find({条件}) 
查询符合条件的第一条记录：db.comment.findOne({条件}) 
查询符合条件的前几条记录：db.comment.find({条件}).limit(条数) 
查询符合条件的跳过的记录：db.comment.find({条件}).skip(条数) 
修改数据：db.comment.updateOne({条件},{修改后的数据}) 或db.comment.updateOne({条件},{$set:{要修改部分的字段:数据}) 
修改数据并自增某字段值：db.comment.updateOne({条件},{$inc:{自增的字段:步进值}})
删除数据：db.comment.deleteMany({条件}) 
统计查询：db.comment.countDocuments({条件}) 
模糊查询：db.comment.find({字段名:/正则表达式/}) 
条件比较运算：db.comment.find({字段名:{$gt:值}}) 
包含查询：db.comment.find({字段名:{$in:[值1，值2]}})或db.comment.find({字段名:{$nin:[值1，值2]}}) 
条件连接查询：db.comment.find({$and:[{条件1},{条件2}]})或db.comment.find({$or:[{条件1},{条件2}]})
```

### 新增用户密码

```shell
use admin
##创建账号/密码##
db.createUser({ user: 'admin', pwd: 'admin', roles: [ { role: "userAdminAnyDatabase", db: "admin" } ] });
```

### 库操作

```shell
use aticledb

show dbs
show databases

db.dropDatabase()

show dbs
db.createCollection("mycollection")

show collections

db.mycollection.drop()
```

### 文档操作

#### 插入操作

```shell
// 单个插入
db.comment.insertOne({"articleid":"100000",
"content":"今天天气真好，阳光明 媚",
"userid":"1001",
"nickname":"Rose",
"createdatetime":new Date(),
"likenum":NumberInt(10),
"state":null})

// 批量插入
db.comment.insertMany([ {"_id":"1","articleid":"100001","content":"我们不应该把清晨浪费在手机上，健康很重要，一杯温水幸福你我 他。","userid":"1002","nickname":"相忘于江湖","createdatetime":new Date("2019-08-05T22:08:15.522Z"),"likenum":NumberInt(1000),"state":"1"},
{"_id":"2","articleid":"100001","content":"我夏天空腹喝凉开水，冬天喝温开水","userid":"1005","nickname":"伊人憔 悴","createdatetime":new Date("2019-08-05T23:58:51.485Z"),"likenum":NumberInt(888),"state":"1"},
{"_id":"3","articleid":"100001","content":"我一直喝凉开水，冬天夏天都喝。","userid":"1004","nickname":"杰克船 长","createdatetime":new Date("2019-08-06T01:05:06.321Z"),"likenum":NumberInt(666),"state":"1"},
{"_id":"4","articleid":"100001","content":"专家说不能空腹吃饭，影响健康。","userid":"1003","nickname":"凯 撒","createdatetime":new Date("2019-08-06T08:18:35.288Z"),"likenum":NumberInt(2000),"state":"1"},
{"_id":"5","articleid":"100001","content":"研究表明，刚烧开的水千万不能喝，因为烫 嘴。","userid":"1003","nickname":"凯撒","createdatetime":new Date("2019-08-06T11:01:02.521Z"),"likenum":NumberInt(3000),"state":"1"} ]);

/* 提示： 插入时指定了 _id ，则主键就是该值。
如果某条数据插入失败，将会终止插入，但已经插入成功的数据不会回滚掉。 因为批量插入由于数据较多容易出现失败，因此，可以使用try catch进行异常捕捉处理，测试的时候可以不处理 */
try {
db.comment.insertMany([ {"_id":"1","articleid":"100001","content":"我们不应该把清晨浪费在手机上，健康很重要，一杯温水幸福你我他。","userid":"1002","nickname":"相忘于江湖","createdatetime":new Date("2019-08-05T22:08:15.522Z"),"likenum":NumberInt(1000),"state":"1"},
{"_id":"2","articleid":"100001","content":"我夏天空腹喝凉开水，冬天喝温开水","userid":"1005","nickname":"伊人憔 悴","createdatetime":new Date("2019-08-05T23:58:51.485Z"),"likenum":NumberInt(888),"state":"1"},
{"_id":"3","articleid":"100001","content":"我一直喝凉开水，冬天夏天都喝。","userid":"1004","nickname":"杰克船 长","createdatetime":new Date("2019-08-06T01:05:06.321Z"),"likenum":NumberInt(666),"state":"1"},
{"_id":"4","articleid":"100001","content":"专家说不能空腹吃饭，影响健康。","userid":"1003","nickname":"凯 撒","createdatetime":new Date("2019-08-06T08:18:35.288Z"),"likenum":NumberInt(2000),"state":"1"},
{"_id":"5","articleid":"100001","content":"研究表明，刚烧开的水千万不能喝，因为烫 嘴。","userid":"1003","nickname":"凯撒","createdatetime":new Date("2019-08-06T11:01:02.521Z"),"likenum":NumberInt(3000),"state":"1"} ]); }
catch (e) { print (e); }
```

#### 查询操作

##### 基本查询

```shell
// 查询所有集合数据
db.comment.find()
// 每条文档会有一个叫_id的字段，这个相当于我们原来关系数据库中表的主键，当你在插入文档记录时没有指定该字段， MongoDB会自动创建，其类型是ObjectID类型。

// 查询指定条件数据
db.comment.find({"userid":"1003"})
db.comment.findOne({"_id":"3"})

// 投影查询（Projection Query），即只查询某些字段
db.comment.find({"userid":"1003"},{"content":NumberInt(1),"userid":NumberInt(1)})
```

#### 更新操作

```shell
// 更新文档
// 3.2版本之后- 不太可行
db.comment.replaceOne({"_id":"1"},{"likenum":NumberInt(1001)});


// 3.2 版本之前
// 覆盖修改：如果我们想修改_id为1的记录，点赞量为1001，其他字段不见了 -- 不可用
db.comment.update({"_id":"1"},{likenum:NumberInt(1001)});
// 增量修改，$set：如果我们想修改_id为1的记录，点赞量加1, 其他字段不变
db.comment.update({"_id":"2"},{$set:{likenum:NumberInt(889)}});
// 批量修改：更新所有用户为 1003 的用户的昵称为 凯撒大帝 。
// 3.2版本之后
// 默认只修改第一条数据
db.comment.updateOne({userid:"1003"},{$set:{nickname:"凯撒2"}})
// 修改所有符合条件的数据
db.comment.updateMany({userid:"1003"},{$set:{nickname:"凯撒大帝"}})

// 3.2版本之前支持
// 默认只修改第一条数据
db.comment.update({userid:"1003"},{$set:{nickname:"凯撒2"}})
// 修改所有符合条件的数据
db.comment.update({userid:"1003"},{$set:{nickname:"凯撒大帝"}},{multi:true})

// 列值自增
db.comment.updateOne({_id:"3"},{$inc:{likenum:NumberInt(1)}})
```

#### 删除操作

3.2版本之后

```shell
// 删除全部文档
db.comment.deleteMany({})
// 根据条件进行删除
db.comment.deleteOne({"_id":"1"});
```

3.2版本之前

```shell

// 删除全部文档
db.comment.remove({})
db.comment.deleteOne({_id:"1"})
db.comment.deleteMany({userid:"1003"})
```

#### 统计查询

3.2版本之前

```shell
// 统计所有记录数
db.comment.count()
// 统计userid为1003的记录条数
db.comment.count({userid:"1003"})
```

3.2版本之后

```shell
// 统计所有记录数
db.comment.countDocuments()
// 统计userid为1003的记录条数
db.comment.count({userid:"1003"})
```

#### 分页列表查询

```shell
// 查询前两条数据
db.comment.find({}).limit(2)
// 跳过前两条，查询后两条数据
db.comment.find({}).skip(2).limit(2)
```

#### 排序查询

语法：`db.COLLECTION_NAME.find().sort({KEY:1})` 或 `db.集合名称.find().sort(排序方式)`

```shell
// 对userid降序排列，并对访问量进行升序排列
db.comment.find().sort({userid:-1,likenum:1})
```

> skip(), limilt(), sort()三个放在一起执行的时候，执行的顺序是先 sort(), 然后是 skip()，最后是显示的 limit()，和命令编写顺序无关。

#### 正则查询

语法：`db.collection.find({field:/正则表达式/})` 或 `db.集合.find({字段:/正则表达式/})`
> 正则表达式是js的语法，直接量的写法。

```shell
// 我要查询评论内容包含“开水”的所有文档，
db.comment.find({content:/开水/})
// 要查询评论的内容中以“专家”开头的
db.comment.find({content:/^专家/})
```

#### 比较查询

```shell
 db.集合名称.find({ "field" : { $gt: value }}) // 大于: field > value
 db.集合名称.find({ "field" : { $lt: value }}) // 小于: field < value
 db.集合名称.find({ "field" : { $gte: value }}) // 大于等于: field >= value
 db.集合名称.find({ "field" : { $lte: value }}) // 小于等于: field <= value
 db.集合名称.find({ "field" : { $ne: value }}) // 不等于: field != value
 // 查询评论点赞数量大于700的记录
 db.comment.find({likenum:{$gt:NumberInt(700)}})
```

#### 包含查询

包含使用$in操作符：`db.集合名称.find({ "field" : { $in: [value1, value2,...] }})`

```shell
// 查询评论的集合中userid字段包含1003或1004的文档
db.comment.find({userid:{$in:["1003","1004"]}})
```

不包含使用$nin操作符：`db.集合名称.find({ "field" : { $nin: [value1, value2,...] }})`

```shell
// 查询评论的集合中userid字段不包含1003或1004的文档
db.comment.find({userid:{$nin:["1003","1004"]}})
```

#### 条件连接查询

查询同时满足两个以上条件，需要使用`$and`操作符将条件进行关联。

```shell
// 查询评论集合中likenum大于等于700 并且小于2000的文档
db.comment.find({$and:[{likenum:{$gte:NumberInt(700)}},{likenum:{$lt:NumberInt(2000)}}]})
```

两个以上条件之间是或者的关系，我们使用`$or`操作符进行关联

```shell
// 查询评论集合中userid为1003，或者点赞数小于1000的文档记录
db.comment.find({$or:[ {userid:"1003"} ,{likenum:{$lt:1000} }]})
```

