package zen.hua.mongo.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import zen.hua.mongo.demo.model.Comment;

public interface CommentRepository extends MongoRepository<Comment, String> {


    Page<Comment> findByParentid(String parentid, Pageable pageable);

}
