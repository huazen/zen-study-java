package zen.biz.t4_islike.other;

import lombok.Data;

/**
 * @author: ZEN
 * @create: 2024-09-17 13:04
 **/
@Data
public class Blog {

    /**
     * 主键id
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户图标
     */
    private String icon;
    /**
     * 用户姓名
     */
    private String name;
}
