package zen.biz.t4_islike;

import zen.biz.t3_seckill.other.Result;

/**
 * @author: ZEN
 * @create: 2024-09-17 09:58
 **/
public interface LikeService {


    /**
     * 发起点赞
     *
     * @param id blogId
     * @return 点赞结果
     */
    Result likeBlog(Long id);

    /**
     * 查询点赞数据
     *
     * @param id blogId
     * @return 点赞结果
     */
    Result queryBlogLikes(Long id);
}
