package zen.biz.t6_feed;

import cn.hutool.core.util.StrUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import zen.biz.t1_token.UserHolder;
import zen.biz.t1_token.other.UserDTO;
import zen.biz.t3_seckill.other.Result;
import zen.biz.t4_islike.other.Blog;
import zen.biz.t4_islike.other.ScrollResult;
import zen.biz.t5_follow.other.Follow;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author: ZEN
 * @create: 2024-09-17 13:03
 **/
@Service
public class FeedService {

    private static final String FEED_KEY = "FEED:";

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 保存笔记,并且推送笔记id给所有粉丝
     *
     * @param blog 笔记
     * @return Result
     */
    public Result saveBlog(Blog blog) {
        // 1.获取登录用户
        UserDTO user = UserHolder.getUser();
        Long userId = user.getId();
        blog.setUserId(userId);
        // 2.保存探店笔记
        boolean isSuccess = save(blog);
        if (!isSuccess) {
            return Result.fail("新增笔记失败!");
        }
        // 3.查询笔记作者的所有粉丝 select * from tb_follow where follow_user_id = #{userId}
        List<Follow> follows = query();
        // 4.推送笔记id给所有粉丝
        for (Follow follow : follows) {
            // 4.1.获取粉丝id
            Long fansId = follow.getUserId();
            // 4.2.推送
            String key = FEED_KEY + fansId;
            stringRedisTemplate.opsForZSet().add(key, blog.getId().toString(), System.currentTimeMillis());
        }
        // 5.返回id
        return Result.ok(blog.getId());
    }

    /**
     * 分页查询当前用户收件箱：关注人的更新消息
     * <pre>
     *  我们从t1时刻开始，拿第一页数据，拿到了10~6，然后记录下当前最后一次拿取的记录，就是6，
     *  t2时刻发布了新的记录，此时这个11放到最顶上，但是不会影响我们之前记录的6，此时t3时刻来拿第二页，
     *  第二页这个时候拿数据，还是从6后一点的5去拿，就拿到了5-1的记录。
     *
     *  我们这个地方可以采用sortedSet来做，可以进行范围查询，并且还可以记录当前获取数据时间戳最小值，就可以实现滚动分页了
     *
     * </pre>
     *
     * @param max    最大时间戳(倒序查询)
     * @param offset 上一次查询的偏移量
     * @return
     */
    public Result queryBlogOfFollow(Long max, Integer offset) {
        // 1.获取当前用户
        Long userId = UserHolder.getUser().getId();
        // 2.查询收件箱 ZREVRANGEBYSCORE key Max Min LIMIT offset count
        String key = FEED_KEY + userId;
        Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet()
                .reverseRangeByScoreWithScores(key, 0, max, offset, 2);
        // 3.非空判断
        if (typedTuples == null || typedTuples.isEmpty()) {
            return Result.ok();
        }
        // 4.解析数据：blogId、minTime（时间戳）、offset
        List<Long> ids = new ArrayList<>(typedTuples.size());
        long minTime = 0; // 下次请求的max
        int os = 1; // 下次请求的offset
        for (ZSetOperations.TypedTuple<String> tuple : typedTuples) { // 5 4 4 2 2
            // 4.1.获取id
            ids.add(Long.valueOf(tuple.getValue()));
            // 4.2.获取分数(时间戳）
            long time = tuple.getScore().longValue();
            if (time == minTime) { // 当前时间戳有多条数据，则os+1
                os++;
            } else {  // 查到了下一个时间戳，则os归1，从新时间戳往下查
                minTime = time;
                os = 1;
            }
        }

        // 5.根据id查询blog
        String idStr = StrUtil.join(",", ids);
        // where id in (#{idStr}) order by field(id,#{idStr});
        List<Blog> blogs = queryBlogs();

        for (Blog blog : blogs) {
            // 5.1.查询blog有关的用户
            // 5.2.查询blog是否被点赞
        }

        // 6.封装并返回
        ScrollResult r = new ScrollResult();
        r.setList(blogs);
        r.setOffset(os);
        r.setMinTime(minTime);

        return Result.ok(r);
    }

    private List<Blog> queryBlogs() {
        return null;
    }

    private List<Follow> query() {
        return null;
    }

    private boolean save(Blog blog) {
        return true;
    }

}
