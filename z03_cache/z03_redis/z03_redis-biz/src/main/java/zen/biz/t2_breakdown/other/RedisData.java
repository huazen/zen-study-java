package zen.biz.t2_breakdown.other;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class RedisData<T> {
    private LocalDateTime expireTime;
    private T data;
}