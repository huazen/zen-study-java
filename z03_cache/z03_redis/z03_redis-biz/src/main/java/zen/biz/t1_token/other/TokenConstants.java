package zen.biz.t1_token.other;

/**
 * @author: ZEN
 * @create: 2024-09-16 22:41
 **/
public interface TokenConstants {

    public static final String LOGIN_CODE_KEY = "login:code:";
    public static final Long LOGIN_CODE_TTL = 2L;
    public static final String LOGIN_USER_KEY = "login:token:";
    public static final Long LOGIN_USER_TTL = 36000L;

}
