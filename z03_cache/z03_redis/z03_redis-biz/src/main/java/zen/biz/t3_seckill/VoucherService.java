package zen.biz.t3_seckill;

import zen.biz.t3_seckill.other.Result;
import zen.biz.t3_seckill.other.Voucher;

/**
 * @author: ZEN
 * @create: 2024-09-16 23:53
 **/
public interface VoucherService {

    /**
     * 添加优惠卷信息
     *
     * @param voucher 优惠卷信息
     */
    void addSeckillVoucher(Voucher voucher);

    /**
     * 秒杀优惠卷
     *
     * @param voucherId
     * @return
     */
    Result seckillVoucher(Long voucherId);
}
