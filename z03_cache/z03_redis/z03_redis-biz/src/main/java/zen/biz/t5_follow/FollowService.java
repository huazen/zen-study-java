package zen.biz.t5_follow;

import zen.biz.t3_seckill.other.Result;

/**
 * @author: ZEN
 * @create: 2024-09-17 10:27
 **/
public interface FollowService {

    /**
     * 判断是否关注
     *
     * @param followUserId 被关注人id
     * @return 关注返回true, 未关注返回false
     */
    Result isFollow(Long followUserId);

    /**
     * 关注或取关
     *
     * @param followUserId 被关注人id
     * @param isFollow     关注还是取关
     * @return result
     */
    Result follow(Long followUserId, Boolean isFollow);


    /**
     * 共同关注
     *
     * @param otherUserId 对方id
     * @return 共同关注列表
     */
    Result followCommons(Long otherUserId);
}
