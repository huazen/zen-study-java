package zen.biz.t5_follow.other;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import zen.biz.t1_token.UserHolder;
import zen.biz.t1_token.other.UserDTO;
import zen.biz.t3_seckill.other.Result;
import zen.biz.t5_follow.FollowService;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: ZEN
 * @create: 2024-09-17 10:28
 **/
@Service
public class FollowServiceImpl implements FollowService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 判断是否关注
     *
     * @param followUserId 被关注人id
     * @return 关注返回true, 未关注返回false
     */
    @Override
    public Result isFollow(Long followUserId) {
        // 1.获取登录用户
        Long userId = UserHolder.getUser().getId();
        // 2.查询是否关注 select count(*) from tb_follow where user_id = ? and follow_user_id = ?
        Integer count = null;
        // 3.判断
        return Result.ok(count > 0);
    }


    @Override
    public Result follow(Long followUserId, Boolean isFollow) {
        // 1.获取登录用户
        Long userId = UserHolder.getUser().getId();
        String key = "follows:" + userId;
        boolean followFlag = isFollow(followUserId, userId);
        // case1: 发起关注
        if (isFollow) {
            if (followFlag) {
                return Result.fail("已关注,不能重复发起关注");
            }
            // 关注，新增数据
            Follow follow = new Follow();
            follow.setUserId(userId);
            follow.setFollowUserId(followUserId);
            boolean isSuccess = save(follow);
            if (isSuccess) {
                // 把关注用户的id，放入redis的set集合 sadd userId followerUserId
                stringRedisTemplate.opsForSet().add(key, followUserId.toString());
            }
        } else {
            // case2: 取关
            if (!followFlag) {
                return Result.fail("未关注,不能重复发起取关");
            }
            // 3.取关，删除 delete from tb_follow where user_id = ? and follow_user_id = ?
            boolean isSuccess = remove();
            if (isSuccess) {
                // 把关注用户的id从Redis集合中移除
                stringRedisTemplate.opsForSet().remove(key, followUserId.toString());
            }
        }
        return Result.ok();
    }

    @Override
    public Result followCommons(Long id) {
        // 1.获取当前用户
        Long userId = UserHolder.getUser().getId();
        String key = "follows:" + userId;
        // 2.求交集
        String key2 = "follows:" + id;
        Set<String> intersect = stringRedisTemplate.opsForSet().intersect(key, key2);
        if (intersect == null || intersect.isEmpty()) {
            // 无交集
            return Result.ok(Collections.emptyList());
        }
        // 3.解析id集合
        List<Long> ids = intersect.stream().map(Long::valueOf).collect(Collectors.toList());
        // 4.查询用户
        List<UserDTO> users = queryUsersByIds(ids);
        return Result.ok(users);
    }

    private List<UserDTO> queryUsersByIds(List<Long> ids) {
        return null;
    }

    private boolean isFollow(Long followUserId, Long userId) {
        if (followUserId == null || userId == null) {
            return false;
        }
        String key = "follows:" + userId;
        return Boolean.TRUE.equals(stringRedisTemplate.opsForSet().isMember(key, followUserId.toString()));
    }

    private boolean remove() {
        return false;
    }

    private boolean save(Follow follow) {
        return false;
    }
}
