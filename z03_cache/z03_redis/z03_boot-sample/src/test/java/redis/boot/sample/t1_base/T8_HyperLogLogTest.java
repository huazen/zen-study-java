package redis.boot.sample.t1_base;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

/**
 * @author: ZEN
 * @create: 2024-09-16 17:11
 **/
public class T8_HyperLogLogTest extends BaseTest {

    @Test
    public void testBase() {
        // `pfadd <key>< element> [element ...]`
        // # 执行pfadd命令后HLL估计的近似基数发生变化，则返回1，否则返回0。
        redisTemplate.opsForHyperLogLog().add("hll1", "redis");
        redisTemplate.opsForHyperLogLog().add("hll1", "mysql");
        redisTemplate.opsForHyperLogLog().add("hll1", "redis");

        // `pfcount <key> [key ...]` 返回给定HLL注册集的基数估算值。
        Long hll1 = redisTemplate.opsForHyperLogLog().size("hll1");
        Assert.assertEquals(2, hll1.intValue());

        // `pfmerge <destkey> <sourcekey> [sourcekey ...]` 将多个HLL合并为一个HLL
        redisTemplate.opsForHyperLogLog().add("hll2", "redis");
        redisTemplate.opsForHyperLogLog().add("hll2", "mongodb");
        // HyperLogLogOperations#union, 先执行了pfmerge，又执行了pfcount
        Long union = redisTemplate.opsForHyperLogLog().union("hll1", "hll2");
        Assert.assertEquals(3, union.intValue());
    }


    /**
     * HyperLogLog中添加100万条数据，看看内存占用和统计效果如何?
     * Redis中的HLL是基于string结构实现的，单个HLL的内存**永远小于16kb**，**内存占用低**的令人发指！
     * 作为代价，其测量结果是概率性的，**有小于0.81％的误差**。不过对于UV统计来说，这完全可以忽略。
     */
    @Test
    public void testHyperLogLog() {
        String[] values = new String[1000];
        int j = 0;
        for (int i = 0; i < 1000000; i++) {
            j = i % 1000;
            values[j] = "user_" + i;
            if (j == 999) {
                // 发送到Redis
                stringRedisTemplate.opsForHyperLogLog().add("hl2", values);
            }
        }
        // 统计数量
        Long count = stringRedisTemplate.opsForHyperLogLog().size("hl2");
        System.out.println("count = " + count); //997593
        stringRedisTemplate.delete("hl2");
    }

}
