package redis.boot.sample.t1_base;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;

/**
 * @author: ZEN
 * @create: 2024-09-16 16:38
 **/
public class T7_BitMapTest extends BaseTest {

    /**
     * 使用 RedisTemplate 的 execute 方法执行 SETBIT 命令
     */
    @Test
    public void testBase() {

        // 测试：setbit key offset value
        redisTemplate.execute((RedisCallback<Void>) connection -> {
            // id为1000的用户20200101签到 : setbit user:sign:1000 20200101 1
            connection.setBit("user:sign:1000".getBytes(), 20200101, true);
            // #id为1000的用户20200103签到 : setbit user:sign:1000 20200103 1
            connection.setBit("user:sign:1000".getBytes(), 20200103, true);
            return null;
        });

        // 测试：getbit key offset
        // 获得id为1000的用户20200101签到状态  1 表示签到
        Boolean execute = redisTemplate.execute(
                (RedisCallback<Boolean>) connection -> {
                    byte[] key = "user:sign:1000".getBytes();
                    return connection.getBit(key, 20200101);
                });
        Assert.assertTrue(execute);

        // 获得id为1000的用户20200102签到状态  0表示未签到
        Boolean execute1 = redisTemplate.execute(
                (RedisCallback<Boolean>) connection -> {
                    byte[] key = "user:sign:1000".getBytes();
                    return connection.getBit(key, 20200102);
                });
        Assert.assertFalse(execute1);

        //  测试：bitcount key [start end]  获得key的bit位为1的个数
        //  获得id为1000的用户签到次数
        Long execute2 = redisTemplate.execute(
                (RedisCallback<Long>) connection -> {
                    byte[] key = "user:sign:1000".getBytes();
                    return connection.bitCount(key);
                });
        Assert.assertEquals(new Long(2), execute2);

        // 测试：bitpos key value [start end] 返回第一个值为1或0的位的索引
        // #id为1000的用户第一次签到的日期
        Long execute3 = redisTemplate.execute(
                (RedisCallback<Long>) connection -> {
                    byte[] key = "user:sign:1000".getBytes();
                    return connection.bitPos(key, true);
                });
        Assert.assertEquals(new Long(20200101), execute3);
    }


    @Test
    public void testBitop() {
        // 测试：bitop operation destkey key [key ...]
        // 将id为2000和3000的用户作为合并对象签到
        //准备
        Long execute = redisTemplate.execute(
                (RedisCallback<Long>)
                        connection -> {
                            // 20200201的1000号用户上线
                            connection.setBit("20200201".getBytes(), 1000, true);
                            // 20200202的1000号用户上线
                            connection.setBit("20200202".getBytes(), 1001, true);
                            // 20200201的1002号用户上线
                            connection.setBit("20200201".getBytes(), 1002, true);
                            // 合并20200201的用户和20200202上线了的用户
                            byte[] destkey = "user:sign:all".getBytes();
                            byte[][] keys = new byte[2][];
                            keys[0] = "20200201".getBytes();
                            keys[1] = "20200202".getBytes();
                            // 核心操作
                            connection.bitOp(RedisStringCommands.BitOperation.OR, destkey, keys);

                            // 统计20200201和20200202都上线的用户个数
                            return connection.bitCount(destkey);
                        });
        Assert.assertEquals(new Long(3), execute);

    }

}
