package redis.boot.sample.t1_base;

import cn.hutool.core.collection.CollUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * @program: zen-study
 * @description:
 * @author: HUA
 * @create: 2023-03-17 23:23
 **/
@Slf4j
public class T3_ListBaseTest extends BaseTest {

    /**
     * <p> 从左边/右边插入一个或多个值。
     * <p> lpush/rpush  <key><value1><value2><value3> .... 	从左边/右边插入一个或多个值。
     */
    @Test
    public void push() {
        log.info("[从左边/右边插入一个或多个值。]  lpush/rpush  <key><value1><value2><value3> ....");
        log.info("=============stringRedisTemplate: lpush k1 v1 v2 v3 ");
        stringRedisTemplate.delete("k1");
        stringRedisTemplate.opsForList().leftPush("k1", "v1");
        stringRedisTemplate.opsForList().leftPushAll("k1", "v2", "v3");
        // 按照索引下标获得元素(从左到右)
        List<String> stringList = stringRedisTemplate.opsForList().range("k1", 0, -1);
        Assert.assertEquals(CollUtil.newArrayList("v3", "v2", "v1"), stringList);

        stringRedisTemplate.opsForList().rightPush("k1", "v11");
        stringRedisTemplate.opsForList().rightPushAll("k1", "v22", "v33");
        List<String> stringList2 = stringRedisTemplate.opsForList().range("k1", 0, -1);
        Assert.assertEquals(CollUtil.newArrayList("v3", "v2", "v1", "v11", "v22", "v33"), stringList2);
    }

    /**
     *
     */
    @Test
    public void pop() {
        stringRedisTemplate.delete("k1");
        // 从左边开始弹出
        stringRedisTemplate.opsForList().leftPushAll("k1", "v3", "v2");
        stringRedisTemplate.opsForList().leftPush("k1", "v1");
        // 从左边开始弹出
        String value = stringRedisTemplate.opsForList().leftPop("k1");
        Assert.assertEquals("v1", value);
        // 从右边开始弹出
        String value2 = stringRedisTemplate.opsForList().rightPop("k1");
        Assert.assertEquals("v3", value2);
    }


    /**
     * 测试：rpoplpush  <key1><key2>
     */
    @Test
    public void testRightPopAndLeftPush() {
        stringRedisTemplate.delete(CollUtil.newArrayList("k1", "k2"));
        // k1 -> v13,v12,v11
        stringRedisTemplate.opsForList().leftPushAll("k1", "v11", "v12", "v13");
        // k2 -> v21,v22,v23
        stringRedisTemplate.opsForList().rightPushAll("k2", "v21", "v22", "v23");
        // 从`<key1>`列表右边吐出一个值，插到`<key2>`列表左边
        String value = stringRedisTemplate.opsForList().rightPopAndLeftPush("k1", "k2");
        Assert.assertEquals("v11", value);

        List<String> k2List = stringRedisTemplate.opsForList().range("k2", 0, -1);
        Assert.assertEquals(CollUtil.newArrayList("v11", "v21", "v22", "v23"), k2List);
    }


    @Test
    public void testMany() {
        stringRedisTemplate.delete("k1");
        stringRedisTemplate.opsForList().leftPushAll("k1", "v11", "v12", "v13");
        // 测试：lindex  <key><index> 获取列表中指定位置的值
        String value = stringRedisTemplate.opsForList().index("k1", 1);
        Assert.assertEquals("v12", value);

        // 测试：`llen <key>`  获取列表长度
        Long length = stringRedisTemplate.opsForList().size("k1");
        Assert.assertEquals(3L, length.longValue());

        // 测试： `linsert <key>  before <value> <newvalue>`   在列表中<value>之前插入一个新值
        Long insert = stringRedisTemplate.opsForList().leftPush("k1", "v12", "newV");
        List<String> range = stringRedisTemplate.opsForList().range("k1", 0, -1);
        Assert.assertEquals(CollUtil.newArrayList("v13", "newV", "v12", "v11"), range);


        // 测试： `lrem <key><n><value>`  从左边删除n个value(从左到右)
        stringRedisTemplate.opsForList().leftPush("k1", "v12");
        stringRedisTemplate.opsForList().rightPush("k1", "v12");
        List<String> sourceRange = stringRedisTemplate.opsForList().range("k1", 0, -1);
        Assert.assertEquals(CollUtil.newArrayList("v12", "v13", "newV", "v12", "v11", "v12"), sourceRange);
        stringRedisTemplate.opsForList().remove("k1", 2, "v12");
        List<String> resultRange = stringRedisTemplate.opsForList().range("k1", 0, -1);
        Assert.assertEquals(CollUtil.newArrayList("v13", "newV", "v11", "v12"), resultRange);
    }

}
