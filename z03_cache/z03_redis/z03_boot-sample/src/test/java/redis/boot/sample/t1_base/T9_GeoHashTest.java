package redis.boot.sample.t1_base;

import cn.hutool.core.collection.CollUtil;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.domain.geo.Metrics;

import java.util.List;

/**
 * @author: ZEN
 * @create: 2024-09-16 17:24
 **/
public class T9_GeoHashTest extends BaseTest {

    @Test
    public void testBase() {
        redisTemplate.delete("user:addr");
        // 添加地理坐标: geoadd key 经度 纬度 成员名称1 经度1 纬度1 成员名称2 经度2 纬度2 成员名称3...
        // 添加用户地址 zhangf、zhaoyun、diaochan的经纬度
        redisTemplate.opsForGeo().add("user:addr", new Point(116.31, 40.05), "zhangf");
        redisTemplate.opsForGeo().add("user:addr", new Point(116.38, 39.88), "zhaoyun");
        redisTemplate.opsForGeo().add("user:addr", new Point(116.47, 40.00), "daiochan");


        // 返回geoHash值: geohash key 成员名称1 成员名称2 成员名称3...
        // 获得zhangf和diaochan的geohash码
        List<String> hashList = redisTemplate.opsForGeo().hash("user:addr", "zhangf", "daiochan");
        Assert.assertEquals(CollUtil.newArrayList("wx4eydyk5m0", "wx4gd3fbgs0"), hashList);

        // 返回成员经纬度: geopos key 成员名称1 成员名称2 成员名称3...
        // 获得zhangf和diaochan的经纬度
        List<Point> pointList = redisTemplate.opsForGeo().position("user:addr", "zhaoyun");
        Assert.assertEquals(new Point(116.38000041246414, 39.88000114172373).toString(), pointList.get(0).toString());

        // 返回成员之间的距离: geodist key 成员名称1 成员名称2 [m|km|ft|mi], 默认单位为米
        // 获得zhangf和diaochan之间的距离
        Distance distance = redisTemplate.opsForGeo().distance("user:addr", "zhangf", "daiochan");
        Assert.assertTrue(new Distance(14718.6972, Metrics.METERS).compareTo(distance) == 0);
        // 获得zhangf和diaochan之间的距离,单位为千米
        distance = redisTemplate.opsForGeo().distance("user:addr", "zhangf", "daiochan", Metrics.KILOMETERS);
        Assert.assertTrue(new Distance(14.7187, Metrics.KILOMETERS).compareTo(distance) == 0);

        // 返回成员之间的距离: georadius key 经度 纬度 半径 [m|km|ft|mi] [WITHCOORD] [WITHDIST] [ASC]


        // 获得距离zhangf20km以内的按由近到远的顺序排出前三名的成员名称、距离及经纬度
        // georadiusbymember user:addr zhangf 20 km withcoord withdist count 3 asc
        GeoResults<RedisGeoCommands.GeoLocation<Object>> georadius = redisTemplate.opsForGeo().radius("user:addr",
                "zhangf",
                new Distance(20, Metrics.KILOMETERS),
                RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().sort(Sort.Direction.ASC).limit(3).includeDistance().includeCoordinates());
        georadius.getContent().forEach(System.out::println);

        GeoResults<RedisGeoCommands.GeoLocation<Object>> georadius1 = redisTemplate.opsForGeo().radius("user:addr",
                new Circle(new Point(116.31, 40.05),
                        new Distance(20, Metrics.KILOMETERS)), RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().sort(Sort.Direction.DESC).limit(3));
    }

}
