package redis.boot.sample.t1_base;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

/**
 * @author: ZEN
 * @create: 2024-09-16 13:56
 **/

public class T5_HashBaseTest extends BaseTest {


    @Test
    public void testBase() {
        stringRedisTemplate.delete("k1");
        // 测试： hset <key> <field> <value>
        stringRedisTemplate.opsForHash().put("k1", "e1", "v1");

        // 测试： hget <key> <field>
        Object o = stringRedisTemplate.opsForHash().get("k1", "e1");
        Assert.assertEquals("v1", o);

        // 测试hmset <key1> <field1><value1><field2><value2>..
        HashMap<String, String> strMap = MapUtil.newHashMap();
        strMap.put("e2", "v2");
        strMap.put("e3", "v3");
        stringRedisTemplate.opsForHash().putAll("k1", strMap);
        Assert.assertEquals("v3", stringRedisTemplate.opsForHash().get("k1", "e3"));

        // 测试：`hexists <key1> <field>`
        Assert.assertEquals(true, stringRedisTemplate.opsForHash().hasKey("k1", "e3"));

        // 测试：hkeys <key>    列出该hash集合的所有field
        Assert.assertEquals(CollUtil.newHashSet("e1", "e2", "e3"), stringRedisTemplate.opsForHash().keys("k1"));

        // 测试：hvals <key>    列出该hash集合的所有value
        stringRedisTemplate.opsForHash().put("k1", "e4", "v1");
        Assert.assertEquals(CollUtil.newArrayList("v1", "v2", "v3", "v1"), stringRedisTemplate.opsForHash().values("k1"));

        // 测试：hgetall <key>  列出该hash集合的所有field和value
        HashMap<String, String> fieldMap = MapUtil.newHashMap();
        fieldMap.put("e1", "v1");
        fieldMap.put("e2", "v2");
        fieldMap.put("e3", "v3");
        fieldMap.put("e4", "v1");
        Assert.assertEquals(fieldMap, stringRedisTemplate.opsForHash().entries("k1"));

        // 测试：hlen <key>    获取该hash集合的长度
        Assert.assertEquals(4L, stringRedisTemplate.opsForHash().size("k1").longValue());

        // 测试：hdel <key> <field>  删除该hash集合中的某个元素
        stringRedisTemplate.opsForHash().delete("k1", "e2");
        Assert.assertNull(stringRedisTemplate.opsForHash().get("k1", "e2"));

        // 测试：del <key>   删除该key
        stringRedisTemplate.delete("k1");
    }


    @Test
    public void testBase2() {
        redisTemplate.delete("k1");
        redisTemplate.opsForHash().put("k1", "e1", 1);

        // 测试： hincrby <key> <field> <increment>
        redisTemplate.opsForHash().increment("k1", "e1", 2);
        Assert.assertEquals(3, redisTemplate.opsForHash().get("k1", "e1"));

        // 测试： hincrbyfloat <key> <field> <increment>
        redisTemplate.opsForHash().put("k1", "e2", 1.0);
        redisTemplate.opsForHash().increment("k1", "e2", 2.5);
        Assert.assertEquals(3.5, redisTemplate.opsForHash().get("k1", "e2"));


        // 测试： hsetnx <key> <field> <value>
        redisTemplate.opsForHash().putIfAbsent("k1", "e3", 2);
        Assert.assertEquals(2, redisTemplate.opsForHash().get("k1", "e3"));
    }
}
