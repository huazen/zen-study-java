package redis.boot.sample.t1_base;

import cn.hutool.core.collection.CollUtil;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Set;

/**
 * @author: ZEN
 * @create: 2024-09-16 13:33
 **/
public class T4_SetBaseTest extends BaseTest {

    @Test
    public void testSAdd() {
        stringRedisTemplate.delete("k1");
        // 测试：`sadd <key><value1><value2> .....` 添加元素
        stringRedisTemplate.opsForSet().add("k1", "1", "2", "3");

        // 测试：`smembers <key>` 获取元素
        Set<String> k1 = stringRedisTemplate.opsForSet().members("k1");
        Assert.assertEquals(CollUtil.newHashSet("1", "2", "3"), k1);

        // 测试： sismember <key><value> 判断元素是否在集合中
        boolean b = stringRedisTemplate.opsForSet().isMember("k1", "2");
        Assert.assertTrue(b);

        // 测试：scard <key> 获取集合中的元素个数
        Long size = stringRedisTemplate.opsForSet().size("k1");
        Assert.assertEquals(3l, size.longValue());

        // 测试：srandmember <key><count> 随机获取集合中的元素, 不会删除元素
        String s = stringRedisTemplate.opsForSet().randomMember("k1");
        Assert.assertTrue(CollUtil.newHashSet("1", "2", "3").contains(s));

        // 测试：spop <key> 随机移除集合中的元素
        String pop = stringRedisTemplate.opsForSet().pop("k1");
        Assert.assertTrue(CollUtil.newHashSet("1", "2", "3").contains(pop));

        // 测试：srem <key><value1><value2> .... 移除元素
        stringRedisTemplate.opsForSet().remove("k1", "2", "3");
        Assert.assertEquals(CollUtil.newHashSet("1"), stringRedisTemplate.opsForSet().members("k1"));
    }


    @Test
    public void testMove() {
        stringRedisTemplate.delete("k1");
        stringRedisTemplate.delete("k2");
        stringRedisTemplate.opsForSet().add("k1", "1", "2", "3");
//        stringRedisTemplate.opsForSet().add("k2", "4","5");
        // 测试：smove <key1><key2><value> 将元素从key1中移动到key2, key2不存在则创建
        stringRedisTemplate.opsForSet().move("k1", "2", "k2");
        Assert.assertEquals(CollUtil.newHashSet("1", "3"), stringRedisTemplate.opsForSet().members("k1"));
        Assert.assertEquals(CollUtil.newHashSet("2"), stringRedisTemplate.opsForSet().members("k2"));

        // 测试：sdiff <key1><key2>... 获取第一个集合中存在，其他集合中不存在的元素
        Set<String> diff = stringRedisTemplate.opsForSet().difference("k1", "k2");
        Assert.assertEquals(CollUtil.newHashSet("3", "1"), diff);

        // 测试：SDIFFSTORE destination key1 [key2] ... 将key1存在，其他集合中不存在的元素存入到 destination 中
        stringRedisTemplate.opsForSet().differenceAndStore("k1", CollUtil.newHashSet("k2"), "k3");
        Assert.assertEquals(CollUtil.newHashSet("1", "3"), stringRedisTemplate.opsForSet().members("k3"));

        // 测试：sinter <key1><key2>... 获取所有集合中都存在的元素
        stringRedisTemplate.opsForSet().add("k1", "2");
        Set<String> inter = stringRedisTemplate.opsForSet().intersect("k1", "k2");
        Assert.assertEquals(CollUtil.newHashSet("2"), inter);

        // 测试：sinterstore destination key1 [key2] ... 将所有集合中存在的元素存入到 destination 中
        stringRedisTemplate.opsForSet().intersectAndStore("k1", CollUtil.newHashSet("k2"), "k4");
        Assert.assertEquals(CollUtil.newHashSet("2"), stringRedisTemplate.opsForSet().members("k4"));

        // 测试：sunion <key1><key2>... 获取所有集合中存在的元素
        stringRedisTemplate.opsForSet().union("k1", "k2");
        Assert.assertEquals(CollUtil.newHashSet("3", "1", "2"), stringRedisTemplate.opsForSet().members("k1"));
    }


}
