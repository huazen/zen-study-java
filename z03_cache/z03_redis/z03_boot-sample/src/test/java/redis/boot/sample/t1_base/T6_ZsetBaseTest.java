package redis.boot.sample.t1_base;

import cn.hutool.core.collection.CollUtil;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.core.ZSetOperations;

import java.util.Set;

/**
 * @author: ZEN
 * @create: 2024-09-16 14:33
 **/
public class T6_ZsetBaseTest extends BaseTest {

    @Test
    public void testBase() {
        // 测试：zadd  <key> <score1><value1><score2><value2>…
        stringRedisTemplate.delete("topn");
        stringRedisTemplate.opsForZSet().add("topn", "java", 200);
        stringRedisTemplate.opsForZSet().add("topn", CollUtil.newHashSet(ZSetOperations.TypedTuple.of("python", 50d),
                ZSetOperations.TypedTuple.of("go", 10d), ZSetOperations.TypedTuple.of("php", 80d), ZSetOperations.TypedTuple.of("js", 70d)));

        // 测试：zrange  <key> <start><stop>
        Set<String> topn = stringRedisTemplate.opsForZSet().range("topn", 0, -1);
        Assert.assertEquals(CollUtil.newHashSet("java", "python", "php", "js", "go"), topn);

        // 测试： zrange topn 0 -1 withscores
        Set<ZSetOperations.TypedTuple<String>> topnWithScores = stringRedisTemplate.opsForZSet().rangeWithScores("topn", 0, -1);
        Assert.assertEquals(CollUtil.newHashSet(ZSetOperations.TypedTuple.of("java", 200d), ZSetOperations.TypedTuple.of("python", 50d),
                ZSetOperations.TypedTuple.of("php", 80d), ZSetOperations.TypedTuple.of("js", 70d), ZSetOperations.TypedTuple.of("go", 10d)), topnWithScores);

        // 测试：zrangebyscore key min max [withscores] [limit offset count]
        Set<String> topnByScore = stringRedisTemplate.opsForZSet().rangeByScore("topn", 0, 100);
        Assert.assertEquals(CollUtil.newHashSet("go", "js", "python", "php"), topnByScore);
        // zrangebyscore topn 50 200 withscores limit 1 2
        Set<ZSetOperations.TypedTuple<String>> topn1 = stringRedisTemplate.opsForZSet().rangeByScoreWithScores("topn", 50, 200, 1, 2);
        Assert.assertEquals(CollUtil.newHashSet(ZSetOperations.TypedTuple.of("js", 70d), ZSetOperations.TypedTuple.of("php", 80d)), topn1);

        // 测试：zrem  <key> <value1><value2>…
        stringRedisTemplate.opsForZSet().remove("topn", "go");
        Assert.assertEquals(CollUtil.newHashSet("java", "python", "php", "js"), stringRedisTemplate.opsForZSet().range("topn", 0, -1));

        // 测试：zincrby <key> <increment><value>
        stringRedisTemplate.opsForZSet().incrementScore("topn", "php", 10);
        Assert.assertEquals(90d, stringRedisTemplate.opsForZSet().score("topn", "php"), 0.01);

        // 测试：zcard <key> 统计该集合的元素个数
        Assert.assertEquals(4l, stringRedisTemplate.opsForZSet().zCard("topn").longValue());

        // 测试：zcount <key> <min><max>  统计该集合，分数区间内的元素个数
        Assert.assertEquals(3l, stringRedisTemplate.opsForZSet().count("topn", 0, 100).longValue());

        // 测试：zrank <key> <value>  返回该值在集合中的排名， 从小到大
        Assert.assertEquals(3l, stringRedisTemplate.opsForZSet().rank("topn", "java").longValue());
        // zrevrank <key> <value>  返回该值在集合中的逆序排名
        Assert.assertEquals(0l, stringRedisTemplate.opsForZSet().reverseRank("topn", "java").longValue());
    }
}
