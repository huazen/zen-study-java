package redis.boot.sample.t1_base;

import cn.hutool.core.map.MapUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.connection.stream.*;
import org.springframework.data.redis.core.StreamOperations;

import java.time.Duration;
import java.util.List;

/**
 * @author: ZEN
 * @create: 2024-09-17 02:00
 **/
@Slf4j
public class T10_StreamTest extends BaseTest {


    @SneakyThrows
    @Test
    public void testBase() {
        // 删除消费者组中的消费者 -- 不存在key就删除会报错
//        stringRedisTemplate.opsForStream().deleteConsumer("mystream",Consumer.from("mygroup","consumer1"));
        // 删除消费者组
//        stringRedisTemplate.opsForStream().destroyGroup("mystream", "mygroup");
        // 删除流
        stringRedisTemplate.delete("mystream");


        // step1: 创建消费者组，从最新位置开始消费
        checkAndCreatGroup("mystream", "mygroup");
        // stringRedisTemplate.opsForStream().createGroup("mystream", ReadOffset.latest(), "mygroup");


        // step2: 将消息添加至消息队列中
        // add(Record)
        stringRedisTemplate.opsForStream().add(StreamRecords.newRecord().in("mystream").ofMap(MapUtil.of("field1", "value1")));
        stringRedisTemplate.opsForStream().add(StreamRecords.newRecord().in("mystream").ofMap(MapUtil.of("field2", "value2")));

        // step3: 从流中读取数据
        /**
         * zennote pending-list核心使用区别
         * pending-list: 未ack的消息列表, 从第一条开始读取
         * 非pending-list: 正常消息从最新位置开始读取
         */
        StreamOffset<String> pendingStreamOffset = StreamOffset.create("mystream", ReadOffset.from("0"));
        ;
        StreamOffset<String> streamOffset = StreamOffset.create("mystream", ReadOffset.lastConsumed());

        // 读取消息，但不ack，pending-list中会存在该消息
        read(streamOffset, false);
        System.out.println("============未ack时 再次读取最新消息，没有。");
        read(streamOffset, false);
        System.out.println("============未ack时 读取pending-list");
        read(pendingStreamOffset, false);
        System.out.println("============未ack时 再次读取pending-list");
        read(pendingStreamOffset, true);
        System.out.println("============ack后, 再次读取pending-list, 已经没了");
        read(pendingStreamOffset, false);
    }

    /**
     * 读取消息
     *
     * @param streamOffset
     */
    private void read(StreamOffset<String> streamOffset, boolean ack) {
        List<MapRecord<String, Object, Object>> list = stringRedisTemplate.opsForStream().read(
                Consumer.from("mygroup", "consumer1"),
                StreamReadOptions.empty().count(10).block(Duration.ofSeconds(1)),
                streamOffset
        );

        list.forEach(recordItem -> {
            System.out.println("Stream Key: " + recordItem.getStream());
            if (ack) {
                System.out.println("Stream RecordId ack: " + recordItem.getId());
                // 确认消息 XACK, 没有确认的消息会进入pending-list中
                stringRedisTemplate.opsForStream().acknowledge("mystream", "mygroup", recordItem.getId());
            } else {
                System.out.println("Stream RecordId: " + recordItem.getId());
            }

            recordItem.getValue().forEach((field, value) -> {
                System.out.println("field: " + field);
                System.out.println("value: " + value);
            });
        });
    }

    /**
     * 校验消费组是否存在，不存在则创建，否则会出现异常
     * Error in execution; nested exception is io.lettuce.core.RedisCommandExecutionException:
     * NOGROUP No such key 'streamKey' or consumer group 'consumerGroup' in XREADGROUP with GROUP option
     *
     * @param streamKey     streamKey
     * @param consumerGroup consumerGroup
     */
    private void checkAndCreatGroup(String streamKey, String consumerGroup) {
        if (Boolean.TRUE.equals(redisTemplate.hasKey(streamKey))) {
            StreamOperations<Object, Object, Object> streamOperations = redisTemplate.opsForStream();
            StreamInfo.XInfoGroups groups = streamOperations.groups(streamKey);
            if (groups.isEmpty() || groups.stream().noneMatch(data -> consumerGroup.equals(data.groupName()))) {
                creatGroup(streamKey, consumerGroup);
            } else {
                groups.stream().forEach(g -> {
                    log.info("XInfoGroups:{}", g);
                    StreamInfo.XInfoConsumers consumers = streamOperations.consumers(streamKey, g.groupName());
                    log.info("XInfoConsumers:{}", consumers);
                });
            }
        } else {
            creatGroup(streamKey, consumerGroup);
        }
    }

    private void creatGroup(String key, String group) {
        StreamOperations<Object, Object, Object> streamOperations = redisTemplate.opsForStream();
        String groupName = streamOperations.createGroup(key, group);
        log.info("创建组成功:{}", groupName);
    }
}
