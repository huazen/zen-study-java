package redis.boot.sample.t2_sub;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * @program: zen-study-java
 * @description:
 * @author: HUA
 * @create: 2023-07-12 06:29
 **/
@SpringBootTest(classes = SubBaseApplication.class)
public class BaseTest {

    @Resource
    private RedisTemplate redisTemplate;


}
