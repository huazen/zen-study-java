package redis.boot.sample.t2_sub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: zen-study
 * @description: 将启动类多实例启动，模拟多个客户端订阅。
 * @author: HUA
 * @create: 2023-03-13 23:26
 **/
@SpringBootApplication
public class SubBaseApplication {
    public static void main(String[] args) {
        SpringApplication.run(SubBaseApplication.class, args);
    }
}
