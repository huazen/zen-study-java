package redis.boot.sample.t4_mq;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: ZEN
 * @create: 2024-09-08 09:33
 **/
@SpringBootApplication
public class RedisMqApplication implements CommandLineRunner {

    @Value("${spring.application.name}")
    private String applicationName;

    public static void main(String[] args) {
        SpringApplication.run(RedisMqApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        System.out.println("当前项目: " + applicationName);
    }
}
