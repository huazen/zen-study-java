package redis.boot.sample.t4_mq.context;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.core.RedisTemplate;
import redis.boot.sample.t4_mq.annotation.MessageConsumer;
import redis.boot.sample.t4_mq.annotation.MessageListener;
import redis.boot.sample.t4_mq.entity.Message;

import javax.annotation.Resource;

/**
 * 消费者
 *
 * @author 散装java
 * @date 2023-02-03
 */
@MessageConsumer
@Slf4j
public class RedisMqConsumer {
    @Resource
    RedisTemplate<Object, Object> redisTemplate;

    @MessageListener(topic = "topic1", mode = MessageListener.Mode.TOPIC)
    public void testTopic1(Message<?> message) {
        log.info("topic1===> " + message);
    }

    @MessageListener(topic = "topic1", mode = MessageListener.Mode.TOPIC)
    public void testTopic11(Message<?> message) {
        log.info("topic11===> " + message);
    }

    @MessageListener(topic = "topic2", mode = MessageListener.Mode.TOPIC)
    public void testTopic2(Message<?> message) {
        log.info("topic2===> " + message);
    }

    @MessageListener(topic = "topic3", mode = MessageListener.Mode.TOPIC)
    public void testTopic3(Message<?> message) {
        log.info("topic3===> " + message);
    }

    @MessageListener(channel = "pubsub", mode = MessageListener.Mode.PUBSUB)
    public void testPubsub1(Message<?> message) {
        log.info("pubsub1===> " + message);
    }

    @MessageListener(channel = "pubsub", mode = MessageListener.Mode.PUBSUB)
    public void testPubsub11(Message<?> message) {
        log.info("pubsub11===> " + message);
    }

    @MessageListener(channel = "pubsub2", mode = MessageListener.Mode.PUBSUB)
    public void testPubsub2(Message<?> message) {
        log.info("pubsub2===> " + message);
    }

    @MessageListener(streamKey = "streamKey",
            consumerGroup = "consumerGroup",
            consumerName = "consumerName",
            mode = MessageListener.Mode.STREAM)
    public void testStream1(ObjectRecord<Object, Object> message) {
        log.info("testStream1 组 consumerGroup 名 consumerName 消费消息 ===> " + message);
        // 手动 ack
//        try {
//            // 模拟消息消费业务处理耗时
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        redisTemplate.opsForStream().acknowledge("consumerGroup", message);
    }

    /**
     * 同一个组，不同的 consumerName 都可以进行消息的消费，与上面 testStream() 为竞争关系，消息仅被其中一个消费
     *
     * @param message msg
     */
    @MessageListener(streamKey = "streamKey",
            consumerGroup = "consumerGroup", consumerName = "consumerName2",
            mode = MessageListener.Mode.STREAM)
    public void testStream2(ObjectRecord<Object, Object> message) {
        log.info("testStream2  组 consumerGroup 名 consumerName2 消费消息 ===> " + message);
//        redisTemplate.opsForStream().acknowledge("consumerGroup", message);
    }

    /**
     * 不同组，会接收并行消息
     *
     * @param message msg
     */
    @MessageListener(streamKey = "streamKey",
            consumerGroup = "consumerGroup1", consumerName = "consumerName",
            mode = MessageListener.Mode.STREAM)
    public void test1Stream(ObjectRecord<Object, Object> message) {
        log.info("test1Stream  组 consumerGroup1 名 consumerName 消费消息 ===> " + message);
//        redisTemplate.opsForStream().acknowledge("consumerGroup", message);
    }

    @MessageListener(streamKey = "streamKey1",
            consumerGroup = "consumerGroup",
            consumerName = "consumerName",
            mode = MessageListener.Mode.STREAM)
    public void test1Stream1(ObjectRecord<Object, Object> message) {
        log.info("[streamKey1] 组 consumerGroup 名 consumerName 消费消息 ===> " + message);
        // 手动 ack
        try {
            // 模拟消息消费业务处理耗时
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        redisTemplate.opsForStream().acknowledge("consumerGroup", message);
    }


    /**
     * 消费 streamKey 中，没有进行 ack 的消息
     *
     * @param message message
     */
    @MessageListener(
            streamKey = "streamKey1",
            consumerGroup = "consumerGroup",
            consumerName = "consumerName",
            pending = true,
            mode = MessageListener.Mode.STREAM)
    public void testStreamPending(ObjectRecord<Object, Object> message) {
        log.info("[streamKey1] testStreamPending 组 consumerGroup 名 consumerName 消费未 ack 消息 ===> " + message);
        redisTemplate.opsForStream().acknowledge("consumerGroup", message);
    }
}