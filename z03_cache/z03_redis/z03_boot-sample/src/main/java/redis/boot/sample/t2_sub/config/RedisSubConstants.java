package redis.boot.sample.t2_sub.config;

/**
 * @program: zen-study-java
 * @description:
 * @author: HUA
 * @create: 2023-07-12 06:32
 **/
public class RedisSubConstants {

    public static final String channel = "redis.life";
    public static final String channel2 = "redis.news";
}
