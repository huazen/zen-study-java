package redis.boot.sample.biz;

import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

public class RedisLockService {

    public static final String SCRIPT_LOCK_STR = "return redis.call('set',KEYS[1], ARGV[1],'NX','PX',ARGV[2])";


    public static final String SCRIPT_UNLOCK_STR = "if redis.call('get',KEYS[1]) == ARGV[1] then return tostring(redis.call('del', KEYS[1])==1) else return 'false' end";


    public static final String SCRIPT_RENEWAL_STR = "if redis.call('get', KEYS[1]) ==ARGV[1] then return redis.call('pexpire', KEYS[1], ARGV[2]) else  return 0  end";


    private final RedisTemplate<String, String> redisTemplate;

    public RedisLockService(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 乐观锁递增
     * <pre>
     *     使用场景:
     *     1. 库存管理：购买商品
     *     2. 抢购活动
     *     3. 限流
     * </pre>
     *
     * @param key 键
     * @return 递增后的值
     */
    public long incrementWithOptimisticLock(String key) {
        return redisTemplate.execute((RedisCallback<Long>) connection -> {
            byte[] rawKey = key.getBytes();
            long newValue = 0;

            do {
                // 监控键
                connection.watch(rawKey);
                // 获取当前值
                byte[] rawValue = connection.get(rawKey);
                long currentValue = rawValue != null ? Long.parseLong(new String(rawValue)) : 0;
                // 开始事务
                connection.multi();
                // 递增键的值
                connection.incr(rawKey);
                // 尝试执行事务
                List<Object> execResult = connection.exec();

                if (execResult != null && !execResult.isEmpty()) {
                    newValue = Long.parseLong(execResult.get(0).toString());
                    break; // 事务成功，跳出循环
                }

                // 事务被取消，因为被监视的键被修改了，重试
            } while (true);


            return newValue;
        });
    }


}