package redis.boot.sample.t2_sub.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import redis.boot.sample.t2_sub.config.RedisSubConstants;

import javax.annotation.Resource;

@RequestMapping("sub")
@RestController
public class SubController {

    @Resource
    private RedisTemplate<Object, Object> redisTemplate;


    /**
     * <pre>
     *
     * 测试发布订阅
     * 1. 将启动类多实例启动，模拟多个客户端订阅-所有订阅方都会收到消息。
     * 2. 通过控制台发送测试：publish redis.life '"hello"'  - 因为配置json序列化，所以需要用双引号包裹消息内容。
     *  </pre>
     * 测试：<a>http://localhost:8981/sub/publish</a>
     */
    @RequestMapping(value = "/publish/{msg}", method = RequestMethod.GET)
    public Boolean eventPush(@PathVariable("msg") String msg) {
        redisTemplate.convertAndSend(RedisSubConstants.channel, msg);
        redisTemplate.convertAndSend(RedisSubConstants.channel, "topic1-我是第一种事件消息");
        redisTemplate.convertAndSend(RedisSubConstants.channel2, "topic2-我是第二种事件消息");
        return true;
    }

}
