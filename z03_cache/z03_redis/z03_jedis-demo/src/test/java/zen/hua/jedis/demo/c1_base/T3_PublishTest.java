package zen.hua.jedis.demo.c1_base;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

/**
 * @program: zen-study-java
 * @description: 发布订阅测试
 * @author: HUA
 * @create: 2023-07-11 22:05
 **/
public class T3_PublishTest {

    public static void main(String[] args) {

    }

    private Jedis buildJedis() {
        //创建Jedis对象, 非线程安全，每次使用每次创建
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        jedis.auth("123456");
        return jedis;
    }

    @Test
    public void testPublish() {
        // 频道名，自定义
        String channel = "channel";
        String message = "测试发送消息";

        Jedis jedis = buildJedis();

        // 发布消息
        jedis.publish(channel, message);

        // 订阅消息
        jedis.subscribe(new JedisPubSub() {
            @Override
            public void onMessage(String channel, String message) {
                System.out.println(String.format("收到消息:%s", message));
            }
        }, channel);

        jedis.close();
    }
}
