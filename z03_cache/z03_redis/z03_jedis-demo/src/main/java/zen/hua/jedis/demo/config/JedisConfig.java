package zen.hua.jedis.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @program: zen-study-java
 * @description: jedis配置类
 * @author: HUA
 * @create: 2023-06-08 22:31
 **/
@Configuration
public class JedisConfig {


    @Bean
    @ConfigurationProperties("redis")
    public JedisPoolConfig jedisPoolConfig() {
        return new JedisPoolConfig();
    }

    /**
     * Jedis本身是线程不安全的，并且频繁的创建和销毁连接会有性能损耗，
     * 因此我们推荐大家使用Jedis连接池代替Jedis的直连方式。
     *
     * @param host
     * @param port
     * @param password
     * @return
     */
    @Bean(destroyMethod = "close")
    public JedisPool jedisPool(@Value("${redis.host}") String host,
                               @Value("${redis.port}") int port,
                               @Value("${redis.password}") String password) {
        JedisPool jedisPool = new JedisPool(jedisPoolConfig(), host, port, 2000, password);
        return jedisPool;
    }
}
