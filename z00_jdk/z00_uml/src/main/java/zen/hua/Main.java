package zen.hua;

import cn.hutool.core.convert.Convert;

public class Main {
    public static void main(String[] args) {
        String str = "\\u542F\\u52A8\\u65F6\\u521B\\u5EFA\\u8868\\u7ED3\\u6784\\uFF0C\\u7ED3\\u675F\\u65F6\\u5220\\u9664";
        String toStr = Convert.unicodeToStr(str);
        System.out.println(toStr);
    }
}