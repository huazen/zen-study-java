package zen.hua.relation.d5_inheritance;

/**
 * @author: ZEN
 * @create: 2024-01-14 21:56
 **/
public class People {

    private String name;
    private Integer age;

    public void eat() {
        System.out.println("吃饭");
    }

    public void sleep() {
        System.out.println("睡觉");
    }
}
