package zen.hua.relation.d3_composition;

import java.util.List;

/**
 * @author: ZEN
 * @create: 2024-01-14 21:34
 **/
public class Order {
    private String orderCode;
    private Double totalMoney;
    private List<OrderItem> orderItems;
}
