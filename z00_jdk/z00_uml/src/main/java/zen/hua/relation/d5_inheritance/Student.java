package zen.hua.relation.d5_inheritance;

/**
 * @author: ZEN
 * @create: 2024-01-14 21:57
 **/
public class Student extends People {
    private String studentNo;

    public void study() {
        System.out.println("学生学习");
    }
}
