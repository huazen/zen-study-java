package zen.hua.relation.d5_inheritance;

/**
 * @author: ZEN
 * @create: 2024-01-14 21:58
 **/
public class Teacher extends People {

    private String teacherCode;

    public void teach() {
        System.out.println("上课");
    }
}
