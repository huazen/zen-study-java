package zen.hua.relation.d4_dependency;

/**
 * @author: ZEN
 * @create: 2024-01-14 21:43
 **/
public class People {

    private String name;
    private Integer age;

    public void drawCircle(Circle circle) {
        System.out.println("人画圆");
        circle.draw();
    }
}
