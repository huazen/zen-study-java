package zen.hua.relation.d6_implementation;

/**
 * @author: ZEN
 * @create: 2024-01-14 22:04
 **/
public interface USB {

    void connect();
}
