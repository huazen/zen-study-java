package zen.hua.relation.d1_association;


/**
 * @author: ZEN
 * @create: 2024-01-14 20:33
 * <pre>
 *     1. Customer与Address单向关联
 *     2. 与Product双向关联
 * </pre>
 **/
public class Customer {

    private Address address;

    private Product product;
}
