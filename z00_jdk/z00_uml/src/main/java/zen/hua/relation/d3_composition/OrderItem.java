package zen.hua.relation.d3_composition;

/**
 * @author: ZEN
 * @create: 2024-01-14 21:34
 **/
public class OrderItem {
    private String itemCode;
    private Double unitPrice;
    private Integer num;
    private Product product;
}
