package zen.bug.string;

import com.alibaba.fastjson2.JSONObject;
import org.junit.Test;
import zen.bug.pojo.User;

/**
 * @author: ZEN
 * @create: 2024-07-20 14:06
 **/
public class StringTest {


    /**
     * 字符+数字，无论前后顺序，都会被解析为数字
     * 注意 +=操作符，会先进行运算，再拼接
     */
    @Test
    public void testCharSplit() {
        String str = "hhh";
        str += '(' + 123 + ')';
        // 返回 hhh204
        System.out.println(str);
    }

    /**
     * 位数错觉
     */
    @Test
    public void placeValueIllusion() {
        Long num = 12346L;
        String str = Long.toBinaryString(num);
        // 返回 11000000111010
        System.out.println(str);
        // 返回 1，容易造成错觉，字符串的的低位是数字的高位
        System.out.println(str.charAt(0));
    }


    @Test
    public void testJson() {
        System.out.println(JSONObject.toJSONString("+++++++" + new User()));
    }

}
