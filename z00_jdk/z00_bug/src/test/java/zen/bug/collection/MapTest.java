package zen.bug.collection;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: ZEN
 * @create: 2024-08-03 20:54
 **/
public class MapTest {


    /**
     * 类型擦除，编译通过，运行时不报错，但获取不到数据，难以排查。
     */
    @Test
    public void testTypeErasure() {
        Map<Long, String> map = new HashMap<>();
        map.put(1L, "a");
        // 可以用 "1"，类型擦除，编译通过，运行时不报错，但获取不到数据，难以排查。
        String s = map.get("1");
        System.out.println(s);
    }
}
