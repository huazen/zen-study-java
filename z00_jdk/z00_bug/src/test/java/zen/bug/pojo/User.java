package zen.bug.pojo;

import lombok.Data;

/**
 * @author: ZEN
 * @create: 2024-10-25 21:06
 **/
@Data
public class User {

    private String name;

    private String password;
}
