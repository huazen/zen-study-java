package zen.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zen.book.items.composite.ProductComposite;
import zen.book.pojo.ProductItem;
import zen.book.service.ProductItemService;

@RestController
@RequestMapping("/product")
public class ProductItemController {
    @Autowired
    private ProductItemService productItemService;
    @PostMapping("/fetchAllItems")
    public ProductComposite fetchAllItems(){
        return productItemService.fetchAllItems();
    }

    @PostMapping("/addItems")
    public ProductComposite addItems(@RequestBody ProductItem item){
        return productItemService.addItems(item);
    }

    @PostMapping("/delItems")
    public ProductComposite delItems(@RequestBody ProductItem item){
        return productItemService.delItems(item);
    }
}
