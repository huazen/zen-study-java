package zen.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import zen.book.adapter.Login3rdAdapter;
import zen.book.pojo.BusinessLaunch;
import zen.book.pojo.UserInfo;
import zen.book.service.UserService;

import java.io.IOException;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private Login3rdAdapter login3rdAdapter;

    @PostMapping("/login")
    public String login(String account, String password) {
        return login3rdAdapter.login(account, password);
    }

    @PostMapping("/register")
    public String register(@RequestBody UserInfo userInfo) {
        return login3rdAdapter.register(userInfo);
    }

    @GetMapping("/gitee")
    public String gitee(String code, String state) throws IOException {
        return login3rdAdapter.loginByGitee(code, state);
    }

    @PostMapping("/business/launch")
    public List<BusinessLaunch> filterBusinessLaunch(@RequestParam("city") String city,
                                                     @RequestParam("sex") String sex,
                                                     @RequestParam("product") String product) {
        return userService.filterBusinessLaunch(city, sex, product);
    }
    @PostMapping("/ticket")
    public Object createTicket(String type, String productId, String content, String title, String bankInfo, String taxId) {
        return userService.createTicket(type, productId, content, title, bankInfo, taxId);
    }
}
