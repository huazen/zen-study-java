package zen.book.ordermanagement.command;

import zen.book.pojo.Order;

public interface OrderCommandInterface {
    void execute(Order order);
}
