package zen.book.ordermanagement.command.invoker;

import zen.book.ordermanagement.command.OrderCommandInterface;
import zen.book.pojo.Order;

public class OrderCommandInvoker {
    public void invoke(OrderCommandInterface command, Order order) {
        command.execute(order);
    }
}
