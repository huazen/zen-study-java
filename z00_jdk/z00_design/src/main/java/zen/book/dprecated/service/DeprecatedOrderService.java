package zen.book.dprecated.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zen.book.dprecated.state.DeprecatedOrder;
import zen.book.dprecated.state.DeprecatedOrderContext;

@Service
public class DeprecatedOrderService {

    @Autowired
    private DeprecatedOrderContext orderContext;

    public DeprecatedOrder createOrder(String productId) {
        //订单Id的生成逻辑，笔者有话要说... ...
        String orderId = "OID" + productId;
        return orderContext.createOrder(orderId, productId);
    }

    public DeprecatedOrder pay(String orderId) {
        return orderContext.payOrder(orderId);
    }

    public DeprecatedOrder send(String orderId) {
        return orderContext.sendOrder(orderId);
    }

    public DeprecatedOrder receive(String orderId) {
        return orderContext.receiveOrder(orderId);
    }
}
