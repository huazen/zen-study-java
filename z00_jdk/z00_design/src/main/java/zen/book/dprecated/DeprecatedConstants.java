package zen.book.dprecated;

import zen.book.dprecated.observer.DeprecatedAbstractObserver;

import java.util.List;
import java.util.Vector;

public class DeprecatedConstants {
    public final static List<DeprecatedAbstractObserver> OBSERVER_LIST = new Vector<>();
}
