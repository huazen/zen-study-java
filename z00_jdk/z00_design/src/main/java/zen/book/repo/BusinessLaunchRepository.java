package zen.book.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zen.book.pojo.BusinessLaunch;

@Repository
public interface BusinessLaunchRepository extends JpaRepository<BusinessLaunch, Integer> {
}
