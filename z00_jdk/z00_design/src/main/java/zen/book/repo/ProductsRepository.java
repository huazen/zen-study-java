package zen.book.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zen.book.pojo.Products;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Integer> {
    //根据product id查询商品信息
    public Products findByProductId(String productId);
}
