package zen.book.items.vistor;

import zen.book.items.composite.AbstractProductItem;

public interface ItemVisitor<T> {
    T visitor(AbstractProductItem productItem);
}
