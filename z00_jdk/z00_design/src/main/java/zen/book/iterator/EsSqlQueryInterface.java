package zen.book.iterator;

public interface EsSqlQueryInterface<T> {
    public T iterator();
}
