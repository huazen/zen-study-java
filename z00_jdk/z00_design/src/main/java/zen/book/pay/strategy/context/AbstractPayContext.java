package zen.book.pay.strategy.context;

import org.springframework.stereotype.Component;
import zen.book.pojo.Order;

@Component
public abstract class AbstractPayContext {
    public abstract String execute(Order order);
}
