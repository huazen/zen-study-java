package zen.book.pay.strategy.context;

import zen.book.pay.strategy.PayStrategyInterface;
import zen.book.pojo.Order;

public class PayContext extends AbstractPayContext{
    private PayStrategyInterface payStrategy;

    public PayContext(PayStrategyInterface payStrategy) {
        this.payStrategy = payStrategy;
    }

    public String execute(Order order) {
        return this.payStrategy.pay(order);
    }
}
