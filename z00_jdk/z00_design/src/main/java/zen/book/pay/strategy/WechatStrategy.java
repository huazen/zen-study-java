package zen.book.pay.strategy;

import zen.book.pojo.Order;

public class WechatStrategy implements PayStrategyInterface{
    @Override
    public String pay(Order order) {
        return "wechat pay success!";
    }
}
