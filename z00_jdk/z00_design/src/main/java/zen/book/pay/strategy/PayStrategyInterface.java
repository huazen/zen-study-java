package zen.book.pay.strategy;

import zen.book.pojo.Order;

public interface PayStrategyInterface {
    String pay(Order order);
}
