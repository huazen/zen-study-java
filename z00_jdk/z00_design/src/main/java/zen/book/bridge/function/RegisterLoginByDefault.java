package zen.book.bridge.function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zen.book.bridge.abst.factory.RegisterLoginComponentFactory;
import zen.book.pojo.UserInfo;
import zen.book.repo.UserRepository;

import javax.annotation.PostConstruct;

@Component
public class RegisterLoginByDefault extends AbstractRegisterLoginFunc implements RegisterLoginFuncInterface {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    private void initFuncMap() {
        RegisterLoginComponentFactory.funcMap.put("Default", this);
    }

    @Override
    public String login(String account, String password) {
        return super.commonLogin(account, password, userRepository);
    }

    @Override
    public String register(UserInfo userInfo) {
        return super.commonRegister(userInfo, userRepository);
    }

    @Override
    public boolean checkUserExists(String userName) {
        return super.commonCheckUserExists(userName, userRepository);
    }
}
